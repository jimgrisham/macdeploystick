//
//  MunkiContainerView.m
//  MDS
//
//  Created by Timothy Perfitt on 9/15/19.
//  Copyright © 2019 Twocanoes Software. All rights reserved.
//

#import "ResourcesContainerView.h"
#import "ResourcesView.h"
@implementation ResourcesContainerView

- (instancetype)initWithCoder:(NSCoder *)coder
{
    self = [super initWithCoder:coder];
    if (self) {
        NSNib *nib=[[NSNib alloc] initWithNibNamed:@"ResourcesView" bundle:nil];
        [nib instantiateWithOwner:self topLevelObjects:nil];
        [self addSubview:self.view];
    }
    return self;
}


-(void)awakeFromNib{


    [self performSelector:@selector(test:) withObject:self afterDelay:1];
}
-(void)test:(id)sender{


    [self.packagesButtonView addSubview:self.packagesOrderButton];
    self.packagesOrderButton.frame=NSMakeRect(0, 0, self.packagesOrderButton.frame.size.width, self.packagesOrderButton.frame.size.height);


    [self.profilesButtonView addSubview:self.profilesOrderButton];
    self.profilesOrderButton.frame=NSMakeRect(0, 0, self.profilesOrderButton.frame.size.width, self.profilesOrderButton.frame.size.height);


    [self.scriptsButtonView addSubview:self.scriptsOrderButton];
    self.scriptsOrderButton.frame=NSMakeRect(0, 0, self.scriptsOrderButton.frame.size.width, self.scriptsOrderButton.frame.size.height);

}
@end


