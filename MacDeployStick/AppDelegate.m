//
//  AppDelegate.m
//  MacDeployStick
//
//  Created by Timothy Perfitt on 12/22/18.
//  Copyright © 2018 Twocanoes Software. All rights reserved.
//
//#define BETA 1
#import "TCSConfigHelper.h"
#import "AppDelegate.h"
#import <TCSPromo/TCSPromoManager.h>
#import "DeploySettings.h"
#import "TCSDefaultsManager.h"
#import "TCSMDMService.h"
#import "TCSArrayControllerCountTransformer.h"
#import "TCSUtility.h"
#import "TCSConstants.h"
#import "TCSServiceUpdateManager.h"
//#import "GCDWebServerDataResponse.h"
@interface AppDelegate ()
@property (assign) BOOL isStoreVersion;
@property (strong) NSViewController *vc;
@end

@implementation AppDelegate
- (instancetype)init
{
    self = [super init];
    if (self) {
        TCSArrayControllerCountTransformer *xfmr=[[TCSArrayControllerCountTransformer alloc] init];

        [NSValueTransformer setValueTransformer:xfmr forName:@"ArrayControllerCountTransformer"];
        NSString *path=[[NSBundle mainBundle] pathForResource:@"Defaults" ofType:@"plist" inDirectory:nil];

        NSDictionary *defaultDict=[NSDictionary dictionaryWithContentsOfFile:path];

        NSError *err;
        NSDictionary *mdsinfo;
        if (@available(macOS 10.13, *)) {
            mdsinfo=[NSDictionary dictionaryWithContentsOfURL:[NSURL URLWithString:[defaultDict objectForKey:@"mdsinfo_url"]] error:&err];

            if (!mdsinfo) {
                NSLog(@"could not load mdsinfo");
            }
        }

        NSDictionary *infoPlist=[[NSBundle mainBundle] infoDictionary];

        NSString *appVersion=[infoPlist objectForKey:@"CFBundleShortVersionString"];

        if (mdsinfo && mdsinfo[@"imgr_required_versions"][appVersion]) {

            if (![mdsinfo[@"imgr_required_versions"][appVersion] isEqualToString:[infoPlist objectForKey:@"CFBundleShortVersionString"]]){
                if (mdsinfo[@"imgr_required_versions"][appVersion]){
                    NSString *imagrRequiredVersion=mdsinfo[@"imgr_required_versions"][appVersion];

                    [defaultDict setValue:imagrRequiredVersion forKey:@"imagr_version_required"];

                    [defaultDict setValue:mdsinfo[@"imgr"][imagrRequiredVersion] forKey:@"imagr_url"];
                }
            }

        }
        if (mdsinfo && mdsinfo[@"munki_required_versions"][appVersion]) {

            if (mdsinfo[@"munki_required_versions"][appVersion]){
                NSString *munkiRequiredVersion=mdsinfo[@"munki_required_versions"][appVersion];

                [defaultDict setValue:munkiRequiredVersion forKey:@"munki_required_versions"];

                [defaultDict setValue:mdsinfo[@"munki"][munkiRequiredVersion] forKey:@"munki_url"];
            }


        }

        
        NSUserDefaults *ud=[NSUserDefaults standardUserDefaults];

        [ud registerDefaults:defaultDict];


    }
    return self;
}
-(BOOL)resourcesNeeded{
    NSString *currentImgrVersion=[TCSConfigHelper imagrVersion];
    if (currentImgrVersion) {
        //means we have imagr
        NSString *requiredImgrVersion=[[NSUserDefaults standardUserDefaults] objectForKey:@"imagr_version_required"];
        if (![currentImgrVersion isEqualToString:requiredImgrVersion]){


            if ([[NSApp delegate] respondsToSelector:@selector(clearResourceCache:)]){

                [[NSApp delegate] performSelector:@selector(clearResourceCache:) withObject:self];
            }

        }

    }
    
    return [TCSConfigHelper checkIfResourcesNeeded];
}
#ifndef APPSTORE
- (nullable PADDisplayConfiguration *)willShowPaddleUIType:(PADUIType)uiType product:(nonnull PADProduct *)product{

    PADDisplayConfiguration *config=[[PADDisplayConfiguration alloc] initWithDisplayType:PADDisplayTypeSheet hideNavigationButtons:NO parentWindow:[NSApp mainWindow]];
    return config;

}
-(void)showPaddleInfo{
//    NSString *myPaddleProductID = @"548513";
//    NSString *myPaddleVendorID = @"16588";
//    NSString *myPaddleAPIKey = @"d20e1996121f529dac694ebddb57a294";

    // Default Product Config in case we're unable to reach our servers on first run:
    PADProductConfiguration *defaultProductConfig = [[PADProductConfiguration alloc] init];

    defaultProductConfig.productName = @"MDS";
    defaultProductConfig.vendorName = @"Twocanoes Software, Inc";

    // Initialize the SDK singleton with the config:
//        Paddle *paddle = [Paddle sharedInstanceWithVendorID:myPaddleVendorID
//                                                 apiKey:myPaddleAPIKey
//                                              productID:myPaddleProductID
//                                          configuration:defaultProductConfig];
//    paddle.delegate=self;
//    // Initialize the Product you'd like to work with:
//    PADProduct *paddleProduct = [[PADProduct alloc] initWithProductID:myPaddleProductID productType:PADProductTypeSDKProduct configuration:nil];
//
//
//    // Ask the Product to get it's latest state and info from the Paddle Platform:
//    [paddleProduct refresh:^(NSDictionary * _Nullable productDelta, NSError * _Nullable error) {
//        // Optionally show the default "Product Access" UI to gatekeep your app
////        [paddle showProductAccessDialogWithProduct:paddleProduct];
//    }];


}
#endif

-(IBAction)activateLicense:(id)sender{
    NSString *productID=[[Paddle sharedInstance] productID];

    PADProduct *paddleProduct=[PADProduct initializedProductForID:productID];

    [paddleProduct refresh:^(NSDictionary * _Nullable productDelta, NSError * _Nullable error) {
        // Optionally show the default "Product Access" UI to gatekeep your app
        [[Paddle sharedInstance] showProductAccessDialogWithProduct:paddleProduct];
    }];

}
-(BOOL)isActivated{
    NSString *productID=[[Paddle sharedInstance] productID];

    PADProduct *paddleProduct=[PADProduct initializedProductForID:productID];

    return [paddleProduct activated];

}
-(IBAction)deactivateLicense:(id)sender{


    NSAlert *alert=[[NSAlert alloc] init];
    alert.messageText=@"License Deactive Confirmation";
    alert.informativeText=@"Are you sure you want to deactivate your license?";

    [alert addButtonWithTitle:@"Cancel"];
    [alert addButtonWithTitle:@"Deactivate"];
    NSInteger res=[alert runModal];
    if (res==NSAlertFirstButtonReturn) return;

   NSString *productID=[[Paddle sharedInstance] productID];

    PADProduct *padProduct=[PADProduct initializedProductForID:productID];

    [padProduct deactivateWithCompletion:^(BOOL deactivated, NSError * _Nullable error) {

        if (error) {

            [[NSAlert alertWithError:error] runModal];
        }
        else {
            NSAlert * alert=[NSAlert alertWithError:[NSError errorWithDomain:@"TCS" code:-1 userInfo:@{NSLocalizedDescriptionKey:@"License successfully deactivated"}]];
            [alert runModal];

        }
    }];



}
- (IBAction)clearResourceCache:(id)sender {
    NSFileManager *fm=[NSFileManager defaultManager];
    NSArray *applicationSupportPath = NSSearchPathForDirectoriesInDomains(NSApplicationSupportDirectory, NSUserDomainMask, YES);

    NSString *appSupportDir=[applicationSupportPath[0] stringByAppendingPathComponent:@"com.twocanoes.macdeploystick"];

    if ([fm fileExistsAtPath:[appSupportDir stringByAppendingPathComponent:@"Imagr.app"]]){

        NSError *err;
        if([fm removeItemAtPath:[appSupportDir stringByAppendingPathComponent:@"Imagr.app"] error:&err]==NO) {

            [[NSAlert alertWithError:err] runModal];
        }
    }
}


-(BOOL)applicationShouldTerminateAfterLastWindowClosed:(NSApplication *)sender{
    return YES;

}

- (void)applicationDidFinishLaunching:(NSNotification *)aNotification {

    NSUserDefaults *ud=[NSUserDefaults standardUserDefaults];

    if (![ud objectForKey:SERVERHOSTNAME] ||
        [[ud objectForKey:SERVERHOSTNAME] length]==0){

        NSString *currHost=[[NSHost currentHost] name];
        if ([currHost containsString:@"."]==NO){
            currHost=[currHost stringByAppendingString:@".local"];
        }

        [ud setObject:currHost forKey:SERVERHOSTNAME];
    }

    NSFileManager *fm=[NSFileManager defaultManager];

    if ([ud objectForKey:@"syncURL"] && ![ud objectForKey:@"syncURLArray"]){

        NSError *err;
        if([fm fileExistsAtPath:[DeploySettings syncFolder]]==NO){
            if([fm createDirectoryAtPath:[DeploySettings syncFolder] withIntermediateDirectories:YES attributes:nil error:&err]==NO){

                [[NSAlert alertWithError:err] runModal];

            }

        }
        NSString *syncURLString=[ud objectForKey:@"syncURL"];

        NSString *hash=[NSString stringWithFormat:@"%lx",[syncURLString hash]];
        NSArray *itemsToMove=[fm contentsOfDirectoryAtPath:[DeploySettings syncFolder] error:&err];

        if (err){

            [[NSAlert alertWithError:err] runModal];

        }
        else {

            NSString *newSyncFolder=[[DeploySettings syncFolder] stringByAppendingPathComponent:hash];


            if([fm createDirectoryAtPath:newSyncFolder withIntermediateDirectories:YES attributes:nil error:&err]==NO){

                [[NSAlert alertWithError:err] runModal];

            }

            else {

                [itemsToMove enumerateObjectsUsingBlock:^(NSString *itemName, NSUInteger idx, BOOL * _Nonnull stop) {
                    NSError *err;
                    if([fm moveItemAtPath:[[DeploySettings syncFolder] stringByAppendingPathComponent:itemName] toPath:[newSyncFolder stringByAppendingPathComponent:itemName] error:&err]==NO){

                        *stop=YES;
                        [[NSAlert alertWithError:err] runModal];
                    }

                }];

                [ud setObject:@[@{@"name":@"migrated",@"url":syncURLString}] forKey:@"syncURLArray"];

            }
        }
    }
    NSArray *mdmCommands=[NSArray arrayWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"MDMCommands" ofType:@"plist"]];
    NSMenu *mainMenu = [[NSApplication sharedApplication] mainMenu];

    NSMenuItem *commandMenuItem=[mainMenu itemWithTag:1];
    NSMenu *commandsMenu=commandMenuItem.submenu;
    [mdmCommands  enumerateObjectsUsingBlock:^(NSDictionary *info, NSUInteger idx, BOOL * _Nonnull stop) {

        NSString *title=[info objectForKey:@"title"];
        SEL selector = NSSelectorFromString(@"mdmCommand:");
        NSMenuItem *newMenuItem=[[NSMenuItem alloc] initWithTitle:title action:selector keyEquivalent:@""];
        newMenuItem.representedObject=info;

        newMenuItem.hidden=[[info objectForKey:@"isHidden"] boolValue];
        [commandsMenu insertItem:newMenuItem atIndex:0];

    }];

    [[NSNotificationCenter defaultCenter] addObserverForName:@"TCSWarning" object:nil queue:nil usingBlock:^(NSNotification * _Nonnull note) {
        dispatch_async(dispatch_get_main_queue(), ^{
        [[NSAlert alertWithError:note.userInfo[@"error"]] runModal];
        });

    }];
#ifdef APPSTORE
        self.isStoreVersion=YES;
#else
    self.isStoreVersion=NO;
    [self showPaddleInfo];
#endif
    NSDate *lastTimePromoShown=[[NSUserDefaults standardUserDefaults] objectForKey:@"lastTimePromoShown"] ;

    if (lastTimePromoShown==nil) {

        [[NSUserDefaults standardUserDefaults] setObject:[NSDate date] forKey:@"lastTimePromoShown"]; lastTimePromoShown=[NSDate date]; }
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"hidePromo"] boolValue]==NO && (fabs([lastTimePromoShown timeIntervalSinceNow])>60*60*24*7) ) {
        [[TCSPromoManager sharedPromoManager] showPromoWindow:self];

        [[NSUserDefaults standardUserDefaults] setObject:[NSDate date] forKey:@"lastTimePromoShown"];
    }




#ifdef BETA

    NSString *compileDate = [NSString stringWithUTF8String:__DATE__];
    NSDateFormatter *df = [[NSDateFormatter alloc] init];
    [df setDateFormat:@"MMM d yyyy"];
    NSLocale *usLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US"];
    [df setLocale:usLocale];
    NSDate *betaExpireDate = [[df dateFromString:compileDate] dateByAddingTimeInterval:60*60*24*14];

    NSLog(NSLocalizedString(@"Beta Product.  Expires on %@",@"log message"),[betaExpireDate description]);


    NSTimeInterval ti=[betaExpireDate timeIntervalSinceNow];
    NSInteger res;
    if (ti<0) {
        res=NSRunAlertPanel(NSLocalizedString(@"Beta Period Ended",@"Title for alert panel that beta expired"),NSLocalizedString(@"This beta has expired.  Please visit twocanoes.com to download an updated version.",@"body for alert panel that beta expired"), NSLocalizedString(@"Visit",@"button alert panel that beta expired"),NSLocalizedString(@"Quit", @"button for alert panel that beta expired"),nil);
        if (res==NSAlertDefaultReturn) {
            ;
            NSURL *url=[NSURL URLWithString:SUPPORTURLS[@"mainwebsite"]];
            [[NSWorkspace sharedWorkspace]  openURL:url];
        }
        [NSApp terminate:self];
    }
    else {

        NSAlert *alert=[NSAlert alertWithMessageText:@"Beta Product" defaultButton:@"OK" alternateButton:nil otherButton:nil informativeTextWithFormat:@"This is a beta product and will expire on %@",[betaExpireDate description]];

        [alert runModal];
    }


#endif
}


- (void)applicationWillTerminate:(NSNotification *)aNotification {

    [[DeploySettings sharedSettings] saveToPrefs:self];
    [[TCSDefaultsManager sharedManager] clearCache];

}

-(IBAction)importWorkflows:(id)sender{

    NSOpenPanel *openPanel=[NSOpenPanel openPanel];

    openPanel.canChooseFiles=YES;
    openPanel.canChooseDirectories=NO;
    openPanel.allowsMultipleSelection=NO;
    openPanel.allowedFileTypes=@[@"mdsworklows",@"mdsworkflows"];
    openPanel.message=@"Select a volume to import workflows:";
    NSModalResponse res=[openPanel runModal];

    if (res!=NSModalResponseOK) {

        return;
    }

    NSString *openVolumePath=[openPanel URL].path;

    BOOL missingPassword=NO;
    NSData *workflowData=[NSData dataWithContentsOfFile:openVolumePath];
    if (workflowData) {
        if ([[DeploySettings sharedSettings] addWorkflowsFromData:workflowData]==YES){
            missingPassword=YES;
        }
    }

    if (missingPassword==YES) {
        NSAlert * alert=[NSAlert alertWithError:[NSError errorWithDomain:@"TCS" code:0 userInfo:@{NSLocalizedDescriptionKey:@"One or more workflows have a blank user or WiFi password. Please open workflows and enter in passwords as necessary."}]];
        [alert runModal];
    }


}

-(BOOL)workflowsExist{
    if ([[[DeploySettings sharedSettings] workflows] count]>0) return YES;
    else return NO;
}
@end
