//
//  TCSDepProfile.m
//  MDS
//
//  Created by Timothy Perfitt on 8/1/19.
//  Copyright © 2019 Twocanoes Software. All rights reserved.
//

#import "TCSDepProfile.h"
#import <RepackageFramework/TCSRepackage.h>
#import "TCSUtility.h"
#import "MDMDataController.h"
#import "TCSMDMWebController.h"
#import "TCSConfigHelper.h"
#import "MDMUtilities.h"
#define STEP_APPS @"Apps"
#define STEP_SCRIPTS @"Scripts"
#define STEP_PROFILES @"Profiles"
@interface TCSDepProfile()
@property (strong) TCSRepackage *repackage;
@property (strong) NSMutableArray *resourceSteps;
@end
@implementation TCSDepProfile
+ (BOOL)supportsSecureCoding {
    return YES;
}
- (void)encodeWithCoder:(nonnull NSCoder *)encoder {

    [super encodeWithCoder:encoder];
    [encoder encodeObject:self.profileName forKey:@"profileName"];
    [encoder encodeObject:self.supportPhoneNumber forKey:@"supportPhoneNumber"];
    [encoder encodeObject:self.supportEmailAddress forKey:@"supportEmailAddress"];
    [encoder encodeObject:self.mdmServerURL forKey:@"mdmServerURL"];
    [encoder encodeBool:self.shouldIncludeAnchorCert forKey:@"shouldIncludeAnchorCert"];
    [encoder encodeBool:self.isProfileRemovable forKey:@"isProfileRemovable"];

    [encoder encodeBool:self.shouldSkipAppearance forKey:@"shouldSkipAppearance"];
    [encoder encodeBool:self.shouldSkipAppleID forKey:@"shouldSkipAppleID"];
    [encoder encodeBool:self.shouldSkipBiometric forKey:@"shouldSkipBiometric"];
    [encoder encodeBool:self.shouldSkipDiagnostics forKey:@"shouldSkipDiagnostics"];
    [encoder encodeBool:self.shouldSkipDisplayTone forKey:@"shouldSkipDisplayTone"];
    [encoder encodeBool:self.shouldSkipLocation forKey:@"shouldSkipLocation"];
    [encoder encodeBool:self.shouldSkipPayment forKey:@"shouldSkipPayment"];
    [encoder encodeBool:self.shouldSkipPrivacy forKey:@"shouldSkipPrivacy"];
    [encoder encodeBool:self.shouldSkipRestore forKey:@"shouldSkipRestore"];
    [encoder encodeBool:self.shouldSkipScreentime forKey:@"shouldSkipScreentime"];
    [encoder encodeBool:self.shouldSkipSiri forKey:@"shouldSkipSiri"];
    [encoder encodeBool:self.shouldSkipTOS forKey:@"shouldSkipTOS"];
    [encoder encodeBool:self.shouldSkipFilevault forKey:@"shouldSkipFilevault"];
    [encoder encodeBool:self.shouldSkipiCloudDiagnostics forKey:@"shouldSkipiCloudDiagnostics"];
    [encoder encodeBool:self.shouldSkipiCloudStorage forKey:@"shouldSkipiCloudStorage"];
    [encoder encodeBool:self.shouldSkipRegistration forKey:@"shouldSkipRegistration"];

    [encoder encodeBool:self.shouldSkipPrimaryAccountCreation forKey:@"shouldSkipPrimaryAccountCreation"];

    [encoder encodeBool:self.shouldSetPrimaryAccountAsRegularUser forKey:@"shouldSetPrimaryAccountAsRegularUser"];

}

- (nullable instancetype)initWithCoder:(nonnull NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if (!self) {
        return nil;
    }

    self.profileName = [aDecoder decodeObjectForKey:@"profileName"];
    self.supportPhoneNumber = [aDecoder decodeObjectForKey:@"supportPhoneNumber"];
    self.supportEmailAddress = [aDecoder decodeObjectForKey:@"supportEmailAddress"];
    self.mdmServerURL = [aDecoder decodeObjectForKey:@"mdmServerURL"];
    self.shouldIncludeAnchorCert = [aDecoder decodeBoolForKey:@"shouldIncludeAnchorCert"];
    self.isProfileRemovable = [aDecoder decodeBoolForKey:@"isProfileRemovable"];


    self.shouldSkipAppearance = [aDecoder decodeBoolForKey:@"shouldSkipAppearance"];
    self.shouldSkipAppleID = [aDecoder decodeBoolForKey:@"shouldSkipAppleID"];
    self.shouldSkipBiometric = [aDecoder decodeBoolForKey:@"shouldSkipBiometric"];
    self.shouldSkipDiagnostics = [aDecoder decodeBoolForKey:@"shouldSkipDiagnostics"];
    self.shouldSkipDisplayTone = [aDecoder decodeBoolForKey:@"shouldSkipDisplayTone"];
    self.shouldSkipLocation = [aDecoder decodeBoolForKey:@"shouldSkipLocation"];
    self.shouldSkipPayment = [aDecoder decodeBoolForKey:@"shouldSkipPayment"];
    self.shouldSkipPrivacy = [aDecoder decodeBoolForKey:@"shouldSkipPrivacy"];
    self.shouldSkipRestore = [aDecoder decodeBoolForKey:@"shouldSkipRestore"];
    self.shouldSkipScreentime = [aDecoder decodeBoolForKey:@"shouldSkipScreentime"];
    self.shouldSkipSiri = [aDecoder decodeBoolForKey:@"shouldSkipSiri"];
    self.shouldSkipTOS = [aDecoder decodeBoolForKey:@"shouldSkipTOS"];
    self.shouldSkipFilevault = [aDecoder decodeBoolForKey:@"shouldSkipFilevault"];
    self.shouldSkipiCloudDiagnostics = [aDecoder decodeBoolForKey:@"shouldSkipiCloudDiagnostics"];
    self.shouldSkipiCloudStorage = [aDecoder decodeBoolForKey:@"shouldSkipiCloudStorage"];
    self.shouldSkipRegistration = [aDecoder decodeBoolForKey:@"shouldSkipRegistration"];

    self.shouldSkipPrimaryAccountCreation = [aDecoder decodeBoolForKey:@"shouldSkipPrimaryAccountCreation"];

    self.shouldSetPrimaryAccountAsRegularUser = [aDecoder decodeBoolForKey:@"shouldSetPrimaryAccountAsRegularUser"];

    return self;

}
-(void)blueprintWithStatusBlock:(void(^)(NSString *status, float percentComplete))statusBlock returnBlock:(void(^)(NSDictionary *depDict))returnBlock{

    NSFileManager *fm=[NSFileManager defaultManager];
    self.resourceSteps=[NSMutableArray array];
    if (self.usePackagesFolderPath==YES && self.usePackagesFolderPath) {
        [self.resourceSteps addObject:STEP_APPS];
    }

    if (self.shouldEnableARD==YES  || (self.useProfilesFolderPath && self.profilesFolderPath && [fm fileExistsAtPath:self.profilesFolderPath])){
        [self.resourceSteps addObject:STEP_PROFILES];
    }

    NSMutableDictionary *blueprint=[NSMutableDictionary dictionary];

    [blueprint setObject:[[NSUUID UUID] UUIDString] forKey:@"uuid"];
    [blueprint setObject:@"MDS Blueprint" forKey:@"name"];


    [blueprint setObject:@(self.shouldSetPrimaryAccountAsRegularUser) forKey:@"set_primary_setup_account_as_regular_user"];

    [blueprint setObject:@[@"Enroll"] forKey:@"apply_at"];
    NSArray *serialArray=[[[MDMDataController dataController]  allDevices] valueForKey:@"serial"];
    if (serialArray && serialArray.count>0) {
        [blueprint setObject:serialArray forKey:@"devices"];
    }
    if (self.users.count>0){

        NSMutableArray *userUUIDs=[NSMutableArray array];

        [self.users enumerateObjectsUsingBlock:^(TCSWorkflowUser * currUser, NSUInteger idx, BOOL * _Nonnull stop) {

            [userUUIDs addObject:currUser.userUUID];
        }];

        [blueprint setObject:userUUIDs forKey:@"user_uuids"];
        [blueprint setObject:@(self.shouldSkipPrimaryAccountCreation) forKey:@"skip_primary_setup_account_creation"];


    }
//    [[TCSMDMWebController sharedController] addProfileFromPath:@"/Users/tperfitt/Desktop/Resources/DEP-Profiles/Boot Runner Config Profile.mobileconfig" onCompletion:^(BOOL success) {
//
//    }];


    [self updateBlueprint:blueprint statusBlock:statusBlock returnBlock:returnBlock];

}
-(void)updateBlueprint:(NSMutableDictionary *)blueprint statusBlock:(void(^)(NSString *status, float percentComplete))statusBlock returnBlock:(void(^)(NSDictionary *depDict))returnBlock{

    if ([self.resourceSteps containsObject:STEP_APPS]){
        NSError *err;
        NSFileManager *fm=[NSFileManager defaultManager];
        NSArray *contents=[fm contentsOfDirectoryAtPath:self.packagesFolderPath error:&err];
        if(!contents){

            NSLog(@"%@",err.localizedDescription);
        }


        __block NSMutableArray *fullPathArray=[NSMutableArray array];

        [contents enumerateObjectsUsingBlock:^(NSString *currItem, NSUInteger idx, BOOL * _Nonnull stop) {
            [fullPathArray addObject:[self.packagesFolderPath stringByAppendingPathComponent:currItem]];
        }];
        [[MDMUtilities sharedUtilities] setupApps:[NSArray arrayWithArray:fullPathArray] statusBlock:^(NSString *status, float percentComplete) {
            statusBlock(status,percentComplete);
        }
          completeBlock:^(NSArray *manifestArray) {

            NSString *repoResolvedPath=[[[NSUserDefaults standardUserDefaults] objectForKey:MDMSERVERFILEREPOPATH] stringByExpandingTildeInPath];

            NSString *finalPackagePath=[repoResolvedPath stringByAppendingPathComponent:@"com.twocanoes.mds.pkg"];

            NSMutableArray *fullPathScriptsArray=[NSMutableArray array];
            NSError *err;
            NSFileManager *fm=[NSFileManager defaultManager];
            if (self.useScriptFolderPath && self.scriptFolderPath) {
                NSArray *scriptArray=[fm contentsOfDirectoryAtPath:self.scriptFolderPath error:&err];
                   if(err || !scriptArray) {
                       returnBlock(nil);
                       return;
                   }
                   [scriptArray enumerateObjectsUsingBlock:^(NSString *scriptName, NSUInteger idx, BOOL * _Nonnull stop) {

                           [fullPathScriptsArray addObject:[self.scriptFolderPath stringByAppendingPathComponent:scriptName]];
                   }];
                   if (err){
                       err=[NSError errorWithDomain:@"TCS" code:-1 userInfo:@{NSLocalizedDescriptionKey:@"Error creating Scripts"}];
                       NSLog(@"error saving Scripts");
                       returnBlock(nil);
                       return;


                   }

            }

            if([TCSConfigHelper saveScripts:fullPathScriptsArray toPath:finalPackagePath profiles:nil resourcePath:[self.scriptFolderPath stringByAppendingPathComponent:@"Resources"] waitForNetworking:NO shouldSetComputerName:NO workflow:self shouldSetOneTimeSettings:YES shouldSetPasswordHint:NO wifiAlreadySet:YES shouldRunSoftwareUpdate:NO rebootAction:TCSSHUTDOWNOPTIONNONE shouldCreateUsers:NO error:nil]==YES){

                NSString *manifestPath=[TCSConfigHelper manifestForPackagePath:finalPackagePath packageName:finalPackagePath.lastPathComponent];
                manifestArray=[manifestArray arrayByAddingObject:manifestPath];
            }

              [blueprint setObject:manifestArray forKey:@"install_application_manifest_urls"];
            [self.resourceSteps removeObject:STEP_APPS];
            [self updateBlueprint:blueprint statusBlock:statusBlock returnBlock:returnBlock];
          }];


    }
    else if ([self.resourceSteps containsObject:STEP_PROFILES]){


        NSMutableArray *profilePathArray=[NSMutableArray array];
        NSError *err;
        NSFileManager *fm=[NSFileManager defaultManager];

        if (self.useProfilesFolderPath==YES && self.profilesFolderPath) {
            NSArray *dirContents=[fm contentsOfDirectoryAtPath:self.profilesFolderPath error:&err];

            if (!dirContents) {
                return;
            }


            [dirContents enumerateObjectsUsingBlock:^(NSString *currFilename, NSUInteger idx, BOOL * _Nonnull stop) {

                if ([[[currFilename pathExtension] lowercaseString] isEqualToString:@"mobileconfig"]){

                    [profilePathArray addObject:[self.profilesFolderPath stringByAppendingPathComponent:currFilename]];
                }

            }];
        }
        if (self.shouldEnableARD ){

            NSString *ardProfilePath=[[NSBundle mainBundle] pathForResource:@"ard" ofType:@"mobileconfig"];
            [profilePathArray addObject:ardProfilePath];
        }
        [[TCSMDMWebController sharedController] updateProfiles:[NSArray arrayWithArray:profilePathArray] onCompletion:^(NSArray * _Nonnull profiles) {
            if (profiles && profiles.count>0) {
                [blueprint setObject:profiles forKey:@"profile_ids"];

            }

            [self.resourceSteps removeObject:STEP_PROFILES];
            [self updateBlueprint:blueprint statusBlock:statusBlock returnBlock:returnBlock];


        }];

    }
    else {
        NSMutableDictionary *returnDictionary=[NSMutableDictionary dictionary];
        [returnDictionary setObject:blueprint forKey:@"blueprint"];
        returnBlock([NSDictionary dictionaryWithDictionary:returnDictionary]);

    }
}

-(void)profileForDEPWithStatusBlock:(void(^)(NSString *status, float percentComplete))statusBlock returnBlock:(void(^)(NSDictionary *depDict))returnBlock{

    NSMutableDictionary *returnDictionary=[NSMutableDictionary dictionary];

    [returnDictionary setObject:self.profileName forKey:@"profile_name"];
    [returnDictionary setObject:self.mdmServerURL forKey:@"url"];
    if(self.supportPhoneNumber){
        [returnDictionary setObject:self.supportPhoneNumber forKey:@"support_phone_number"];
    }

   if (self.supportEmailAddress) [returnDictionary setObject:self.supportEmailAddress forKey:@"support_email_address"];


    if (self.shouldIncludeAnchorCert==YES){
        NSString *certPath=[USERDEFAULTS objectForKey:CERTIFICATEPATH];
        if (certPath && [[NSFileManager defaultManager] fileExistsAtPath:certPath]){
            NSError *err;
            NSMutableString *certContents=[NSMutableString stringWithContentsOfFile:certPath encoding:NSUTF8StringEncoding error:&err];
            if(!certContents)  {NSLog(@"%@",err.localizedDescription); }
            else {
                [certContents replaceOccurrencesOfString:@"\n" withString:@"" options:NSCaseInsensitiveSearch range:NSMakeRange(0, certContents.length)];
                 [certContents replaceOccurrencesOfString:@"-----BEGIN CERTIFICATE-----" withString:@"" options:NSCaseInsensitiveSearch range:NSMakeRange(0, certContents.length)];
                 [certContents replaceOccurrencesOfString:@"-----END CERTIFICATE-----" withString:@"" options:NSCaseInsensitiveSearch range:NSMakeRange(0, certContents.length)];
                [returnDictionary setObject:@[certContents] forKey:@"anchor_certs"];
            }
        }

    }
    [returnDictionary setObject:@(self.isProfileRemovable) forKey:@"is_mdm_removable"];
    [returnDictionary setObject:@(YES) forKey:@"await_device_configured"];

    NSMutableArray *skipItems=[NSMutableArray array];

    if (self.shouldSkipAppearance==YES)  [skipItems addObject:@"Appearance"];
    if (self.shouldSkipAppleID==YES)  [skipItems addObject:@"AppleID"];
    if (self.shouldSkipBiometric==YES)  [skipItems addObject:@"Biometric"];
    if (self.shouldSkipDiagnostics==YES)  [skipItems addObject:@"Diagnostics"];
    if (self.shouldSkipDisplayTone==YES)  [skipItems addObject:@"DisplayTone"];
    if (self.shouldSkipLocation==YES)  [skipItems addObject:@"Location"];
    if (self.shouldSkipPayment==YES)  [skipItems addObject:@"Payment"];
    if (self.shouldSkipPrivacy==YES)  [skipItems addObject:@"Privacy"];
    if (self.shouldSkipRestore==YES)  [skipItems addObject:@"Restore"];
    if (self.shouldSkipScreentime==YES)  [skipItems addObject:@"ScreenTime"];
    if (self.shouldSkipSiri==YES)  [skipItems addObject:@"Siri"];
    if (self.shouldSkipTOS==YES)  [skipItems addObject:@"TOS"];
    if (self.shouldSkipFilevault==YES)  [skipItems addObject:@"FileVault"];
    if (self.shouldSkipiCloudDiagnostics==YES)  [skipItems addObject:@"iCloudDiagnostics"];
    if (self.shouldSkipiCloudStorage==YES)  [skipItems addObject:@"iCloudStorage"];
    if (self.shouldSkipRegistration==YES)  [skipItems addObject:@"Registration"];

    [returnDictionary setObject:skipItems forKey:@"skip_setup_items"];

    returnBlock([NSDictionary dictionaryWithDictionary:returnDictionary]);
}
-(void)cancel:(id)sender{

    [MDMUtilities sharedUtilities].shouldCancel=YES;

}
@end
