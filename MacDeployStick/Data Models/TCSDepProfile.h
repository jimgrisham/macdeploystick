//
//  TCSDepProfile.h
//  MDS
//
//  Created by Timothy Perfitt on 8/1/19.
//  Copyright © 2019 Twocanoes Software. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TCSWorkflow.h"
NS_ASSUME_NONNULL_BEGIN

@interface TCSDepProfile : TCSWorkflow <NSSecureCoding>


@property (strong) NSString *profileName;
@property (strong) NSString *supportPhoneNumber;
@property (strong) NSString *supportEmailAddress;
@property (strong) NSString *mdmServerURL;
@property (assign) BOOL shouldIncludeAnchorCert;
@property (assign) BOOL isProfileRemovable;

//@property (assign) BOOL shouldUseDEPApps;
//@property (assign) BOOL shouldUseDEPScripts;
//@property (assign) BOOL shouldUseDEPProfiles;


//@property (strong) NSString *depAppsPath;
//@property (strong) NSString *depScriptsPath;
//@property (strong) NSString *depProfilesPath;

@property (assign) BOOL shouldSkipAppearance;
@property (assign) BOOL shouldSkipAppleID;
@property (assign) BOOL shouldSkipBiometric;
@property (assign) BOOL shouldSkipDiagnostics;
@property (assign) BOOL shouldSkipDisplayTone;
@property (assign) BOOL shouldSkipLocation;
@property (assign) BOOL shouldSkipPayment;
@property (assign) BOOL shouldSkipPrivacy;
@property (assign) BOOL shouldSkipRestore;
@property (assign) BOOL shouldSkipScreentime;
@property (assign) BOOL shouldSkipSiri;
@property (assign) BOOL shouldSkipTOS;
@property (assign) BOOL shouldSkipFilevault;
@property (assign) BOOL shouldSkipiCloudDiagnostics;
@property (assign) BOOL shouldSkipiCloudStorage;
@property (assign) BOOL shouldSkipRegistration;

@property (assign) BOOL shouldSkipPrimaryAccountCreation;
@property (assign) BOOL shouldSetPrimaryAccountAsRegularUser;



-(void)cancel:(id)sender;
-(void)blueprintWithStatusBlock:(void(^)(NSString *status, float percentComplete))statusBlock returnBlock:(void(^)(NSDictionary *depDict))returnBlock;
-(void)profileForDEPWithStatusBlock:(void(^)(NSString *status, float percentComplete))statusBlock returnBlock:(void(^)(NSDictionary *depDict))returnBlock;
@end

NS_ASSUME_NONNULL_END
