//
//  TCSWorkflowTableViewController.m
//  MDS
//
//  Created by Timothy Perfitt on 3/9/19.
//  Copyright © 2019 Twocanoes Software. All rights reserved.
//

#import "TCSChromeBookTableViewDatasource.h"

@interface TCSChromeBookTableViewDatasource ()
@property (strong) NSArray *tableItems;
@property (assign) NSInteger selectedRow;
@end

@implementation TCSChromeBookTableViewDatasource
- (instancetype)init
{
    self = [super init];
    if (self) {
        self.tableItems=@[
                          @{@"name":@"ChromeOS Version",@"image":@"chromebook"},
                          @{@"name":@"WiFi",@"image":@"wifi"},
                          @{@"name":@"Google Enterprise",@"image":@"cloud"}];

    }
    return self;
}

    // Do view setup here.

- (NSInteger)numberOfRowsInTableView:(NSTableView *)tableView{


    return self.tableItems.count;
}
-(void)tableViewSelectionDidChange:(NSNotification *)inNot{

    NSTableView *tableView=[inNot object];
    self.selectedRow=tableView.selectedRow;
}
- (NSView *)tableView:(NSTableView *)tableView
   viewForTableColumn:(NSTableColumn *)tableColumn
                  row:(NSInteger)row {

    // Retrieve to get the @"MyView" from the pool or,
    // if no version is available in the pool, load the Interface Builder version
    NSTableCellView *result = [tableView makeViewWithIdentifier:@"ChromebookView" owner:self];

    // Set the stringValue of the cell's text field to the nameArray value at row
    NSDictionary *currentObject=[self.tableItems objectAtIndex:row];
    result.textField.stringValue =currentObject[@"name"];
    NSImage *rowImage=[NSImage imageNamed:currentObject[@"image"]];

    [rowImage setTemplate:YES];
    [result.imageView setImage:rowImage];    // Return the result
    return result;
}
@end
