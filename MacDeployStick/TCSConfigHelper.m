//
//  TCSConfigHelper.m
//  MacDeployStick
//
//  Created by Timothy Perfitt on 1/30/19.
//  Copyright © 2019 Twocanoes Software. All rights reserved.
//

#import "TCSConfigHelper.h"
#import "TCSUtility.h"
#import "TCSDefaultsManager.h"
@implementation TCSConfigHelper
+(NSDictionary *)WifiConfigWithSSID:(NSString *)ssid password:(NSString *)password{

    NSBundle *mainBundle=[NSBundle mainBundle];
    NSString *wifiConfigPath=[mainBundle pathForResource:@"wifi_config" ofType:@"plist"];


    NSMutableDictionary *wifiConfigDict=[NSMutableDictionary dictionaryWithContentsOfFile:wifiConfigPath] ;

    if (!wifiConfigDict) {
        NSLog(@"%@",@"Error reading dictionary");
        return nil;
    }


    NSUUID *uuid1=[[NSUUID alloc] init];
    NSUUID *uuid2=[[NSUUID alloc] init];
    NSUUID *uuid3=[[NSUUID alloc] init];
    NSUUID *uuid4=[[NSUUID alloc] init];

    wifiConfigDict[@"PayloadUUID"]=uuid1.UUIDString;
    wifiConfigDict[@"PayloadIdentifier"]=[NSString stringWithFormat:@"twocanoes.com.%@",uuid2.UUIDString];
    wifiConfigDict[@"PayloadContent"][0][@"PayloadIdentifier"]=[NSString stringWithFormat:@"twocanoes.com.%@.com.apple.wifi.managed",uuid3.UUIDString];
    wifiConfigDict[@"PayloadContent"][0][@"PayloadUUID"]=uuid4.UUIDString;
    wifiConfigDict[@"PayloadContent"][0][@"SSID_STR"]=ssid;
    wifiConfigDict[@"PayloadContent"][0][@"Password"]=password;

    return [NSDictionary dictionaryWithDictionary:wifiConfigDict];


}
+(NSString *)manifestForPackagePath:(NSString *)finalPackagePath packageName:(NSString *)packageName{
    NSArray *hashArray=[TCSUtility md5HashArrayFromFile:finalPackagePath];
                      NSString *manifestFilePath=[[finalPackagePath stringByDeletingPathExtension] stringByAppendingPathExtension:@"plist"];

                      NSString *urlComponents=[[[packageName stringByDeletingPathExtension] stringByAppendingPathExtension:@"pkg"] stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLPathAllowedCharacterSet]];

                      NSString *urlComponentsManifest=[[@"repo" stringByAppendingPathComponent:[[packageName stringByDeletingPathExtension] stringByAppendingPathExtension:@"plist"]] stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLPathAllowedCharacterSet]];

                      NSString *serverURL=[NSString stringWithFormat:@"https://%@:%@",[USERDEFAULTS objectForKey:SERVERHOSTNAME],[USERDEFAULTS objectForKey:MDMSERVERPORT]];

    NSString *finalPackageURLString=[NSString stringWithFormat:@"%@/repo/%@",serverURL,urlComponents];

                      NSString *finalManifestURLString=[NSString stringWithFormat:@"%@/%@",serverURL,urlComponentsManifest];

                      NSDictionary *manifestDict=[TCSUtility manifestForURL:finalPackageURLString fileHashArray:hashArray];

                      [manifestDict writeToFile:manifestFilePath atomically:NO];
    return finalManifestURLString;
}
+(BOOL)savePackageNamed:(NSString *)inName path:(NSString *)inPath fromProfile:(NSDictionary *)inProfile{

    NSError *err;
    NSString *tempFolder=[[TCSDefaultsManager sharedManager] newTempFolder];

    if (!tempFolder) return NO;

    NSFileManager *fm=[NSFileManager defaultManager];


    NSString *rootFolderPath=[tempFolder stringByAppendingPathComponent:@"root"];
    NSString *scriptFolderPath=[tempFolder stringByAppendingPathComponent:@"script_root"];

    if([fm createDirectoryAtPath:rootFolderPath withIntermediateDirectories:YES attributes:nil error:&err]==NO){

        NSLog(@"Error creating %@ with error:%@",rootFolderPath,err.localizedDescription);
        return NO;

    }
    if([fm createDirectoryAtPath:scriptFolderPath withIntermediateDirectories:YES attributes:nil error:&err]==NO){

        NSLog(@"Error creating %@ with error:%@",scriptFolderPath,err.localizedDescription);
        return NO;

    }

    NSString *savePath=[tempFolder stringByAppendingPathComponent:[inName stringByAppendingPathExtension:@"pkg"]];

    NSString *saveFile=[rootFolderPath stringByAppendingPathComponent:[inName stringByAppendingPathExtension:@"mobileconfig"]];
    if([inProfile writeToFile:saveFile atomically:NO]==NO){
        NSLog(@"Error writing %@",saveFile);
        return NO;

    }

    NSString *script=[NSString stringWithFormat:@"#!/bin/bash\n/usr/bin/profiles install -type configuration -path \"/tmp/%@.mobileconfig\"\nrm \"/tmp/%@.mobileconfig\"",inName,inName];

    if([script writeToFile:[scriptFolderPath stringByAppendingPathComponent:@"postinstall"] atomically:NO encoding:NSUTF8StringEncoding error:&err]==NO){

        NSLog(@"%@",err.localizedDescription);

        return NO;
    }

    [fm setAttributes:@{NSFilePosixPermissions:[NSNumber numberWithShort:0755]} ofItemAtPath:[scriptFolderPath stringByAppendingPathComponent:@"postinstall"] error:&err];

    double version=[[NSDate date] timeIntervalSince1970];
    NSTask *productBuildTask=[[NSTask alloc] init];
    productBuildTask.launchPath=@"/usr/bin/pkgbuild";
    productBuildTask.arguments=@[@"--install-location",@"/private/tmp",@"--root",rootFolderPath,@"--identifier",[NSString stringWithFormat:@"com.twocanoes.%@",inName],@"--version",[NSString stringWithFormat:@"%fd",version],@"--scripts",scriptFolderPath,savePath];
    [productBuildTask launch];
    [productBuildTask waitUntilExit];


    if (productBuildTask.terminationStatus!=0) {

        NSLog(@"Could not build package: %i",productBuildTask.terminationStatus);
        return NO;

    }
    NSTask *packageConvertTask=[[NSTask alloc] init];
    packageConvertTask.launchPath=@"/usr/bin/productbuild";
    packageConvertTask.arguments=@[@"--package",savePath,inPath];
    [packageConvertTask launch];
    [packageConvertTask waitUntilExit];

    if (packageConvertTask.terminationStatus!=0) {

        NSLog(@"Could not embed meta package: %i",packageConvertTask.terminationStatus);
        return NO;

    }

    return YES;

}

+(NSString *)applicationSupportPath{
    NSArray *applicationSupportPath = NSSearchPathForDirectoriesInDomains(NSApplicationSupportDirectory, NSUserDomainMask, YES);

       NSString *appSupportDir=[applicationSupportPath[0] stringByAppendingPathComponent:@"com.twocanoes.macdeploystick"];
    return appSupportDir;

}
+(NSString *)imagrPath{
    NSArray *applicationSupportPath = NSSearchPathForDirectoriesInDomains(NSApplicationSupportDirectory, NSUserDomainMask, YES);

    NSString *appSupportDir=[applicationSupportPath[0] stringByAppendingPathComponent:@"com.twocanoes.macdeploystick"];

    return [appSupportDir stringByAppendingPathComponent:@"Imagr.app"];

}

+(NSString *)imagrVersion{

    return [TCSConfigHelper imagrVersionAtPath:nil];
}
+(NSString *)imagrVersionAtPath:(nullable NSString *)inImgrPath{
    NSString *imagrPath=inImgrPath;
    if (imagrPath==nil) {
        imagrPath=[TCSConfigHelper imagrPath];
    }

    NSDictionary *imagrDict=[NSDictionary dictionaryWithContentsOfFile:[[imagrPath stringByAppendingPathComponent:@"Contents"] stringByAppendingPathComponent:@"Info.plist"]];
    if (!imagrDict) return nil;
    return imagrDict[@"CFBundleVersion"];

}
+(BOOL)checkIfResourcesNeeded{
    NSFileManager *fm=[NSFileManager defaultManager];

    NSString *imagrPath=[TCSConfigHelper imagrPath];

    if ([fm fileExistsAtPath:imagrPath]) return NO;
    return YES;

}
+(BOOL)saveScripts:( NSArray * )inScripts toPath:(NSString *)destinationPackagePath profiles:(NSArray *)inProfiles resourcePath:(NSString *)resourcesPath waitForNetworking:(BOOL)shouldWaitForNetworking shouldSetComputerName:(BOOL)shouldSetComputerName workflow:(TCSWorkflow * __nullable)inWorkflow shouldSetOneTimeSettings:(BOOL)shouldSetOneTimeSettings shouldSetPasswordHint:(BOOL)shouldSetPasswordHint wifiAlreadySet:(BOOL)wifiAlreadySet shouldRunSoftwareUpdate:(BOOL)shouldRunSoftwareUpdate
      rebootAction:(int)rebootIndex shouldCreateUsers:(BOOL)shouldCreateUsers
             error:(NSError **)err{
    NSString *tempFolder=[[TCSDefaultsManager sharedManager] newTempFolder];

    if (!tempFolder) return NO;

    NSFileManager *fm=[NSFileManager defaultManager];

    NSString *rootFolderPath=[tempFolder stringByAppendingPathComponent:@"root"];
    NSString *scriptFolderPath=[tempFolder stringByAppendingPathComponent:@"script_root"];
    NSString *additionalScriptFolderDestination=[rootFolderPath stringByAppendingPathComponent:@"com.twocanoes.mds.scripts"];

    NSString *additionalScriptResourceDestinationPath=[additionalScriptFolderDestination stringByAppendingPathComponent:@"Resources"];

    //this will create the additionalScriptFolderDestination as well
    if (![fm fileExistsAtPath:additionalScriptResourceDestinationPath]){

        if([fm createDirectoryAtPath:additionalScriptResourceDestinationPath withIntermediateDirectories:YES attributes:nil error:err]==NO) {
            NSLog(@"%@",[*err localizedDescription]);
            return NO;


        }
    }


    if([fm createDirectoryAtPath:rootFolderPath withIntermediateDirectories:YES attributes:nil error:err]==NO){

        NSLog(@" Error creating %@ with error:%@",rootFolderPath,[*err localizedDescription]);
        return NO;

    }
    if([fm createDirectoryAtPath:scriptFolderPath withIntermediateDirectories:YES attributes:nil error:err]==NO){

        NSLog(@" Error creating %@ with error:%@",scriptFolderPath,[*err localizedDescription]);
        return NO;

    }


    BOOL isDir;
    NSMutableArray *configSettings=[NSMutableArray array];

    switch (rebootIndex) {
        case TCSSHUTDOWNOPTIONREBOOT:
            [configSettings addObject:@"worksflows_should_reboot=1"];

            break;
        case TCSSHUTDOWNOPTIONSHUTDOWN:
            [configSettings addObject:@"worksflows_should_shutdown=1"];

            break;

        default:
            break;
    }

    if(shouldRunSoftwareUpdate==YES){
        [configSettings addObject:@"worksflows_should_run_software_update=1"];
    }

    if (inWorkflow.users.count>0){
        NSString *sshKeyFilesPath=[rootFolderPath stringByAppendingPathComponent:@"sshkeys"];

        if([fm createDirectoryAtPath:sshKeyFilesPath withIntermediateDirectories:YES attributes:nil error:err]==NO){
            NSLog(@"%@",[*err localizedDescription]);
            return NO;
        }

        __block BOOL didCreate=NO;
        __block NSError *loopErr=nil;
       [inWorkflow.users enumerateObjectsUsingBlock:^(TCSWorkflowUser *currUser, NSUInteger idx, BOOL * _Nonnull stop) {

           if (currUser.sshKey) {
               NSString *currKeyPath=[sshKeyFilesPath stringByAppendingPathComponent:currUser.shortName];
               if([currUser.sshKey writeToFile:currKeyPath atomically:NO encoding:NSUTF8StringEncoding error:&loopErr]==NO){
                   NSLog(@"%@",[loopErr localizedDescription]);
                   *stop=YES;

               }
               didCreate=YES;
           }
       }];
        if (loopErr !=nil) {
            *err=loopErr;
            return NO;
        }

        if (didCreate==YES) {
            [configSettings addObject:@"workflow_should_create_user=1"];
        }
    }

    if (wifiAlreadySet==NO && inWorkflow.shouldConfigureWifi==YES && inWorkflow.wifiSSID && inWorkflow.wifiPassword) {
        [configSettings addObject:@"workflow_should_configure_wifi=1"];
        [configSettings addObject:[NSString stringWithFormat:@"workflow_wifi_ssid=\'%@\'",inWorkflow.wifiSSID]];
        [configSettings addObject:[NSString stringWithFormat:@"workflow_wifi_password=\'%@\'",inWorkflow.wifiPassword]];
    }
    if (shouldWaitForNetworking==YES) {
        [configSettings addObject:@"workflows_should_wait_for_network=1"];

    }
    if (shouldSetComputerName==YES){
        [configSettings addObject:@"workflows_should_set_computer_name=1"];
    }
    if (shouldSetOneTimeSettings) {

        if (shouldSetPasswordHint==YES) {
            [configSettings addObject:@"worksflows_should_enable_password_hints=1"];

        }
        [configSettings addObject:@"workflows_remove_nvram_vars=1"];

        if ( inWorkflow.shouldTrustServerCertificate==YES && [[NSUserDefaults standardUserDefaults] objectForKey:CERTIFICATEPATH] && [[NSUserDefaults standardUserDefaults] objectForKey:INDENTITYKEYPATH]){

                [configSettings addObject:@"workflows_should_trust_server_certificate=1"];

                if([fm copyItemAtPath:[[NSUserDefaults standardUserDefaults] objectForKey:CERTIFICATEPATH] toPath:[rootFolderPath stringByAppendingPathComponent:@"server_certificate.pem"] error:err]==NO){
                    return NO;
                }
            }


        if (inWorkflow.shouldTrustMunkiClientCertificate && inWorkflow.munkiClientCertificate && [fm fileExistsAtPath:inWorkflow.munkiClientCertificate]){

            [configSettings addObject:@"workflows_should_trust_munki_client_certificate=1"];

            if([fm copyItemAtPath:inWorkflow.munkiClientCertificate toPath:[rootFolderPath stringByAppendingPathComponent:@"munki_certificate.pem"] error:err]==NO){
                return NO;
            }
        }
        if (inWorkflow.shouldEnableARD==YES){
            [configSettings addObject:@"workflows_should_enable_ard=1"];

        }
        if (inWorkflow.shouldEnableSSH==YES) {
            [configSettings addObject:@"workflows_should_enable_ssh=1"];
        }
        if (inWorkflow.shouldConfigureMunkiClient==YES && inWorkflow.munkiWorkflowURL && inWorkflow.munkiWorkflowURL.length>0) {
            NSString *repoURL=inWorkflow.munkiWorkflowURL;
            if ([[repoURL substringFromIndex:repoURL.length-1] isEqualToString:@"/"]){
                repoURL=[inWorkflow.munkiWorkflowURL substringToIndex:repoURL.length-1];
            }


                [configSettings addObject:[NSString stringWithFormat:@"workflows_munki_workflow_url=\'%@\'",repoURL]];

        }

        if (inWorkflow.shouldSkipSetupAssistant==YES){
            [configSettings addObject:@"workflows_should_skip_setup_assistant=1"];
            if(inWorkflow.shouldEnableLocationServices==YES) {
                [configSettings addObject:@"workflows_should_enable_location_services=1"];
            }


        }
        if (inWorkflow.shouldSkipPrivacySetup==YES){
            [configSettings addObject:@"workflows_should_skip_privacy_setup=1"];

        }

        if (inWorkflow.shouldEnableScreenSharing==YES) {
            [configSettings addObject:@"workflows_should_enable_screen_sharing=1"];

        }
    }

    __block int totalScripts=0;
    [inScripts enumerateObjectsUsingBlock:^(NSString *currScriptPath, NSUInteger idx, BOOL * _Nonnull stop) {
        BOOL isDir;

        if ([fm fileExistsAtPath:currScriptPath isDirectory:&isDir] && isDir==NO && [[currScriptPath lastPathComponent] hasPrefix:@"."]==NO){

            NSString *scriptName=[NSString stringWithFormat:@"1%02li-%@",idx,currScriptPath.lastPathComponent];
            NSError *err;
            totalScripts++;
            if ( [fm copyItemAtPath:currScriptPath toPath:[additionalScriptFolderDestination stringByAppendingPathComponent:scriptName] error:&err]==NO) {
                NSLog(@"%@",err.localizedDescription);
                *stop=YES;
            }

            if ([fm isExecutableFileAtPath:[additionalScriptFolderDestination stringByAppendingPathComponent:scriptName]]==NO){
                NSError *err;
                if([fm setAttributes:@{NSFilePosixPermissions:[NSNumber numberWithShort:0755]} ofItemAtPath:[additionalScriptFolderDestination stringByAppendingPathComponent:scriptName] error:&err]==NO) {
                    NSLog(@" %@",err.localizedDescription);
                }
            }
        }
    }];

    NSUserDefaults *ud=[NSUserDefaults standardUserDefaults];
    NSString *loggingURLString;
    if([ud boolForKey:@"shouldSendLoggingInfo"]==YES && [ud objectForKey:@"loggingInfoURLString"] && [[ud objectForKey:@"loggingInfoURLString"] length]>0 && totalScripts>0 && inProfiles.count>0 ){

        loggingURLString=[ud objectForKey:@"loggingInfoURLString"];
        [configSettings addObject:[NSString stringWithFormat:@"logging_url_string=\'%@\'",loggingURLString]];

    }

    if (configSettings.count==0 && totalScripts==0 && inProfiles.count==0 ) {

        NSLog(@"nothing to save.");
        return NO;
    }
    NSString *configSettingsString=[configSettings componentsJoinedByString:@"\n"];

    if([configSettingsString writeToFile:[rootFolderPath stringByAppendingPathComponent:@"com_twocanoes_mds_workflow_script_config.sh"] atomically:NO encoding:NSUTF8StringEncoding error:err]==NO){

        NSLog(@"%@",[*err localizedDescription]);
        return NO;

    }



    if([@"#!/bin/bash\n\necho $1 $2 $3\ncd \"$2\"\n./com.twocanoes.mds.main.sh \"$1\" \"$2\" \"$3\"\n" writeToFile:[scriptFolderPath stringByAppendingPathComponent:@"postinstall"] atomically:NO encoding:NSUTF8StringEncoding error:err]==NO){

        NSLog(@"%@",[*err localizedDescription]);

        return NO;
    }



    NSBundle *mainBundle=[NSBundle mainBundle];
    NSString *mainScriptPath=[mainBundle pathForResource:@"com.twocanoes.mds.main" ofType:@".sh"];
    if ( [fm copyItemAtPath:mainScriptPath toPath:[rootFolderPath stringByAppendingPathComponent:@"com.twocanoes.mds.main.sh"] error:err]==NO) {
        NSLog(@"%@",[*err localizedDescription]);
        return NO;

    }


    [fm setAttributes:@{NSFilePosixPermissions:[NSNumber numberWithShort:0755]} ofItemAtPath:[scriptFolderPath stringByAppendingPathComponent:@"postinstall"] error:err];
    if ([fm fileExistsAtPath:resourcesPath isDirectory:&isDir] && isDir==YES ) {

        NSArray *scriptResourceFolderArray=[fm contentsOfDirectoryAtPath:resourcesPath error:err];

        [scriptResourceFolderArray enumerateObjectsUsingBlock:^(NSString *currResourceName, NSUInteger idx, BOOL * _Nonnull stop) {
            NSLog(@"currResourceName : %@",currResourceName);
            NSError *err;

            if ( [fm copyItemAtPath:[resourcesPath stringByAppendingPathComponent:currResourceName] toPath:[additionalScriptResourceDestinationPath stringByAppendingPathComponent:currResourceName] error:&err]==NO) {
                NSLog(@"%@",err.localizedDescription);
                *stop=YES;


            }
        }];

    }

    NSString *profilesResourcesFolderDestinationPath=[rootFolderPath stringByAppendingPathComponent:@"com.twocanoes.mds.profiles"];
    if (![fm fileExistsAtPath:profilesResourcesFolderDestinationPath]){

        if([fm createDirectoryAtPath:profilesResourcesFolderDestinationPath withIntermediateDirectories:YES attributes:nil error:err]==NO) {
            NSLog(@"%@",[*err localizedDescription]);
            return NO;


        }
    }
    [inProfiles enumerateObjectsUsingBlock:^(NSString *currProfilePath, NSUInteger idx, BOOL * _Nonnull stop) {
        NSError *err;
        NSString *profileName=[NSString stringWithFormat:@"1%02li-%@",idx,currProfilePath.lastPathComponent];

        if ( [fm copyItemAtPath:currProfilePath toPath:[profilesResourcesFolderDestinationPath stringByAppendingPathComponent:profileName] error:&err]==NO) {
            NSLog(@"%@",err.localizedDescription);
            *stop=YES;


        }
    }];

    NSString *savePath=[tempFolder stringByAppendingPathComponent:[destinationPackagePath lastPathComponent]];
    NSUUID *destinationUUID=[[NSUUID alloc] init];
    NSString *destinationUUIDString=destinationUUID.UUIDString;

    double version=[[NSDate date] timeIntervalSince1970];
    NSTask *productBuildTask=[[NSTask alloc] init];
    productBuildTask.launchPath=@"/usr/bin/pkgbuild";
    productBuildTask.arguments=@[@"--install-location",[@"/private/tmp" stringByAppendingPathComponent:destinationUUIDString],@"--root",rootFolderPath,@"--identifier",[destinationPackagePath lastPathComponent],@"--version",[NSString stringWithFormat:@"%fd",version],@"--scripts",scriptFolderPath,savePath];
    [productBuildTask launch];
    [productBuildTask waitUntilExit];


    if (productBuildTask.terminationStatus!=0) {

        NSLog(@"Could not build package: %i",productBuildTask.terminationStatus);
        return NO;

    }
    BOOL shouldSign=[ud boolForKey:@"shouldSignPackages"];
    NSString *signingIdentity=[ud objectForKey:@"signingIdentity"];

    NSTask *packageConvertTask=[[NSTask alloc] init];
    packageConvertTask.launchPath=@"/usr/bin/productbuild";

    if (shouldSign==YES && signingIdentity){
        packageConvertTask.arguments=@[@"--sign",signingIdentity,@"--package",savePath,destinationPackagePath];
    }
    else {
        packageConvertTask.arguments=@[@"--package",savePath,destinationPackagePath];

    }
    [packageConvertTask launch];
    [packageConvertTask waitUntilExit];

    if (packageConvertTask.terminationStatus!=0) {

        NSLog(@"Could not embed meta package: %i",packageConvertTask.terminationStatus);
        return NO;

    }
    return YES;
}
+(void)scriptAddReport:(NSMutableString *)inScript withMessage:(NSString *)inMessage{

    NSUserDefaults *ud=[NSUserDefaults standardUserDefaults];
    if([ud boolForKey:@"shouldSendLoggingInfo"]==YES && [ud objectForKey:@"loggingInfoURLString"] && [[ud objectForKey:@"loggingInfoURLString"] length]>0){



        NSString *loggingURLString=[ud objectForKey:@"loggingInfoURLString"];

        NSString *encodedString=[inMessage stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];

        [inScript appendString:[NSString stringWithFormat:@"if [ -n  \"${network_available}\"  ] ; then\n      /usr/bin/curl -X POST -d \"message=%@&serial=${serial}&status=script_info\" %@\nfi\n\n",encodedString,loggingURLString]];
    }

}
@end


