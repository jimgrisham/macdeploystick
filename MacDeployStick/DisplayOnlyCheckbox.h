//
//  DisplayOnlyCheckbox.h
//  MDS
//
//  Created by Timothy Perfitt on 9/2/19.
//  Copyright © 2019 Twocanoes Software. All rights reserved.
//

#import <Cocoa/Cocoa.h>

NS_ASSUME_NONNULL_BEGIN

@interface DisplayOnlyCheckbox : NSButton

@end

NS_ASSUME_NONNULL_END
