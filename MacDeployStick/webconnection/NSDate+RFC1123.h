//
//  NSDate+NSDate_RFC1123.h
//  Locamotion
//
//  Created by Tim Perfitt on 9/26/12.
//  Copyright (c) 2012 Twocanoes Software, Inc. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDate (RFC1123)

/**
 Convert a RFC1123 'Full-Date' string
 (http://www.w3.org/Protocols/rfc2616/rfc2616-sec3.html#sec3.3.1)
 into NSDate.
 */
+(NSDate*)dateFromRFC1123:(NSString*)value_;

/**
 Convert NSDate into a RFC1123 'Full-Date' string
 (http://www.w3.org/Protocols/rfc2616/rfc2616-sec3.html#sec3.3.1).
 */
-(NSString*)rfc1123String;

@end
