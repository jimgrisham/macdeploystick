//
//  NSURLRequest+TCSSignedURLRequest.h
//  webconnection
//
//  Created by Steve Brokaw on 3/28/14.
//  Copyright (c) 2014 Twocanoes Softoware, inc. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSURLRequest (TCSSignedURLRequest)

+ (instancetype)signedRequestWithURL:(NSURL *)URL method:(NSString *)method payload:(NSDictionary *)payload authKey:(NSString *)authKey authId:(NSString *)authId;
+ (instancetype)signedGETRequestWithURL:(NSURL *)URL authKey:(NSString *)authKey authId:(NSString *)authId;
+ (instancetype)signedPOSTRequestWithURL:(NSURL *)URL payload:(NSDictionary *)payload authKey:(NSString *)authKey authId:(NSString *)authId;
//+ (instancetype)signedPUTRequestWithURL:(NSURL *)URL payload:(NSDictionary *)payload authKey:(NSString *)authKey authId:(NSString *)authId;

@end
