//
//  TCWebConnection.h
//  Locamotion
//
//  Created by Tim Perfitt on 9/15/12.
//  Copyright (c) 2012 Twocanoes Software, Inc. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TCWebConnection.h"

@class TCWebConnection;

typedef void (^TCWebCompletionBlock)(NSInteger responseCode, NSData *responseData);
typedef void (^TCWebErrorBlock)(NSError *error);

@interface TCWebConnection : NSObject <NSURLConnectionDelegate>

@property (readonly) NSInteger httpCode;

/**
 */
- (id)initWithBaseURL:(NSURL *)baseURL;


/**
 Legacy API for backward compatability. If you require auth, use the other one.
 
 @param path The resource path relative to the host
 @param method The HTTP method (GET, POST, PUT, DELETE)
 @param payload The payload.
 @param authRequired Use HMAC authentication
 @param completionBlock Call on successfull completion
 @param errorBlock Called on error.
 
 @return Returns NO if a connection could not be attempted (because of invalid
 parameters, for example). Returns YES if the connection was started. The error
 block will be called for further errors.
 */
- (BOOL)sendRequestToPath:(NSString *)path
                     type:(NSString *)method
                  payload:(NSDictionary *)payload
               useAPIAuth:(BOOL)authRequired
             onCompletion:(TCWebCompletionBlock)completionBlock
                  onError:(TCWebErrorBlock)errorBlock;

/**
 @param path The resource path relative to the host
 @param method The HTTP method (GET, POST, PUT, DELETE)
 @param payload The payload.
 @param authId The authentication ID to be used in the header.
 @param authKey The authentication Key to be used to generate the signature..
 @param completionBlock Called on successfull completion.
 @param errorBlock Called on error.
 
 @return Returns NO if a connection could not be attempted (because of invalid
 parameters, for example). Returns YES if the connection was started. The error
 block will be called for further errors.
*/
- (BOOL)sendRequestToPath:(NSString *)path
                     type:(NSString *)method
                  payload:(NSDictionary *)payload
                   authId:(NSString *)authId
                  authKey:(NSString *)authKey
             onCompletion:(TCWebCompletionBlock)completionBlock
                  onError:(TCWebErrorBlock)errorBlock;

- (BOOL)sendRequestToPath:(NSString *)path
                     type:(NSString *)method
                  payload:(NSDictionary *)payload
        basicAuthUsername:(NSString *)username
        basicAuthPassword:(NSString *)password
             onCompletion:(TCWebCompletionBlock)completionBlock
                  onError:(TCWebErrorBlock)errorBlock;

@end
