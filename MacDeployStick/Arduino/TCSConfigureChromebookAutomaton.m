//
//  TCSCreateAutobot.m
//  MDS
//
//  Created by Timothy Perfitt on 5/8/19.
//  Copyright © 2019 Twocanoes Software. All rights reserved.
//

#import "TCSConfigureChromebookAutomaton.h"
#import "TCSChromeBookTableViewDatasource.h"
struct settings_chromebook_t
{
    uint16 version;
    uint16 device_type;
    char wifi_name[100];
    char wifi_password[100];
    uint16 wifi_security;
    char username[100];
    char password[100];
    bool autorun;
    bool advanced_network_setup;
    uint16 eap_method;
    uint16 phase2_authentication;
    bool server_ca_check;
    char subject_match[100];
    char identity[100];
    char anonymous_identity[100];
} __attribute__((packed));




@interface TCSConfigureChromebookAutomaton ()
@property (assign) NSInteger version;
@property (assign) NSInteger deviceType;
@property (strong) NSString *wifiName;
@property (strong) NSString *wifiPassword;
@property (assign) NSInteger wifiSecurity;
@property (strong) NSString *username;
@property (strong) NSString *password;
@property (assign) BOOL shouldAutorun;
@property (assign) BOOL advancedNetworkSetup;
@property (assign) NSInteger eapMethod;
@property (assign) NSInteger phase2Authentication;
@property (assign) BOOL serverCACheck;
@property (strong) NSString *subjectMatch;
@property (strong) NSString *identity;
@property (strong) NSString *anonymousIdentity;

@property (strong) IBOutlet TCSChromeBookTableViewDatasource *chromebookTableViewDatasource;
@property (strong) IBOutlet NSObjectController *passwordShowHideObjectController;
@property (strong) IBOutlet NSObjectController *selectedRowObjectController;

@property (strong) IBOutlet NSUserDefaultsController *userDefaultsObjectController;

@end

@implementation TCSConfigureChromebookAutomaton

- (void)viewDidLoad {
    
    [super viewDidLoad];
    // Do view setup here.
}

-(NSInteger)automatonVersion{
    NSBundle *mainBundle=[NSBundle mainBundle];

    NSInteger automatonVersion=[[[mainBundle infoDictionary] objectForKey:@"ChromebookAutomatonFirmwareVersion"] integerValue];
    return automatonVersion;
}
-(struct settings_t *)settings{

    struct settings_t *settings=malloc(sizeof(struct settings_chromebook_t));
    return settings;

}
-(NSInteger)settingsSize{
     return sizeof(struct settings_chromebook_t);

}
-(void)updateWithSettings:(void *)inSettings{

    struct settings_chromebook_t *settings=(struct settings_chromebook_t *)inSettings;

    self.version=settings->version;
    self.deviceType=settings->device_type;
    self.wifiName=[NSString stringWithUTF8String:settings->wifi_name];
    self.wifiPassword=[NSString stringWithUTF8String:settings->wifi_password];
    self.wifiSecurity=settings->wifi_security;
    self.username=[NSString stringWithUTF8String:settings->username];
    self.password=[NSString stringWithUTF8String:settings->password];
    self.shouldAutorun=settings->autorun;

    self.advancedNetworkSetup=settings->advanced_network_setup;
    self.eapMethod=settings->eap_method;
    self.phase2Authentication=settings->phase2_authentication;
    self.serverCACheck=settings->server_ca_check;
    self.subjectMatch=[NSString stringWithUTF8String:settings->subject_match];
    self.identity=[NSString stringWithUTF8String:settings->identity];
    self.anonymousIdentity=[NSString stringWithUTF8String:settings->anonymous_identity];


    self.dataLoaded=YES;

}
- (IBAction)updateArduino:(id)sender {

    if (self.wifiSecurity==3) {
        self.advancedNetworkSetup=YES;
    }
    struct settings_chromebook_t *new_settings=(struct settings_chromebook_t *)[self settings];


    if (!self.wifiName) self.wifiName=@"";
    if (!self.wifiPassword) self.wifiPassword=@"";
    if (!self.username) self.username=@"";
    if (!self.password) self.password=@"";
    if (!self.subjectMatch) self.subjectMatch=@"";
    if (!self.identity) self.identity=@"";
    if (!self.anonymousIdentity) self.anonymousIdentity=@"";

    new_settings->version=self.version;
    new_settings->device_type=self.deviceType;
    new_settings->wifi_security=self.wifiSecurity;
    new_settings->autorun=self.shouldAutorun;

    strlcpy(new_settings->wifi_name, self.wifiName.UTF8String, sizeof(new_settings->wifi_name));
    strlcpy(new_settings->wifi_password, self.wifiPassword.UTF8String, sizeof(new_settings->wifi_password));
    strlcpy(new_settings->username, self.username.UTF8String, sizeof(new_settings->username));
    strlcpy(new_settings->password, self.password.UTF8String, sizeof(new_settings->password));

    new_settings->advanced_network_setup=self.advancedNetworkSetup;
    new_settings->eap_method=self.eapMethod;
    new_settings->phase2_authentication=self.phase2Authentication;
    new_settings->server_ca_check=self.serverCACheck;
    strlcpy(new_settings->subject_match, self.subjectMatch.UTF8String, sizeof(new_settings->subject_match));
    strlcpy(new_settings->identity, self.identity.UTF8String, sizeof(new_settings->identity));

    strlcpy(new_settings->anonymous_identity, self.anonymousIdentity.UTF8String, sizeof(new_settings->anonymous_identity));

    NSInteger n=[self sendSettings:new_settings];


    [self completSendSettingsWithSuccess:n<0?NO:YES];
}
@end
