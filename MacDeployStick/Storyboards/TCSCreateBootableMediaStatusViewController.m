//
//  TCSCreateBootableMediaStatusViewController.m
//  MDS
//
//  Created by Timothy Perfitt on 3/19/20.
//  Copyright © 2020 Twocanoes Software. All rights reserved.
//

#import "TCSCreateBootableMediaStatusViewController.h"


@implementation TCSCreateBootableMediaStatusViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do view setup here.
}
- (IBAction)cancelButtonPressed:(id)sender {
    [self.delegate cancelOperation:self];
}
- (IBAction)showLogButtonPressed:(id)sender {
    [self.delegate showLog:self];
}

@end
