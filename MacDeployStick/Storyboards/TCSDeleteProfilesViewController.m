//
//  TCSDeleteProfilesViewController.m
//  MDS
//
//  Created by Timothy Perfitt on 2/25/20.
//  Copyright © 2020 Twocanoes Software. All rights reserved.
//

#import "TCSDeleteProfilesViewController.h"
#import "DeviceMO+CoreDataProperties.h"
#import "ProfileMO+CoreDataProperties.h"
#import "TCSMDMWebController.h"
@interface TCSDeleteProfilesViewController ()
@property (weak) IBOutlet NSTableView *profilesTableView;
@property (strong) IBOutlet NSArrayController *profilesArrayController;
@property (strong) NSArray *devices;
@end

@implementation TCSDeleteProfilesViewController
- (IBAction)removeProfilesButtonPressed:(id)sender {

    NSArray *selectedProfiles=[self.profilesArrayController selectedObjects];

    NSMutableArray *selectedProfileIdentifiersArray=[NSMutableArray array];

    [selectedProfiles enumerateObjectsUsingBlock:^(NSDictionary *currProfileDict, NSUInteger idx, BOOL * _Nonnull stop) {
        [selectedProfileIdentifiersArray addObject:[currProfileDict objectForKey:@"identifier"]];
    }];
    NSAlert *alert=[[NSAlert alloc] init];
    alert.messageText=@"Delete Confirmation";
    alert.informativeText=[NSString stringWithFormat:@"Are you sure you want to delete %li profiles from the %li devices?",selectedProfiles.count,self.devices.count];

    [alert addButtonWithTitle:@"OK"];
    [alert addButtonWithTitle:@"Cancel"];
    NSInteger res=[alert runModal];
    if (res==NSAlertSecondButtonReturn) return;

    [self.devices enumerateObjectsUsingBlock:^(DeviceMO *device, NSUInteger idx, BOOL * _Nonnull stop) {
        NSMutableArray *profilesToDelete=[NSMutableArray array];
        [device.installedProfiles enumerateObjectsUsingBlock:^(ProfileMO *  currDeviceProfile, BOOL * _Nonnull stop) {
            NSString *currProfileIdentifier=[(NSDictionary *)currDeviceProfile.profileDict objectForKey:@"PayloadIdentifier"];

            if ([selectedProfileIdentifiersArray containsObject:currProfileIdentifier]){
                [profilesToDelete addObject:currProfileIdentifier];
            }

        }];
        NSString *currUDID=device.udid;
        [[TCSMDMWebController sharedController] deleteProfilesFromDeviceWithUDID:currUDID profileIdentiifers:profilesToDelete];

    }];
    [[self.view window] close];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do view setup here.
}

-(void)deleteProfiles:(NSArray *)devices{
    __block NSMutableSet *collectProfiles=[NSMutableSet set];
    self.devices=devices;
    [devices enumerateObjectsUsingBlock:^(DeviceMO *device, NSUInteger idx, BOOL * _Nonnull stop) {
        NSSet *profiles=device.installedProfiles;

        [profiles enumerateObjectsUsingBlock:^(ProfileMO *profile, BOOL * _Nonnull stop) {

            NSDictionary *profileInfo=(NSDictionary *)profile.profileDict;
            NSString *profileIdentifier=[profileInfo objectForKey:@"PayloadIdentifier"];
            if (profileIdentifier && ![[profileIdentifier lowercaseString] isEqualToString:@"com.github.micromdm.micromdm.enroll"]){
                [collectProfiles addObject:profileIdentifier];
                 //addObject:@{@"identifier":profileIdentifier}];

            }

        }];

    }];
    NSMutableArray *unifiedArrayOfIdentiers=[NSMutableArray array];
    [collectProfiles enumerateObjectsUsingBlock:^(NSString *currentID, BOOL * _Nonnull stop) {
        [unifiedArrayOfIdentiers addObject:@{@"identifier":currentID}];
    }];

    self.profiles=[NSArray arrayWithArray:unifiedArrayOfIdentiers];
    [self.profilesTableView reloadData];
}
@end
