//
//  TCSStatusTableCellView.m
//  MDS
//
//  Created by Timothy Perfitt on 3/11/20.
//  Copyright © 2020 Twocanoes Software. All rights reserved.
//

#import "TCSStatusTableCellView.h"

@interface TCSStatusTableCellView()
@end

@implementation TCSStatusTableCellView

- (void)drawRect:(NSRect)dirtyRect {
    [super drawRect:dirtyRect];
    
    // Drawing code here.
}

@end
