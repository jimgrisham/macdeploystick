//
//  TCSMDMAppsViewController.h
//  MDS
//
//  Created by Timothy Perfitt on 2/27/20.
//  Copyright © 2020 Twocanoes Software. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import "DeviceMO+CoreDataClass.h"
NS_ASSUME_NONNULL_BEGIN
@protocol TCSInstallAppsProtocol <NSObject>
-(void)installApps:(NSArray *)appArray toDevices:(NSArray <DeviceMO *> *)devices ;
@end
@interface TCSMDMAppsViewController : NSViewController
@property (weak) id <TCSInstallAppsProtocol> delegate;
@property (strong) NSArray <DeviceMO *> * devices;
@end

NS_ASSUME_NONNULL_END
