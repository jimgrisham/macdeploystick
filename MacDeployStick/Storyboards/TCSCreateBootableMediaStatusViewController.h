//
//  TCSCreateBootableMediaStatusViewController.h
//  MDS
//
//  Created by Timothy Perfitt on 3/19/20.
//  Copyright © 2020 Twocanoes Software. All rights reserved.
//

#import <Cocoa/Cocoa.h>

NS_ASSUME_NONNULL_BEGIN
@protocol TCSCreateBootableMediaStatusFeedbackProtocol <NSObject>

-(void)cancelOperation:(id)self;
-(void)showLog:(id)sender;


@end


@interface TCSCreateBootableMediaStatusViewController : NSViewController 
@property (weak) id <TCSCreateBootableMediaStatusFeedbackProtocol> delegate;

@end

NS_ASSUME_NONNULL_END
