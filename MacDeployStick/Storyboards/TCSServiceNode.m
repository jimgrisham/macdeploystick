//
//  TCSServiceNode.m
//  MDS
//
//  Created by Timothy Perfitt on 3/7/20.
//  Copyright © 2020 Twocanoes Software. All rights reserved.
//

#import "TCSServiceNode.h"
@interface TCSServiceNode()

@end
@implementation TCSServiceNode

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.name=@"";
    }
    return self;
}

-(NSInteger) childCount{

    return self.children.count;
}

-(BOOL)isLeaf{
    if ([self childCount]==0) return YES;
    return NO;
}
@end
