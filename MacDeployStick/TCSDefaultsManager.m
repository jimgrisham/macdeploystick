//
//  TCSDefaultsManager.m
//  MDS
//
//  Created by Timothy Perfitt on 7/16/19.
//  Copyright © 2019 Twocanoes Software. All rights reserved.
//

#import "TCSDefaultsManager.h"

@implementation TCSDefaultsManager
+ (id)sharedManager {
    static TCSDefaultsManager *sharedManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedManager = [[self alloc] init];
    });
    return sharedManager;
}
-(void)clearCache{
    NSFileManager *fm=[NSFileManager defaultManager];

    BOOL isDir;
    NSError *err;
    NSString *tempPath=[[TCSDefaultsManager sharedManager] tempFolder];
    if (!tempPath) return;

    if ([fm fileExistsAtPath:tempPath isDirectory:&isDir] && isDir==YES){


        if( [fm removeItemAtPath:tempPath error:&err]==NO){
            [[NSAlert alertWithError:err] runModal];
        }
    }
}
-(NSString *)tempFolder{
    NSFileManager *fm=[NSFileManager defaultManager];

    NSString *tempFolderPath=[[[NSUserDefaults standardUserDefaults] objectForKey:@"tempFolder"] stringByExpandingTildeInPath];

    if ([fm fileExistsAtPath:tempFolderPath]==NO){

        NSError *err;
        if( [fm createDirectoryAtPath:tempFolderPath withIntermediateDirectories:YES attributes:nil error:&err]==NO){

            dispatch_async(dispatch_get_main_queue(), ^{

                NSError *fullerErr=[NSError errorWithDomain:@"TCS" code:-1 userInfo:@{NSLocalizedDescriptionKey:[NSString stringWithFormat:@"The directory could not be created at %@. Error: %@",tempFolderPath,err.localizedDescription]}];

                [[NSAlert alertWithError:fullerErr] runModal];


            });
            return nil;
        }
    }

    return tempFolderPath;
}
//- (id)init {
//    if (self = [super init]) {
//    }
//    return self;
//}

-(NSString *)newTempFolder{
    NSString *tempSavePath= [[self tempFolder] stringByAppendingPathComponent:[[NSUUID UUID] UUIDString]] ;
    NSFileManager *fm=[NSFileManager defaultManager];

    NSError *err;
    if([fm createDirectoryAtPath:tempSavePath withIntermediateDirectories:YES attributes:nil error:&err]==NO){
        dispatch_async(dispatch_get_main_queue(), ^{            [[NSAlert alertWithError:err] runModal];


        });

    }
    return tempSavePath;


}

@end
