//
//  TCSDefaultsViewController.m
//  MacDeployStick
//
//  Created by Timothy Perfitt on 1/5/19.
//  Copyright © 2019 Twocanoes Software. All rights reserved.
//

#import "TCSDefaultsViewController.h"
#import <SecurityInterface/SFChooseIdentityPanel.h>
#import <SecurityInterface/SFCertificatePanel.h>
#import "DeploySettings.h"
#import "TCSUtility.h"
#import "TCSDefaultsManager.h"
#import "NSData+Base64.h"
#import "TCSMDMWebController.h"
@interface TCSDefaultsViewController ()
@property (weak) IBOutlet NSTabView *defaultsTabView;
@property (assign) BOOL sslInfoChanged;

@end

@implementation TCSDefaultsViewController
-(void)viewDidAppear{
    
    [self.view.window setMinSize:NSMakeSize(800,500)];
    
}

-(IBAction)clearTempFiles:(id)sender{
    
    [[TCSDefaultsManager sharedManager] clearCache];
}

-(void)viewWillAppear{
    self.sslInfoChanged=NO;
    [[NSUserDefaults standardUserDefaults] addObserver:self
                                            forKeyPath:@"serverHostname"
                                               options:NSKeyValueObservingOptionNew
                                               context:NULL];
    
    [[NSUserDefaults standardUserDefaults] addObserver:self
                                            forKeyPath:@"munkiServerPort"
                                               options:NSKeyValueObservingOptionNew
                                               context:NULL];
    
    [[NSUserDefaults standardUserDefaults] addObserver:self
                                            forKeyPath:@"tlsCertificateFolder"
                                               options:NSKeyValueObservingOptionNew
                                               context:NULL];
    
}
-(void)viewWillDisappear{
    [[NSUserDefaults standardUserDefaults] removeObserver:self forKeyPath:@"serverHostname"];
    
    [[NSUserDefaults standardUserDefaults] removeObserver:self forKeyPath:@"munkiServerPort"];
    [[NSUserDefaults standardUserDefaults] removeObserver:self forKeyPath:@"tlsCertificateFolder"];

    
    if (self.sslInfoChanged) {
        [[NSNotificationCenter defaultCenter] postNotificationName:SSLINFOCHANGED object:self];
    }
    
}
- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context{
    
    if ([keyPath isEqualToString:@"munkiServerPort"]) {
        self.sslInfoChanged=YES;
    }
    if ([keyPath isEqualToString:@"serverHostname"]) {
        self.sslInfoChanged=YES;
    }
    if ([keyPath isEqualToString:@"tlsCertificateFolder"]) {
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            NSString *identityFolderPath=[[NSUserDefaults standardUserDefaults] objectForKey:TLSCERTIFICATEFOLDER];
            
            NSFileManager *fm=[NSFileManager defaultManager];
            
            NSString *newKeyPath=[identityFolderPath stringByAppendingPathComponent:@"mds_identity.key"];
            NSString *newCertPath=[identityFolderPath stringByAppendingPathComponent:@"mds_identity.cer"];
            if ([fm fileExistsAtPath:newKeyPath]==NO || [fm fileExistsAtPath:newCertPath]==NO){
                
                NSAlert *alert=[[NSAlert alloc] init];
                alert.messageText=@"Identity Not Found";
                alert.informativeText=@"The selected folder must contain a mds_identity.cer and mds_identity.key file. Please select a folder with those items or create a self signed certificate.";
                
                [alert addButtonWithTitle:@"OK"];
                [alert runModal];
                return;
            }
            
            
            [[NSUserDefaults standardUserDefaults] setObject:[identityFolderPath stringByAppendingPathComponent:@"mds_identity.key"] forKey:INDENTITYKEYPATH];
            
            [[NSUserDefaults standardUserDefaults] setObject:[identityFolderPath stringByAppendingPathComponent:@"mds_identity.cer"] forKey:CERTIFICATEPATH];
            
            self.sslInfoChanged=YES;
        });
    }
    
}

- (IBAction)shouldUseTLSButtonPressed:(id)sender {
    if ( [[USERDEFAULTS objectForKey:@"shouldUseTLSMunki"] boolValue]==YES && (![[NSUserDefaults standardUserDefaults] objectForKey:CERTIFICATEPATH] || ![[NSUserDefaults standardUserDefaults] objectForKey:INDENTITYKEYPATH])){
        
        
        NSAlert *alert=[[NSAlert alloc] init];
        alert.messageText=@"No SSL Certificate Defined";
        alert.informativeText=@"In order to use SSL, please create or provide information for an SSL Certificate in the Security pane of preferences.";
        
        [alert addButtonWithTitle:@"OK"];
        [alert runModal];
        
        [USERDEFAULTS
         setBool:NO forKey:@"shouldUseTLSMunki"];
        return;
    }
    self.sslInfoChanged=YES;
}
- (IBAction)clearCustomRunCommandValue:(id)sender {
    
    
    NSUserDefaults *ud=[NSUserDefaults standardUserDefaults];
    
    [ud removeObjectForKey:@"alternateRunCommandPath"];
}
/*
 
 */



- (IBAction)showAPNSIdentityButtonPressed:(id)sender {
    NSArray *returnIdentityArray;
    OSStatus sanityCheck;
    sanityCheck = SecItemCopyMatching((CFDictionaryRef)[NSDictionary dictionaryWithObjectsAndKeys:
                                                        (id)kSecClassIdentity,           kSecClass,
                                                        kSecMatchLimitAll,      kSecMatchLimit,
                                                        kCFBooleanFalse,         kSecReturnRef,
                                                        kCFBooleanFalse,         kSecReturnAttributes,
                                                        nil
                                                        ] , (void *)&returnIdentityArray);
    
    
    if (sanityCheck!=noErr) {
        NSLog(@"SecIdentityCopyCertificate error");
        return ;
    }
    
    
    
    
    SFChooseIdentityPanel *panel=[SFChooseIdentityPanel sharedChooseIdentityPanel];
    
    [panel setAlternateButtonTitle:@"Cancel"];
    NSInteger ret=[panel runModalForIdentities:returnIdentityArray message:@"Select identity for Apple Push Notifications."];
    
    if (ret==NSModalResponseOK) {
        SecIdentityRef selectedIdentity=panel.identity;
        CFRetain(selectedIdentity);
        SecIdentityRef identity=panel.identity;
        OSStatus            err;
        SecCertificateRef   certificate;
        CFStringRef         summary;
        
        err = SecIdentityCopyCertificate(identity, &certificate);
        summary = SecCertificateCopySubjectSummary(certificate);
        NSString *identityCertificateText=(__bridge NSString *)(summary);
        
        [[NSUserDefaults standardUserDefaults] setObject:identityCertificateText forKey:@"APNSIdentity"];
    }
    
}
- (IBAction)showIdentityButtonPressed:(id)sender {
    NSArray *returnIdentityArray;
    OSStatus sanityCheck;
    sanityCheck = SecItemCopyMatching((CFDictionaryRef)[NSDictionary dictionaryWithObjectsAndKeys:
                                                        (id)kSecClassIdentity,           kSecClass,
                                                        kSecMatchLimitAll,      kSecMatchLimit,
                                                        kCFBooleanFalse,         kSecReturnRef,
                                                        kCFBooleanFalse,         kSecReturnAttributes,
                                                        CFSTR("Installer"),kSecMatchSubjectContains,
                                                        
                                                        nil
                                                        ] , (void *)&returnIdentityArray);
    
    
    if (sanityCheck!=noErr) {
        NSAlert *alert=[[NSAlert alloc] init];
        alert.messageText=@"No Identities Found";
        alert.informativeText=@"There were no identities found in the keychain. Please import a developer identity and try again.";
        
        [alert addButtonWithTitle:@"OK"];
        
        [alert runModal];
        
        NSLog(@"SecIdentityCopyCertificate error");
        return ;
    }
    
    
    
    
    SFChooseIdentityPanel *panel=[SFChooseIdentityPanel sharedChooseIdentityPanel];
    
    [panel setAlternateButtonTitle:@"Cancel"];
    NSInteger ret=[panel runModalForIdentities:returnIdentityArray message:@"Select identity for signing macOS packages."];
    
    if (ret==NSModalResponseOK) {
        SecIdentityRef selectedIdentity=panel.identity;
        CFRetain(selectedIdentity);
        SecIdentityRef identity=panel.identity;
        OSStatus            err;
        SecCertificateRef   certificate;
        CFStringRef         summary;
        
        err = SecIdentityCopyCertificate(identity, &certificate);
        summary = SecCertificateCopySubjectSummary(certificate);
        NSString *identityCertificateText=(__bridge NSString *)(summary);
        
        [[NSUserDefaults standardUserDefaults] setObject:identityCertificateText forKey:@"signingIdentity"];
    }
    
}

- (IBAction)selectMDMServerKeyButtonPressed:(id)sender {
    
    
    NSOpenPanel *openPanel=[NSOpenPanel openPanel];
    openPanel.canChooseFiles=YES;
    openPanel.canChooseDirectories=NO;
    openPanel.allowsMultipleSelection=NO;
    openPanel.message=@"Select key in PEM format:";
    NSModalResponse res=[openPanel runModal];
    
    if (res==NSModalResponseOK) {
        [[NSUserDefaults standardUserDefaults] setObject:[openPanel URL].path forKey:@"MDMServerTLSKeyPath"];
        
        
    }
    
    
}
//- (IBAction)selectMDMServerCertificateButtonPressed:(id)sender {
//    NSOpenPanel *openPanel=[NSOpenPanel openPanel];
//    openPanel.canChooseFiles=YES;
//    openPanel.canChooseDirectories=NO;
//    openPanel.allowsMultipleSelection=NO;
//    openPanel.message=@"Select certificate in PEM format:";
//    NSModalResponse res=[openPanel runModal];
//
//    if (res==NSModalResponseOK) {
//        [[NSUserDefaults standardUserDefaults] setObject:[openPanel URL].path forKey:@"mdmServerTLSCertificatePath"];
//
//    }
//}

- (IBAction)selectKeyButtonPressed:(id)sender {
    
    
    NSOpenPanel *openPanel=[NSOpenPanel openPanel];
    openPanel.canChooseFiles=YES;
    openPanel.canChooseDirectories=NO;
    openPanel.allowsMultipleSelection=NO;
    openPanel.message=@"Select key in PEM format:";
    NSModalResponse res=[openPanel runModal];
    
    if (res==NSModalResponseOK) {
        [[NSUserDefaults standardUserDefaults] setObject:[openPanel URL].path forKey:INDENTITYKEYPATH];
        
        //munkiTLSCertificatePath
    }
    
    
}
- (IBAction)selectCertificateButtonPressed:(id)sender {
    NSOpenPanel *openPanel=[NSOpenPanel openPanel];
    openPanel.canChooseFiles=YES;
    openPanel.canChooseDirectories=NO;
    openPanel.allowsMultipleSelection=NO;
    openPanel.message=@"Select certificate in PEM format:";
    NSModalResponse res=[openPanel runModal];
    
    if (res==NSModalResponseOK) {
        
        [[NSUserDefaults standardUserDefaults] setObject:[openPanel URL].path forKey:CERTIFICATEPATH];
        
    }
}

- (IBAction)restoreUserDefaultsButtonPressed:(id)sender {
    
    //    NSString *appDomain = [[NSBundle mainBundle] bundleIdentifier];
    //    [[NSUserDefaults standardUserDefaults] removePersistentDomainForName:appDomain];
    
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"imagr_url"];
    
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"diskImageVolumeName"];
    
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"webserverPort"];
    
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"tempFolder"];
    
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"munkiServerPort"];
    
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"shouldUseTLSMunki"];
    
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:CERTIFICATEPATH];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:INDENTITYKEYPATH];
    
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"munkiTLSKeyPath"];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"alternateRunCommandPath"];
    
    
}
- (IBAction)restoreAlertPromptsButtonPressed:(id)sender {
    
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"TCSSuppressCreateSSLCertificate"];
    
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"TCSSuppressRepackageWarning"];
    
}

-(IBAction)changeDefaultsTabView:(NSToolbarItem *)toolbarItem{
    
    NSInteger tag=toolbarItem.tag;
    
    [self.defaultsTabView selectTabViewItemAtIndex:tag];
    
}
- (void)viewDidLoad {
    [super viewDidLoad];
    //    self.preferredContentSize=self.view.frame.size;
    
}

@end
