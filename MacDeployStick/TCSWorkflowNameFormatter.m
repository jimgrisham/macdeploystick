//
//  TCSWorkflowNameFormatter.m
//  MDS
//
//  Created by Timothy Perfitt on 3/11/19.
//  Copyright © 2019 Twocanoes Software. All rights reserved.
//

#import "TCSWorkflowNameFormatter.h"

@implementation TCSWorkflowNameFormatter
- (NSString *)stringForObjectValue:(id)obj
{
    if (![obj isKindOfClass:[NSString class]]){
        return nil;
    }
    return [obj copy];
}


- (BOOL)getObjectValue:(out __autoreleasing id *)obj forString:(NSString *)string errorDescription:(out NSString *__autoreleasing *)error
{
    *obj = [string copy];
    return YES;
}

- (BOOL)isPartialStringValid:(NSString *)partialString
            newEditingString:(NSString **)newString
            errorDescription:(NSString **)error{
    if ([partialString containsString:@"/"]){

        *newString=[partialString stringByReplacingOccurrencesOfString:@"/" withString:@"-"];
        return NO;
    }
    return YES;
}
//- (NSString *)stringForObjectValue:(id)anObject
//{
//    if (![anObject isKindOfClass:[NSNumber class]]) {
//        return nil;
//    }
//    return [NSString stringWithFormat:@"$%.2f", [anObject floatValue]];
//}
@end
