//
//  TCSAutomatonSelectorViewController.m
//  MDS
//
//  Created by Timothy Perfitt on 5/21/19.
//  Copyright © 2019 Twocanoes Software. All rights reserved.
//

#import "TCSAutomatonSelectorViewController.h"

@interface TCSAutomatonSelectorViewController ()

@end

@implementation TCSAutomatonSelectorViewController


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do view setup here.
}
- (IBAction)createAutomatonButtonPressed:(id)sender{
    [self performSegueWithIdentifier:@"createMacAutomatonSegue" sender:self];

}
- (IBAction)configureAutomatonButtonPressed:(id)sender{
    [self performSegueWithIdentifier:@"configureMacAutomatonSegue" sender:self];
}
- (IBAction)createChromebookAutomatonPressed:(id)sender{
    [self performSegueWithIdentifier:@"createChromeAutomatonSegue" sender:self];

}
- (IBAction)configureChromebookAutomatonPressed:(id)sender{
    [self performSegueWithIdentifier:@"configureChromeAutomatonSegue" sender:self];

}

@end
