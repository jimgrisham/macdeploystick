//
//  TCSCSRViewController.h
//  MDS
//
//  Created by Timothy Perfitt on 8/29/19.
//  Copyright © 2019 Twocanoes Software. All rights reserved.
//

#import <Cocoa/Cocoa.h>

NS_ASSUME_NONNULL_BEGIN

@interface TCSCSRViewController : NSViewController

@end

NS_ASSUME_NONNULL_END
