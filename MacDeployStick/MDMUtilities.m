//
//  MDMUtilities.m
//  MDS
//
//  Created by Timothy Perfitt on 9/9/19.
//  Copyright © 2019 Twocanoes Software. All rights reserved.
//

#import "MDMUtilities.h"
#import <RepackageFramework/TCSRepackage.h>
#import "TCSConfigHelper.h"
@implementation MDMUtilities

+ (instancetype)sharedUtilities {
    static id sharedMyManager = nil;
    static dispatch_once_t onceToken;
    
    dispatch_once(&onceToken, ^{
        sharedMyManager = [[[self class] alloc] init];


    });
    return sharedMyManager;
}

-(void)removeMDMRepositoryApps:(NSArray *)namesOfApps completeBlock:(void (^)(NSError *err))completeBlock {
    NSString *repoResolvedPath=[[[NSUserDefaults standardUserDefaults] objectForKey:MDMSERVERFILEREPOPATH] stringByExpandingTildeInPath];

    [namesOfApps enumerateObjectsUsingBlock:^(NSString *currName, NSUInteger idx, BOOL * _Nonnull stop) {
        NSFileManager *fm=[NSFileManager defaultManager];
        NSString *currItemPath=[repoResolvedPath stringByAppendingPathComponent:currName];
            if ([fm fileExistsAtPath:currItemPath]==YES){

            NSError *err;
            if([fm removeItemAtPath:currItemPath error:&err]==NO){
                *stop=YES;
                completeBlock(err);
                return;
            }
            NSString *manifestFilePath=[[currItemPath stringByDeletingPathExtension] stringByAppendingPathExtension:@"plist"];
            if([fm removeItemAtPath:manifestFilePath error:&err]==NO){
                NSLog(@"could not remove manifest for %@:%@",manifestFilePath,err.localizedDescription);

            }

        }

    }];

}
-(void)setupApps:(NSArray *)apps statusBlock:(void (^)(NSString *status, float percentComplete))statusBlock completeBlock:(void (^)(NSArray *manifestArray))completeBlock{
    self.shouldCancel=NO;

    NSMutableArray *manifestURLArray=[NSMutableArray array];
    NSFileManager *fm=[NSFileManager defaultManager];


    NSString *repoResolvedPath=[[[NSUserDefaults standardUserDefaults] objectForKey:MDMSERVERFILEREPOPATH] stringByExpandingTildeInPath];
    TCSRepackage *repackage=[[TCSRepackage alloc] init];

    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        statusBlock(@"Starting Up",10);

        __block NSInteger total=apps.count;
        [apps enumerateObjectsUsingBlock:^(NSString *currFilePath, NSUInteger idx, BOOL * _Nonnull stop) {
            if (self.shouldCancel==YES) return;
            NSString *pathExt=[currFilePath pathExtension];
            if (![pathExt isEqualToString:@"pkg"]&&
                ![pathExt isEqualToString:@"mpkg"]&&
                ![pathExt isEqualToString:@"app"]&&
                ![pathExt isEqualToString:@"sparsebundle"]&&
                ![pathExt isEqualToString:@"dmg"]) {
                return;
            }

            NSString *finalPackagePath=[repoResolvedPath stringByAppendingPathComponent:[[[currFilePath lastPathComponent] stringByDeletingPathExtension] stringByAppendingPathExtension:@"pkg"]];


            statusBlock([NSString stringWithFormat:@"Repackaging %@",currFilePath.lastPathComponent],-1);
            if (self.shouldCancel==YES) return;

            if ([fm fileExistsAtPath:finalPackagePath]){

                NSError *err;
                if ([fm removeItemAtPath:finalPackagePath error:&err]==NO){

                    NSLog(@"%@ could not be deleted",finalPackagePath);
                }
            }

            [repackage repackagePackageAtPath:currFilePath destination:finalPackagePath forceEmbed:NO forceCopy:NO status:^(NSString * status) {
                statusBlock(status,10+(idx*100.00/total));

            } completionBlock:^(NSError *  err) {
                if (!err) {

                    NSString *manifestURL=[TCSConfigHelper manifestForPackagePath:finalPackagePath packageName:currFilePath.lastPathComponent];
                    [manifestURLArray addObject:manifestURL];
                }
            }];


        }];
        completeBlock([NSArray arrayWithArray:manifestURLArray]);
    });




}


@end
