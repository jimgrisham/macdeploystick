//
//  TCSUtility.h
//  MacDeployStick
//
//  Created by Timothy Perfitt on 1/2/19.
//  Copyright © 2019 Twocanoes Software. All rights reserved.
//

#import <Foundation/Foundation.h>
#include <sys/socket.h>
#include <ifaddrs.h>
#include <arpa/inet.h>
#import "TCSWebserverSetting.h"
NS_ASSUME_NONNULL_BEGIN

@interface TCSUtility : NSObject
+(NSArray *)listOfMedia;
+(BOOL)createUserPackageAtPath:(NSString *)inPackagePath withUID:(NSString *)inUID fullname:(NSString *)inFullname shortname:(NSString *)inShortname password:(NSString *)inPassword isAdmin:(BOOL)inIsAdmin isAutologin:(BOOL)inIsAutologin isHidden:(BOOL)inIsHidden passwordHint:(NSString *)inPasswordHint jpegPhoto:(NSString *)inJpegPhoto outputPackageName:(NSString *)outputPackageName;

+(NSString *)variableStringFromURL:(NSURL *)inURL;
+ (NSString *)getSerialNumber;
+(NSString *)getIPAddress;
+ (NSData *)calculateShadowHashData:(NSString *)pwd;
+(NSArray *)md5HashArrayFromFile:(NSString *)path;
+(NSDictionary *)manifestForURL:(NSString *)inURL fileHashArray:(NSArray *)hashArray;
+(BOOL)appendString:(NSString *)inString toEndOfFile:(NSString *)filePath;

+(void)openMunkiAdmin:(id)sender ;
//+(void)checkForServiceUpdates:(id)sender;
+(NSString *)encryptPassword:(NSString *)inPass;
+(BOOL)isProcessRunning:(NSString *)processName;
+(void)updateTLSCertIfNeed:(id)sender;

@end
NS_ASSUME_NONNULL_END
