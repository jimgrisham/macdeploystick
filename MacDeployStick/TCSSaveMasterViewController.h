//
//  TCSSaveMasterViewController.h
//  MDS
//
//  Created by Timothy Perfitt on 5/3/19.
//  Copyright © 2019 Twocanoes Software. All rights reserved.
//

#import <Cocoa/Cocoa.h>

NS_ASSUME_NONNULL_BEGIN

@interface TCSSaveMasterViewController : NSViewController <NSFileManagerDelegate>

-(void)saveMasterToPath:(NSString *)saveVolPath;
-(void)syncNow:(id)sender;
@property (strong) NSString *status;

@end

NS_ASSUME_NONNULL_END
