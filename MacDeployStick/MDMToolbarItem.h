//
//  MDMToolbarItem.h
//  MDS
//
//  Created by Timothy Perfitt on 9/3/19.
//  Copyright © 2019 Twocanoes Software. All rights reserved.
//

#import <Cocoa/Cocoa.h>

NS_ASSUME_NONNULL_BEGIN

@interface MDMToolbarItem : NSToolbarItem

@end

NS_ASSUME_NONNULL_END
