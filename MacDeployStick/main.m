//
//  main.m
//  MacDeployStick
//
//  Created by Timothy Perfitt on 12/22/18.
//  Copyright © 2018 Twocanoes Software. All rights reserved.
//

#import <Cocoa/Cocoa.h>

int main(int argc, const char * argv[]) {
    return NSApplicationMain(argc, argv);
}
