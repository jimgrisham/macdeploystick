//
//  TCSCreateUsersViewController.h
//  MDS
//
//  Created by Timothy Perfitt on 5/16/19.
//  Copyright © 2019 Twocanoes Software. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import "TCSWorkflow.h"
NS_ASSUME_NONNULL_BEGIN
@protocol TCSWorkflowUserProtocol <NSObject>
-(void)userUpdated:(TCSWorkflowUser *)inUser;
@end

@protocol TCSDelegate <NSObject>

@property (assign) id delegate;


@end
@interface TCSCreateUsersViewController : NSViewController <TCSDelegate>
@property (strong) TCSWorkflowUser *workflowUser;
@property (assign) id delegate;
@property (strong) IBOutlet NSObjectController * showHidePasswordObjectController;

@end

NS_ASSUME_NONNULL_END
