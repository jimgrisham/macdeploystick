//
//  TCSMunkiViewController.m
//  MDS
//
//  Created by Timothy Perfitt on 3/9/20.
//  Copyright © 2020 Twocanoes Software. All rights reserved.
//

#import "TCSMunkiServiceViewController.h"
#import <GCDWebServers/GCDWebServers.h>
#import "TCSUtility.h"
#import <Criollo/Criollo.h>
#import "TCSWebserverSetting.h"
#import "TCSServiceUpdateManager.h"
#import "TCSWebServiceController.h"
#import "TCSConstants.h"
#import "MDSPrivHelperToolController.h"
@interface TCSMunkiServiceViewController () <CRServerDelegate>
@property (assign) BOOL isMunkiServerListening;
@property (strong) GCDWebServer* munkiServer;
@property (strong) CRHTTPServer *server;
@property (strong) NSString *munkiRepoURL;
@property (strong) NSArray *webservicesPaths;
@property (weak) IBOutlet NSButton *munkiActionButton;
@property (assign) BOOL doesRepoExist;
@property (assign) BOOL shouldShowActionButton;
@property (strong) NSString *updateInfo;
@property (strong) NSString *installedVersion;
//@property (assign) BOOL isUpdateAvailable;

@property (strong) NSString *actionButtonTitle;

@end

@implementation TCSMunkiServiceViewController
@synthesize serviceStatus;
@synthesize isRunning;
@synthesize serviceButtonState;

@synthesize isUpdateAvailable;

@synthesize badgeCount;

@synthesize badgeType;

@synthesize hasError;
@synthesize errorMessage;

@synthesize isInstalled;

@synthesize logFilePath;

@synthesize statusMode;

@synthesize statusType;

//service protocol
//- (void)showLogButtonPressed:(nonnull id)sender {
//
//
//}
-(TCSWebserverSetting *)defaultMunkiSettings{
    NSUserDefaults *ud=[NSUserDefaults standardUserDefaults];
    NSString *certPath=[ud objectForKey:CERTIFICATEPATH];
    NSString *keyPath=[ud objectForKey:INDENTITYKEYPATH];


    TCSWebserverSetting *munkiSetting=[[TCSWebserverSetting alloc] init];
    munkiSetting.path=[[NSUserDefaults standardUserDefaults] objectForKey:@"munkiRepo"];

    munkiSetting.port=[[[NSUserDefaults standardUserDefaults] objectForKey:@"munkiServerPort"] intValue];
    if (certPath && keyPath){
        munkiSetting.useTLS=YES;
    }
    else {
        munkiSetting.useTLS=NO;

    }
    munkiSetting.useForMunki=YES;
    munkiSetting.allowDirectoryListing=YES;
    munkiSetting.isActive=YES;
    munkiSetting.enablePHP7=NO;
    return munkiSetting;

}


- (void)serviceUpdateAvailable:(nonnull NSNotification *)not {
    [self updateServiceStatus:self];


}

- (void)updateServiceStatus:(nonnull id)sender {
    NSFileManager *fm=[NSFileManager defaultManager];
    if ([fm fileExistsAtPath:@"/usr/local/munki/munkiimport"]==NO) {
        self.isInstalled=NO;

    }
    else self.isInstalled=YES;

    if (self.isInstalled==NO) {
        self.actionButtonTitle=@"Install Munki";
    }
    NSString *munkiPath=[[NSUserDefaults standardUserDefaults] objectForKey:@"munkiRepo"];
    self.doesRepoExist=YES;
    self.shouldShowActionButton=NO;
    if ([fm fileExistsAtPath:[munkiPath stringByAppendingPathComponent:@"manifests"]]==NO) {
          self.doesRepoExist=NO;
    }
    BOOL isWebServerRunning=[[TCSWebServiceController sharedController] isRunning];

    BOOL isActive=[self isActive];
    self.isRunning=isWebServerRunning && isActive;
    if ([self isRunning]==YES){
        self.serviceButtonState=NSControlStateValueOn;

    }
    else {
        self.serviceButtonState=NSControlStateValueOff;
    }

    if (!isInstalled){
        self.serviceStatus=@"Not Installed";
    }
    else if (self.isRunning==YES){

        self.serviceStatus=@"Running";

    }
    else if (isActive && !isWebServerRunning){
        self.serviceStatus=@"Web Server Stopped";
    }
    else if (!isActive){
        self.serviceStatus=@"Inactive";
    }
    else {
        self.serviceStatus=@"unknown";
    }
    [self updateMunkiURLString];

    self.isUpdateAvailable=NO;
    TCSServiceUpdateManager *updateManager=[TCSServiceUpdateManager updateManager];

    NSDictionary *updateInfo=[[updateManager updatesAvailable] objectForKey:@"munki"];
    if (updateInfo){
        self.installedVersion=[updateInfo objectForKey:@"installed_version"];

        NSString *latestVersion=[updateInfo objectForKey:@"latest_version"];
        if(latestVersion) {
            self.installedVersion=[updateInfo objectForKey:@"installed_version"];
            if ([self.installedVersion hasPrefix:latestVersion]==NO) {

                self.isUpdateAvailable=YES;
                self.updateInfo=[NSString stringWithFormat:@"Update Available! Installed version: %@, New Version: %@",self.installedVersion,latestVersion];
                self.isUpdateAvailable=YES;
            }
        }
    }
    else{
        self.isUpdateAvailable=NO;
    }


}



- (IBAction)updateButtonPressed:(id)sender {
    [self openMunkiDownloads:self];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self updateServiceStatus:self];
    [[NSNotificationCenter defaultCenter] addObserverForName:SSLINFOCHANGED object:nil queue:nil usingBlock:^(NSNotification * _Nonnull note) {

    }];
    NSArray *websiteSettings=[[TCSWebServiceController sharedController] webserviceSettings];

    NSMutableArray *sharedWebFolders=[NSMutableArray array];
    if (websiteSettings && websiteSettings.count>0) {
        [websiteSettings enumerateObjectsUsingBlock:^(TCSWebserverSetting *webSetting, NSUInteger idx, BOOL * _Nonnull stop) {
            if (webSetting.path) [sharedWebFolders addObject:webSetting.path];
        }];

    }
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(serviceUpdateAvailable:) name:TCSNOTIFICATIONUPDATEAVAILABLE object:nil];

    if (sharedWebFolders && sharedWebFolders.count>0) self.webservicesPaths=[NSArray arrayWithArray:sharedWebFolders];
    [[TCSServiceUpdateManager updateManager] checkForMunkiReportUpdate:self];

    [[NSUserDefaults standardUserDefaults] addObserver:self
    forKeyPath:@"munkiRepo"
       options:NSKeyValueObservingOptionNew
       context:NULL];


}
- (void)dealloc
{
    [self.authenticationView setAutoupdate:NO];

    [[NSUserDefaults standardUserDefaults] removeObserver:self forKeyPath:@"munkiRepo"];
}
- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context{

    if ([keyPath isEqualToString:@"munkiRepo"]) {
        [self updateServiceStatus:self];
       }
}
-(void)updateMunkiURLString{
    TCSWebserverSetting *webSetting=[[TCSWebServiceController sharedController] munkiWebserverSettings];

    NSUserDefaults *ud=[NSUserDefaults standardUserDefaults];
    NSString *currentSelectedRepo=[ud objectForKey:@"munkiRepo"];
    if (!currentSelectedRepo) return;
    NSInteger port=webSetting.port;
    NSString *prefix=webSetting.useTLS==YES?@"https://":@"http://";
    self.munkiRepoURL=[NSString stringWithFormat:@"%@%@:%li",prefix,[USERDEFAULTS objectForKey:SERVERHOSTNAME],port];

}
- (IBAction)openBrowserForMunkiRepo:(id)sender {
    [self updateMunkiURLString];
    if(self.munkiRepoURL){
        [[NSWorkspace sharedWorkspace] openURL:[NSURL URLWithString:self.munkiRepoURL]];
    }

}

- (IBAction)showRepoButtonPressed:(id)sender {
    NSString *sharedFolder=[[NSUserDefaults standardUserDefaults] objectForKey:@"munkiRepo"];

    if (sharedFolder){
        [[NSWorkspace sharedWorkspace] openFile:sharedFolder];
    }


}


-(IBAction)createMunkiRepo:(id)sender{
    if (self.isAuthorized==NO) return;
    NSFileManager *fm=[NSFileManager defaultManager];
    NSString *munkiRepo=[[NSUserDefaults standardUserDefaults] objectForKey:@"munkiRepo"];

    if ([fm fileExistsAtPath:[munkiRepo stringByAppendingPathComponent:@"manifests"]]==YES) {
        return;
    }

    NSAlert *alert=[[NSAlert alloc] init];
    alert.messageText=@"Create Munki Repo";

    alert.informativeText=@"Create a new repository?";

    [alert addButtonWithTitle:@"Create"];
    [alert addButtonWithTitle:@"Cancel"];
    NSInteger res=[alert runModal];

    if (res==NSAlertSecondButtonReturn) {
        [self updateServiceStatus:self];
        return;
    }


    [self createMunkiRepoWithCallback:^(NSError * err) {

        dispatch_async(dispatch_get_main_queue(), ^{
            [self updateServiceStatus:self];
            if (err){
                [[NSAlert alertWithError:err] runModal];
                return;
            }
            else {

                NSAlert *alert=[[NSAlert alloc] init];
                alert.messageText=@"Munki Repo Created";
                alert.informativeText=@"The munki repo has been created.";

                [alert addButtonWithTitle:@"OK"];
                [alert runModal];
            }
        });
    }];


}
-(void)createMunkiRepoWithCallback:(void (^)(NSError * _Nullable))callback{


    NSString *munkiPath=[[NSUserDefaults standardUserDefaults] objectForKey:@"munkiRepo"];

    [[MDSPrivHelperToolController sharedHelper] createMunkiRepoAtPath:munkiPath callback:^(NSError * err) {


        if (!err){


            /*
             defaults write com.hjuutilainen.MunkiAdmin selectedRepositoryPath -string "${rep_path}"
             defaults write com.hjuutilainen.MunkiAdmin startupWhatToDo -int 2

             */
            NSUserDefaults *mySharedDefaults = [[NSUserDefaults alloc] initWithSuiteName:@"com.hjuutilainen.MunkiAdmin"];

            [mySharedDefaults setObject:munkiPath forKey:@"selectedRepositoryPath"];
            [mySharedDefaults setObject:@(2) forKey:@"startupWhatToDo"];



        }
        callback(err);

     }];
}


-(BOOL)isActive{
    BOOL isActive=NO;

    TCSWebserverSetting *munkiSetting=[[[TCSWebServiceController sharedController]  munkiWebserverSettings] copy];
    if (munkiSetting) isActive=munkiSetting.isActive;
    return isActive;
}
-(void)stopService:(id)sender{
    if (self.isAuthorized==NO) return;

    if ([self isActive]==YES){
        TCSWebserverSetting *currSettings=[[TCSWebServiceController sharedController] munkiWebserverSettings];
        currSettings.isActive=NO;
        [[TCSWebServiceController sharedController] updateMunkiServerConfiguration:currSettings callback:^(BOOL success) {
            [self updateServiceStatus:self];
            [self updateSidebar:self];

        }];

    }


}
-(void)startService:(id)sender{
    if (self.isAuthorized==NO) return;
    NSFileManager *fm=[NSFileManager defaultManager];
    NSString *munkiRepo=[[NSUserDefaults standardUserDefaults] objectForKey:@"munkiRepo"];


    if ([self isInstalled]==NO){
          NSAlert *alert=[[NSAlert alloc] init];
          alert.messageText=@"Munki Not Installed";

          alert.informativeText=@"The munki tools are not installed. Download and install them now?";

          [alert addButtonWithTitle:@"Install"];
          [alert addButtonWithTitle:@"Cancel"];
          NSInteger res=[alert runModal];

          if (res==NSAlertFirstButtonReturn) {
              [self updateServiceStatus:self];

              [self openMunkiDownloads:self];
          }

        [self updateServiceStatus:self];

        return;
    }

    if ([fm fileExistsAtPath:[munkiRepo stringByAppendingPathComponent:@"manifests"]]==NO){
        NSAlert *alert=[[NSAlert alloc] init];
        alert.messageText=@"No Munki Respository Found";

        alert.informativeText=[NSString stringWithFormat:@"There was no munki repository found at %@. Create a new repository?",munkiRepo];

        [alert addButtonWithTitle:@"Create"];
        [alert addButtonWithTitle:@"Cancel"];
        NSInteger res=[alert runModal];

        if (res==NSAlertSecondButtonReturn) {
            [self updateServiceStatus:self];

            return;
        }


        [self createMunkiRepoWithCallback:^(NSError * err) {
            if (err){
                dispatch_async(dispatch_get_main_queue(), ^{

                    [[NSAlert alertWithError:err] runModal];
                    return;
                });
            }
            else {
                [self createMunkiService];
            }
        }];

    }
    else {
        [self createMunkiService];
    }
    [self updateSidebar:self];


}

-(void)createMunkiService{
    if (self.isAuthorized==NO) return;
    TCSWebserverSetting *munkiSetting=[[[TCSWebServiceController sharedController]  munkiWebserverSettings] copy];

    BOOL isActive;
    if (!munkiSetting) isActive=NO;
    else isActive=munkiSetting.isActive;

    if (isActive==NO){

        TCSWebserverSetting *currSettings=[[TCSWebServiceController sharedController]  munkiWebserverSettings];
        if (!currSettings){
            if ([[TCSWebServiceController sharedController] hasTLSCertificates:self]==NO){

                NSAlert *alert=[[NSAlert alloc] init];
                alert.messageText=@"No SSL Certificates Defined";
                alert.informativeText=@"There are no SSL certificates defined for this server. It is recommended that you create or import an SSL in Security under Preferences.";

                [alert addButtonWithTitle:@"Cancel"];
                [alert addButtonWithTitle:@"Continue"];
                NSInteger res=[alert runModal];
                if (res==NSAlertFirstButtonReturn)
                {
                    [self updateServiceStatus:self];
                    return;

                }

            }
            currSettings=[self defaultMunkiSettings];
        }
        currSettings.isActive=YES;
        [[TCSWebServiceController sharedController] updateMunkiServerConfiguration:currSettings callback:^(BOOL success) {
            [self startWebserviceIfNeeded:self];



        }];
    }
    else {

        [self startWebserviceIfNeeded:self];

    }
    dispatch_async(dispatch_get_main_queue(), ^{
        self.serviceStatus=@"Updating...";

        [self performSelector:@selector(updateServiceStatus:) withObject:self afterDelay:1];
    });

}
- (StatusMode)statusMode {
    return StatusModeStopped;
}

- (StatusType)statusType {
    return StatusTypeOK;
}


-(void)startWebserviceIfNeeded:(id)sender{
    BOOL isWebServerRunning=[[TCSWebServiceController sharedController] isRunning];

    if (isWebServerRunning==NO){
              [[TCSWebServiceController sharedController]  startWebServicesWithCallback:^(BOOL success, BOOL didCancel) {
                  if (success==NO){
                      dispatch_async(dispatch_get_main_queue(), ^{

                          NSAlert *alert=[[NSAlert alloc] init];
                          alert.messageText=@"Web Service Status";
                          alert.informativeText=@"There was an error starting up the web service";

                          [alert addButtonWithTitle:@"OK"];
                          [alert runModal];
                      });
                  }
                  self.serviceStatus=@"Updating...";
                  [self performSelector:@selector(updateServiceStatus:) withObject:self afterDelay:5];



              }];

          }
}
-(IBAction)refreshStatus:(id)sender{

    [self updateServiceStatus:self];
}

-(void)openMunkiDownloads:(id)sender{
    NSString *munkiURL=[[NSUserDefaults standardUserDefaults] objectForKey:@"munki_url"];
    [[NSWorkspace sharedWorkspace] openURL:[NSURL URLWithString:munkiURL]];

}
@end
