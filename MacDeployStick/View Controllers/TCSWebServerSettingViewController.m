//
//  TCSWebServerSettingViewController.m
//  MDS
//
//  Created by Timothy Perfitt on 3/10/20.
//  Copyright © 2020 Twocanoes Software. All rights reserved.
//

#import "TCSWebServerSettingViewController.h"

@interface TCSWebServerSettingViewController ()

@end

@implementation TCSWebServerSettingViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do view setup here.
}


-(void)viewWillAppear{
    if (self.representedObject==nil) {
        
        TCSWebserverSetting *webserverSetting=[[TCSWebserverSetting alloc] init];
        webserverSetting.path=@"";
        webserverSetting.port=self.newWebserverPort;
        webserverSetting.useTLS=NO;
        webserverSetting.isActive=YES;
        webserverSetting.allowDirectoryListing=YES;
        webserverSetting.enablePHP7=NO;
        webserverSetting.useForMunkiReport=NO;
        self.representedObject=webserverSetting;
    }
    else {
        self.representedObject=[self.representedObject copy];


    }
}
- (IBAction)okButtonPressed:(id)sender {
    NSString *homeDirPath=NSHomeDirectory();
    if ([[self.representedObject path] hasPrefix:homeDirPath]){
        NSAlert *alert=[[NSAlert alloc] init];
        alert.messageText=@"Possible Permission Issue";
        alert.informativeText=@"The shared folder is located in your home directory and may be a protected location. It is recommend to select a folder outside your home directory (like /Users/Shared). Do you want continue?";

        [alert addButtonWithTitle:@"Cancel"];
        [alert addButtonWithTitle:@"Continue"];
        NSInteger res=[alert runModal];
        if (res==NSAlertFirstButtonReturn) return;

    }

    if ([self.delegate updateConfiguration:self.representedObject]==NO){
        NSAlert *alert=[[NSAlert alloc] init];
        alert.messageText=@"Port Conflict";
        alert.informativeText=@"The new webserver configuration has a port conflict. Please change the port number and try again.";

        [alert addButtonWithTitle:@"OK"];
        [alert runModal];
        return;
    }
    self.representedObject=nil;
    [self dismissViewController:self];
}

@end
