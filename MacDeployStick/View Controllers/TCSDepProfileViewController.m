//
//  TCSDepProfileViewController.m
//  MDS
//
//  Created by Timothy Perfitt on 8/1/19.
//  Copyright © 2019 Twocanoes Software. All rights reserved.
//

#import "TCSDepProfileViewController.h"
#import "TCSMDMWebController.h"
#import "TCSUtility.h"
#import "ProgressDialog.h"
#import "TCSConfigHelper.h"
#import "ProgressDialog.h"
@interface TCSDepProfileViewController () <ProgressDialogDelegateProtocol>
@property (assign) NSViewController <UpdateStatus> *updateDelegate ;
@property (strong) id sslObserver;
@end

@implementation TCSDepProfileViewController
- (IBAction)selectAllButtonPressed:(id)sender {
    NSArray *subViews=[[sender superview] subviews];

    [subViews enumerateObjectsUsingBlock:^(NSView *currView, NSUInteger idx, BOOL * _Nonnull stop) {
        if ([currView isKindOfClass:[NSButton class]]){

            NSButton *button = (NSButton *)currView;
            NSDictionary *bindingInfo = [button infoForBinding: NSValueBinding];
            [[bindingInfo valueForKey: NSObservedObjectKey] setValue: @YES
                                                          forKeyPath: [bindingInfo valueForKey: NSObservedKeyPathKey]];
        }

    }];
}

- (IBAction)userAccountOptionsChanged:(id)sender {

    if (self.depProfile.shouldSkipPrimaryAccountCreation==YES) {
        self.depProfile.shouldSetPrimaryAccountAsRegularUser=NO;
    }
}
- (IBAction)selectNoneButtonPressed:(id)sender {
    NSArray *subViews=[[sender superview] subviews];

    [subViews enumerateObjectsUsingBlock:^(NSView *currView, NSUInteger idx, BOOL * _Nonnull stop) {
        if ([currView isKindOfClass:[NSButton class]]){

            NSButton *button = (NSButton *)currView;
            NSDictionary *bindingInfo = [button infoForBinding: NSValueBinding];
            [[bindingInfo valueForKey: NSObservedObjectKey] setValue: @NO
                                                          forKeyPath: [bindingInfo valueForKey: NSObservedKeyPathKey]];
        }

    }];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.showAdvancedUserOptions=NO;
    if ([[NSUserDefaults standardUserDefaults] objectForKey:@"depProfile"]){

        NSError *err;
        NSData *inData=[[NSUserDefaults standardUserDefaults] objectForKey:@"depProfile"];
        NSSet *set = [NSSet setWithArray:@[[TCSDepProfile class],
                                           [TCSWorkflowUser class],
                                           [TCSWorkflowLocalization class],
                                           [NSArray class],
                                           [NSString class],
                                           [NSNumber class],
                                           [NSMutableData class],
                                           [TCSMDSVariable class]
        ]];


        if (@available(macOS 10.13, *)) {
            self.depProfile = [NSKeyedUnarchiver unarchivedObjectOfClasses:set fromData:inData error: &err];
            if (err) {
                NSLog(@"error unarchiving data:%@",err.localizedDescription);
                [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"depProfile"];
            }

        } else {
            // Fallback on earlier versions
        }

    }
    else  {
        self.depProfile=[[TCSDepProfile alloc] init];
        self.depProfile.shouldIncludeAnchorCert=YES;
        self.depProfile.isProfileRemovable=YES;
        self.depProfile.profileName=@"MDS DEP Profile";
        self.depProfile.users=[NSMutableArray array];
    }

    if (!self.depProfile.users) {
        self.depProfile.users=[NSMutableArray array];

    }
    self.depProfile.mdmServerURL=[NSString stringWithFormat:@"https://%@:%@/mdm/enroll",[USERDEFAULTS objectForKey:SERVERHOSTNAME],[USERDEFAULTS objectForKey:MDMSERVERPORT]];;

    self.workflow=self.depProfile;

}

- (IBAction)cancelButtonPressed:(id)sender {
    [self dismissViewController:self];
}
-(void)saveToPrefs:(id)sender{
    NSError *err;
    if (@available(macOS 10.13, *)) {
        NSData *dataToSave=[NSKeyedArchiver archivedDataWithRootObject:self.depProfile requiringSecureCoding:YES error:&err];

        if (err) {
            NSLog(@"error archiving data:%@",err.localizedDescription);
        }

        else [[NSUserDefaults standardUserDefaults] setObject:dataToSave forKey:@"depProfile"];
    } else {

        NSLog(@"not available before 10.13");
    }
}
- (IBAction)tableDoubleClick:(id)sender {
    if ([self.tableView clickedRow]!=-1) {

        [self performSegueWithIdentifier:@"editUserDetailSegue" sender:self];
    }
}
-(void)awakeFromNib{
    self.sslObserver=[[NSNotificationCenter defaultCenter] addObserverForName:SSLINFOCHANGED object:nil queue:nil usingBlock:^(NSNotification * _Nonnull note) {

        self.depProfile.mdmServerURL=[NSString stringWithFormat:@"https://%@:%@/mdm/enroll",[USERDEFAULTS objectForKey:SERVERHOSTNAME],[USERDEFAULTS objectForKey:MDMSERVERPORT]];;

    }];

}
-(void)dealloc{
    if (self.sslObserver)  [[NSNotificationCenter defaultCenter] removeObserver:self.sslObserver];


}
-(void)userUpdated:(TCSWorkflowUser *)inUser{

    if (self.addUser) {

        [self.usersArrayController addObject:inUser];
        self.addUser=nil;

    }
    else if (self.editUser) {
        NSInteger selectionIndex=self.usersArrayController.selectionIndex;

        [self.usersArrayController  removeObjectAtArrangedObjectIndex:selectionIndex];
        [self.usersArrayController insertObject:inUser atArrangedObjectIndex:selectionIndex];

    }
}


- (void)extracted:(NSDictionary * _Nonnull)depDict {
    [[TCSMDMWebController sharedController] updateDEPProfile:depDict onCompletion:^(BOOL success) {


        [self.depProfile blueprintWithStatusBlock:^(NSString * _Nonnull status, float percentComplete) {
            if (self.updateDelegate.shouldCancel==YES) {
                [self.depProfile cancel:self];
            }
            [self.updateDelegate updateStatusText:status];
            [self.updateDelegate percentCompleted:percentComplete];

        } returnBlock:^(NSDictionary * _Nonnull depDict) {

            dispatch_async(dispatch_get_main_queue(), ^{

                [[TCSMDMWebController sharedController] updateBlueprint:depDict onCompletion:^(BOOL success) {
                    [self dismissViewController:self];
                    [self dismissViewController:self.updateDelegate];

                }];
            });

        }];

    }];
}

- (void)extracted {
    [self.depProfile profileForDEPWithStatusBlock:^(NSString * _Nonnull status, float percentComplete) {

        if (self.updateDelegate.shouldCancel==YES) {
            [self.depProfile cancel:self];
        }
        [self.updateDelegate updateStatusText:status];
        [self.updateDelegate percentCompleted:percentComplete];


    } returnBlock:^(NSDictionary * _Nonnull depDict) {
        [self saveToPrefs:self];
        dispatch_async(dispatch_get_main_queue(), ^{

            [self extracted:depDict];
        });

    }];
}

- (IBAction)okButtonPressed:(id)sender {

    if (self.depProfile.profileName.length>0 && self.depProfile.supportPhoneNumber.length>0 && self.depProfile.supportEmailAddress.length>0 && self.depProfile.mdmServerURL.length>0) {
        
        [self performSegueWithIdentifier:@"updateProgressSegue" sender:self];
        
        
        [[TCSMDMWebController sharedController] createUsers:self.depProfile.users];
        [self extracted];
    }
    else {
        NSAlert *alert=[[NSAlert alloc] init];
        alert.messageText=@"Missing Profile Information";
        alert.informativeText=@"The Deployment Enrollment General Information in the General Section cannot be left blank. Provide information for the profile name, support phone number, support email, and the mdm server URL.";

        [alert addButtonWithTitle:@"OK"];
        [alert runModal];
        return;

    }

}
- (void)prepareForSegue:(NSStoryboardSegue *)segue sender:(id)sender{

    if ([segue.identifier isEqualToString:@"updateProgressSegue"]){

        ProgressDialog *dialogViewController=segue.destinationController;
        dialogViewController.delegate=self;
        self.updateDelegate=dialogViewController;
    }
    else {
        [super prepareForSegue:segue sender:sender];

    }
}
- (void)cancelUpdate:(nonnull id)sender {
//    self.updateDelegate.shouldCancel=YES;
    [self.depProfile cancel:self];
}



@end
