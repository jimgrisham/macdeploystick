//
//  TCSMDMViewController.m
//  MDS
//
//  Created by Timothy Perfitt on 7/28/19.
//  Copyright © 2019 Twocanoes Software. All rights reserved.
//

#import "TCSMDMViewController.h"
#import "TCSMDMWebController.h"
#import "TCSUtility.h"
#import <SecurityInterface/SFChooseIdentityPanel.h>
#import <SecurityInterface/SFCertificatePanel.h>
#import "NSData+Base64.h"
#import "TCSMDMWebController.h"
#import "MDMDataController.h"
#import "DeviceMO+CoreDataClass.h"
#import "DeviceDetailsViewController.h"
#import "TCSMDMService.h"
#import "TCSConfigHelper.h"
#import "TCSMDMViewController.h"
#import "ProfileMO+CoreDataProperties.h"
#import "TCSDeleteProfilesViewController.h"
#import "TCSEraseDevicesPinViewController.h"
#import "TCSMDMAppsViewController.h"
#import "TCSMDMAppsViewController.h"
#import "TCSLogFileWindowController.h"
#import "TCSConstants.h"

@interface TCSMDMViewController () <TCSMDMActionsProtocol,TCSEraseDeviceConfirmationDelegateProtocol,TCSInstallAppsProtocol,TCSLogWindowDelegate>
@property (strong) IBOutlet NSArrayController *deviceArrayController;
@property (strong) NSMutableArray *deviceArray;
@property (strong) NSImage *mdmStatusImage;
@property (strong) NSImage *pushCertificateStatusImage;
@property (strong) NSImage *depTokenStatusImage;
@property (strong) TCSLogFileWindowController *logWindowController;
@property (assign) NSControlStateValue mdmProcessState;
@property (strong) TCSMDMWebController *mdmWebController;
@end


@implementation TCSMDMViewController
- (instancetype)initWithCoder:(NSCoder *)coder
{
    self = [super initWithCoder:coder];
    if (self) {
//
//        [[NSNotificationCenter defaultCenter] addObserverForName:TCSNOTIFICATIONMDMSERVICESTARTED object:nil queue:nil usingBlock:^(NSNotification * _Nonnull note) {
//            NSToolbarItem *tbi=self.startStopMDMToolBarItem;
//            tbi.image=[NSImage imageNamed:@"stop"];
//            tbi.label=@"Stop";
//        }];
//
//        [[NSNotificationCenter defaultCenter] addObserverForName:TCSNOTIFICATIONMDMSERVICESTOPPED object:nil queue:nil usingBlock:^(NSNotification * _Nonnull note) {
//            NSToolbarItem *tbi=self.startStopMDMToolBarItem;
//            tbi.image=[NSImage imageNamed:@"play"];
//            tbi.label=@"Start";
//        }];



        self.mdmWebController=[TCSMDMWebController sharedController];
    }
    return self;
}



/*
 if ([[TCSMDMService sharedManager] isRunning]==YES){
 [[NSNotificationCenter defaultCenter] postNotificationName:TCSNOTIFICATIONMDMSERVICESTARTED object:self];
 }

 */

- (IBAction)showLogButtonPressed:(id)sender {
    //
    //    if (!self.logWindowController){
    //        self.logWindowController=[[TCSLogFileWindowController alloc] initWithWindowNibName:@"TCSLogFileWindowController"];
    //        self.logWindowController.logFile=MDMLOGFILE;
    //        self.logWindowController.delegate=self;
    //    }
    //    [self.logWindowController.window makeKeyAndOrderFront:self];
    [[NSWorkspace sharedWorkspace] openFile:MDMLOGFILE withApplication:@"Console"];

}

-(void)logWindowClosed{

    self.logWindowController=nil;

}


- (IBAction)deviceTableDoubleDidDoubleClick:(id)sender {

    if(self.deviceArrayController.selectedObjects.count>0) {
        [self performSegueWithIdentifier:@"showDeviceDetailsSegue" sender:self];
    }
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [[TCSMDMWebController sharedController] setAuthAPIKey:[USERDEFAULTS objectForKey:MDMSERVERRAPIKEY]];

    self.deviceArray=[NSMutableArray array];
    [self resetStatus];
    [self refreshMDMStatus];
    [self updateStatus:self];
    //    [[NSNotificationCenter defaultCenter]  addObserverForName:NSApplicationWillTerminateNotification object:nil queue:nil usingBlock:^(NSNotification * _Nonnull note) {
    //        [self stopMDM];
    //
    //    }];
    [[NSNotificationCenter defaultCenter] addObserverForName:@"configurationChanged" object:nil queue:nil usingBlock:^(NSNotification * _Nonnull note) {
        //        [self performSelector:@selector(askToStart:) withObject:self afterDelay:0.25];

    }];

    [[NSNotificationCenter defaultCenter] addObserverForName:TCSNOTIFICATIONMDMSERVICESTARTED object:nil queue:nil usingBlock:^(NSNotification * _Nonnull note) {
        [self refreshMDMStatus];
        [[TCSMDMService sharedManager] isRunning];
        self.mdmStatusImage=[NSImage imageNamed:NSImageNameStatusAvailable];
        [self performSelector:@selector(updateStatus:) withObject:self afterDelay:1];

    }];

    //    [[NSNotificationCenter defaultCenter] addObserverForName:TCSNOTIFICATIONMDMSERVICESTOPPED object:nil queue:nil usingBlock:^(NSNotification * _Nonnull note) {
    //
    //            self.mdmStatusImage=[NSImage imageNamed:NSImageNameStatusNone];
    //        [self resetStatus];
    //    }];


}
-(void)resetStatus{
    self.mdmStatusImage=[NSImage imageNamed:NSImageNameStatusNone];
    self.pushCertificateStatusImage=[NSImage imageNamed:NSImageNameStatusNone];

    self.depTokenStatusImage=[NSImage imageNamed:NSImageNameStatusNone];
    //    [self refreshMDMStatus];
}

- (void)awakeFromNib{
    [super awakeFromNib];

    if (! self.representedObject) {
        self.representedObject = [MDMDataController dataController];
    }
    [self refreshMDMStatus];

}
-(void)viewWillAppear{
    [[TCSMDMWebController sharedController] startWebServerWithCertificatePath:[USERDEFAULTS objectForKey:CERTIFICATEPATH] privateKeyPath:[USERDEFAULTS objectForKey:INDENTITYKEYPATH]];

}

-(void)viewWillDisappear{
    [[TCSMDMWebController sharedController] stopWebserver];

}

-(void)refreshMDMStatus{
    [self.mdmWebController updateIsAvailable];

    if ([[TCSMDMService sharedManager] isRunning]==NO) {
        self.mdmProcessState=NSControlStateValueOff;
        self.mdmStatusImage=[NSImage imageNamed:NSImageNameStatusNone];

    }
    else {

        self.mdmProcessState=NSControlStateValueOn;
        self.mdmStatusImage=[NSImage imageNamed:NSImageNameStatusAvailable];

    }
}
-(IBAction)installApps:(id)sender{

    [self performSegueWithIdentifier:@"installAppsSegue" sender:self];

}
-(IBAction)delete:(id)sender{

    if([self.deviceArrayController selectedObjects].count>0){

        NSAlert *alert=[[NSAlert alloc] init];
        alert.messageText=@"Delete Selected Devices?";
        alert.informativeText=@"Are you sure you want to delete the selected devices? All the data stored for the devices will be deleted and the device will be unenrolled the next time it checks in.";

        [alert addButtonWithTitle:@"Delete"];
        [alert addButtonWithTitle:@"Cancel"];
        NSInteger res=[alert runModal];
        if (res==NSAlertSecondButtonReturn) return;
        NSMutableArray *collectUDIDs=[NSMutableArray array];
        NSArray *selectedObjects=[self.deviceArrayController selectedObjects];
        [selectedObjects enumerateObjectsUsingBlock:^(DeviceMO *currDevice, NSUInteger idx, BOOL * _Nonnull stop) {
            [collectUDIDs addObject:currDevice.udid];
            [[MDMDataController dataController] removeDeviceWithUDID:currDevice.udid];

        }];

        [[TCSMDMWebController sharedController] deleteDevicesWithIDs:collectUDIDs onCompletion:^(BOOL success) {

            if(success==NO) NSLog(@"failed removing some devices with IDs");
        }];

    }
}
-(IBAction)block:(id)sender{
    NSAlert *alert=[[NSAlert alloc] init];
    alert.messageText=@"Block Selected Devices?";
    alert.informativeText=@"Are you sure you want to block the selected devices? The next time they check in to the MDM server, they will be unenrolled.";

    [alert addButtonWithTitle:@"Block"];
    [alert addButtonWithTitle:@"Cancel"];
    NSInteger res=[alert runModal];
    if (res==NSAlertSecondButtonReturn) return;

    NSArray *selectedObjects=[self.deviceArrayController selectedObjects];

    if (selectedObjects && selectedObjects.count>0){
        [self blockDevices:selectedObjects];
    }

}
-(IBAction)unblock:(id)sender{
    NSAlert *alert=[[NSAlert alloc] init];
    alert.messageText=@"Unblock Selected Devices?";
    alert.informativeText=@"Are you sure you want to unblock the selected devices? Any existing blocks will be removed.";

    [alert addButtonWithTitle:@"Unblock"];
    [alert addButtonWithTitle:@"Cancel"];
    NSInteger res=[alert runModal];
    if (res==NSAlertSecondButtonReturn) return;

    NSArray *selectedObjects=[self.deviceArrayController selectedObjects];

    if (selectedObjects && selectedObjects.count>0){
        [self unblockDevices:selectedObjects];
    }

}
-(void)mdmCommand:(NSMenuItem *)sender {


    [self mdmCommand:sender filePayload:nil];
}
-(IBAction)erase:(id)sender{

    [self performSegueWithIdentifier:@"eraseDevicesPinSegue" sender:self];
}
-(void)eraseDevices:(NSArray *)inDevices pin:(NSString *)inPin{
    TCSMDMWebController *webController=[TCSMDMWebController sharedController];
    [webController eraseDevices:inDevices withPIN:inPin onCompletion:^(BOOL success) {

    }];


}

-(void)blockDevices:(NSArray *)inDevices{
    TCSMDMWebController *webController=[TCSMDMWebController sharedController];
    [webController blockDevices:inDevices onCompletion:^(BOOL success) {
        if (success==NO) {
            NSAlert *alert=[[NSAlert alloc] init];
            alert.messageText=@"Error blocking devices";
            alert.informativeText=@"There was an error blocking the selected devices. Please check the log.";

            [alert addButtonWithTitle:@"OK"];
            [alert runModal];
        }


    }];


}
-(void)unblockDevices:(NSArray *)inDevices{
    NSLog(@"unblocking");
    TCSMDMWebController *webController=[TCSMDMWebController sharedController];
    [webController unblockDevices:inDevices onCompletion:^(BOOL success) {
        if (success==NO) {
            NSAlert *alert=[[NSAlert alloc] init];
            alert.messageText=@"Error unblocking devices";
            alert.informativeText=@"There was an error unblocking the selected devices. Please check the log";

            [alert addButtonWithTitle:@"OK"];
            [alert runModal];
        }


    }];


}
-(IBAction)refreshDeviceDetails:(id)sender{

    if ([[TCSMDMService sharedManager] isRunning]==NO) {

        NSAlert *alert=[[NSAlert alloc] init];
        alert.messageText=@"Service Status";
        alert.informativeText=@"The MDM Service is not running. Please start the service and try again.";
        [alert addButtonWithTitle:@"OK"];
        [alert runModal];

        return;
    }
    [[self.deviceArrayController arrangedObjects] enumerateObjectsUsingBlock:^(DeviceMO *device, NSUInteger idx, BOOL * _Nonnull stop) {

        if (device.udid) {
            [[TCSMDMWebController sharedController] refreshDataFromDeviceWithUDID:device.udid];
        }

    }];

}

-(void)mdmCommand:(NSMenuItem *)menu filePayload:(NSString *)filePayload{
    NSDictionary *commandInfo=menu.representedObject;

    [self performMDMCommand:commandInfo pin:nil filePayload:filePayload];
}
-(void)performMDMCommand:(NSDictionary *)commandInfo pin:(NSString *)pin filePayload:(NSString *)filePayload{
    [self performMDMCommand:commandInfo pin:nil filePayload:filePayload forDevices:[self.deviceArrayController selectedObjects]];

}
-(IBAction)removeProfile:(id)sender{

    [self performSegueWithIdentifier:@"deleteProfilesSegue" sender:self];
}

-(void)performMDMCommand:(NSDictionary *)commandInfo pin:(NSString *)pin filePayload:(NSString *)inFilePayload forDevices:(NSArray *)devices{
    NSString *filePayload=nil;

    if (commandInfo[@"payloadType"] && [commandInfo[@"payloadType"] isEqualToString:@"file"]){

        if (inFilePayload){
            filePayload=inFilePayload;
        }
        else
        {
            NSOpenPanel *openPanel=[NSOpenPanel openPanel];
            openPanel.allowsOtherFileTypes=YES;
            openPanel.allowedFileTypes=@[commandInfo[@"payloadFileExtension"]];

            NSModalResponse res=[openPanel runModal];

            if (res!=NSModalResponseOK) {
                return;
            }
            NSURL *openURL=openPanel.URL;
            NSData *fileData=[NSData dataWithContentsOfURL:openURL];
            if (!fileData) return;
            filePayload=[fileData base64EncodedString];
        }

    }

    NSAlert *alert=[[NSAlert alloc] init];
    NSString *confirmationTitle=commandInfo[@"confirmationTitle"];
    alert.messageText=confirmationTitle!=nil?confirmationTitle:@"Confirm Command";

    NSString *confirmationMessage=commandInfo[@"confirmationMessage"];

    alert.informativeText=confirmationMessage!=nil?confirmationMessage:@"Are you sure you want to send the MDM command?";

    NSString *confirmationButton=commandInfo[@"confirmationButton"];

    [alert addButtonWithTitle:confirmationButton!=nil?confirmationButton:@"OK"];
    [alert addButtonWithTitle:@"Cancel"];
    NSInteger res=[alert runModal];
    if (res==NSAlertSecondButtonReturn) return;

    [devices enumerateObjectsUsingBlock:^(DeviceMO *device, NSUInteger idx, BOOL * _Nonnull stop) {

        __block NSString *udid=device.udid;

        if (!commandInfo[@"command"]){
            NSBeep();
            return;
        }

        [[TCSMDMWebController sharedController] sendMDMCommand:commandInfo[@"command"] identifier:nil deviceUDID:udid pin:pin filePayload:filePayload app:nil];

        if (device.udid && pin==nil) { //don't refresh an erase device. makes no sense.
            [[TCSMDMWebController sharedController] refreshDataFromDeviceWithUDID:device.udid];
        }
    }];
}

-(IBAction)refreshDevices:(id)sender{

    TCSMDMWebController *webController=[TCSMDMWebController sharedController];
    [webController sendPayload:@{} toEndpoint:@"/v1/devices" httpAction:@"POST" onCompletion:^(NSInteger responseCode, NSData *responseData) {
        NSDictionary *devices=[NSJSONSerialization JSONObjectWithData:responseData options:0 error:nil];

        NSArray *deviceArray=[devices objectForKey:@"devices"];
        if (deviceArray!=(NSArray *)[NSNull null]){

            [deviceArray enumerateObjectsUsingBlock:^(NSDictionary *deviceInfo, NSUInteger idx, BOOL * _Nonnull stop) {

                [[MDMDataController dataController] addDevice:deviceInfo];

            }];

        }


    } onError:^(NSError *error) {


        NSAlert *alert=[[NSAlert alloc] init];
        alert.messageText=@"Connection Error";
        NSUserDefaults *ud=[NSUserDefaults standardUserDefaults];
        NSString *certPath=[ud objectForKey:CERTIFICATEPATH];

        NSString *msg=[NSString stringWithFormat:@"There was a connection error. Please verify that the service certificate is trusted in the system keychain and there are not multiple certificates with the same server name.\n\nAdd the certificate at %@ to your system keychain and mark it as trusted. You can run this command in Terminal:\n\nsudo security add-trusted-cert -d -r trustRoot -k /Library/Keychains/System.keychain \"%@\"",certPath,certPath];
        alert.informativeText=msg;

        [alert addButtonWithTitle:@"OK"];
        [alert runModal];


    }];
}
-(IBAction)openEnrollmentPage:(id)sender{


    NSString *mdmServiceURL=[NSString stringWithFormat:@"https://%@:%@",[USERDEFAULTS objectForKey:SERVERHOSTNAME],[USERDEFAULTS objectForKey:MDMSERVERPORT]];

    [[NSWorkspace sharedWorkspace] openURL:[NSURL URLWithString:mdmServiceURL]];


}

-(IBAction)startStopMDM:(id)sender{

    [self resetStatus];

    BOOL isRunning=[TCSUtility isProcessRunning:TCSPROCESSNAMEMICROMDM];

    if (isRunning==YES){
        [[TCSMDMService sharedManager] stopMDMWithCallback:^(NSError * _Nullable err) {
            if (err){
                dispatch_async(dispatch_get_main_queue(), ^{

                    [[NSAlert alertWithError:err] runModal];
                });
            }
                        [self refreshMDMStatus];
            //            [self updateStatus:self];

        }];
    }
    else {
        [[TCSMDMService sharedManager] startMDMWithCallback:^(NSError * _Nullable err) {

            if (err){
                dispatch_async(dispatch_get_main_queue(), ^{

                    [[NSAlert alertWithError:err] runModal];
                });
            }
            [self refreshMDMStatus];
            [self updateStatus:self];

        }];
    }
}

-(void)askToStart:(id)sender{

    if ([[TCSMDMService sharedManager] isRunning]==YES) return;

    if ([self checkExistanceOfMDMFiles]==NO) return;

    NSAlert *alert=[[NSAlert alloc] init];
    alert.messageText=@"Start MDM Service?";
    alert.informativeText=@"The MDM service is configured but is not running. Do you want to start it now?";

    [alert addButtonWithTitle:@"Start"];
    [alert addButtonWithTitle:@"Cancel"];
    NSInteger res=[alert runModal];
    if (res==NSAlertSecondButtonReturn) return;

    [self startMDMWithCallback:^(NSError * _Nullable err) {

        if (err){
            [[NSAlert alertWithError:err] runModal];
        }
    }];


}
-(void)viewDidAppear{
    [super viewDidAppear];
    if (![USERDEFAULTS objectForKey:SERVERHOSTNAME]){
        [[NSUserDefaults standardUserDefaults] setObject:[NSString stringWithFormat:@"https://%@",SERVERHOSTNAME] forKey:SERVERHOSTNAME];
    }

    if (![USERDEFAULTS objectForKey:MDMSERVERRAPIKEY]){
        [[NSUserDefaults standardUserDefaults] setObject:[NSUUID UUID].UUIDString forKey:MDMSERVERRAPIKEY];
    }


}

-(void)startMDMWithCallback:(void (^)(NSError * _Nullable err))callback{

    if (![self checkExistanceOfMDMFiles]){
        NSError *err=[NSError errorWithDomain:@"TCS" code:-1 userInfo:@{NSLocalizedDescriptionKey:@"There is no SSL certificate defined in preferences. Please create or specify an SSL certificate now"}];

        callback(err);
        return;
    }

    [self performSelector:@selector(refreshDevices:) withObject:self afterDelay:5];

    [[TCSMDMWebController sharedController] startWebServerWithCertificatePath:[USERDEFAULTS objectForKey:CERTIFICATEPATH] privateKeyPath:[USERDEFAULTS objectForKey:INDENTITYKEYPATH]];


    [[TCSMDMService sharedManager] startMDMWithCallback:^(NSError * _Nullable err) {

        callback(err);


    }];

}
-(void)updateStatus:(id)sender{
    [[TCSMDMWebController sharedController] checkAPNSCertLoadedStatus:^(BOOL isLoaded) {
        if (isLoaded) self.pushCertificateStatusImage=[NSImage imageNamed:NSImageNameStatusAvailable];
        else {
            NSAlert *alert=[[NSAlert alloc] init];
            alert.messageText=@"No Push Certificate Found";
            alert.informativeText=@"An MDM certificate has not been imported. In order for the MDM service to work, you must import a MDM certificate signed by Apple. Click More Information to learn how to do this.";

            [alert addButtonWithTitle:@"OK"];
            [alert addButtonWithTitle:@"More Information"];
            NSInteger res=[alert runModal];
            if (res==NSAlertSecondButtonReturn) {
                NSURL *url=[NSURL URLWithString:SUPPORTURLS[@"pushCertMoreInfo"]];
                [[NSWorkspace sharedWorkspace]  openURL:url];


            }

            self.pushCertificateStatusImage=[NSImage imageNamed:NSImageNameStatusUnavailable];
        }

    }];

    [[TCSMDMWebController sharedController] checkPushTokenLoadedStatus:^(BOOL isLoaded) {

        if (isLoaded) self.depTokenStatusImage=[NSImage imageNamed:NSImageNameStatusAvailable];
        else self.depTokenStatusImage=[NSImage imageNamed:NSImageNameStatusUnavailable];

    }];


}
-(void)stopMDM{
    [[TCSMDMService sharedManager] stopMDMWithCallback:^(NSError * _Nullable err) {

        if (err){
            dispatch_async(dispatch_get_main_queue(), ^{
                [[NSAlert alertWithError:err] runModal];
            });
        }
    }];
}
-(BOOL)checkExistanceOfMDMFiles{

    if (![USERDEFAULTS objectForKey:CERTIFICATEPATH] ||
        ![USERDEFAULTS objectForKey:INDENTITYKEYPATH]||
        ![USERDEFAULTS objectForKey:MDMSERVERRAPIKEY]||
        ![USERDEFAULTS objectForKey:SERVERHOSTNAME]||
        ![USERDEFAULTS objectForKey:MDMSERVERPORT]||
        ![USERDEFAULTS objectForKey:MDMSERVERREPOPATH]){


        //        NSAlert *alert=[[NSAlert alloc] init];
        //        alert.messageText=@"File required by MDM service not found";
        //        alert.informativeText=[NSString stringWithFormat:@"The MDM file %@ was not found. Specify a correct path and try again.",currFile];
        //
        //        retVal=NO;
        //        [alert addButtonWithTitle:@"OK"];
        //        [alert runModal];
        return NO;
    }

    return YES;
}
-(IBAction)depProfileButtonPressed:(id)sender{


}
- (IBAction)importAPNSCertificate:(id)sender {
    NSArray *returnIdentityArray;
    OSStatus sanityCheck;
    sanityCheck = SecItemCopyMatching((CFDictionaryRef)[NSDictionary dictionaryWithObjectsAndKeys:
                                                        (id)kSecClassIdentity,           kSecClass,
                                                        kSecMatchLimitAll,      kSecMatchLimit,
                                                        kCFBooleanFalse,         kSecReturnRef,
                                                        kCFBooleanFalse,         kSecReturnAttributes,
                                                        CFSTR("APSP"),kSecMatchSubjectContains,

                                                        nil
                                                        ] , (void *)&returnIdentityArray);


    if (sanityCheck!=noErr) {
        NSLog(@"SecIdentityCopyCertificate error");
        return ;
    }

    SFChooseIdentityPanel *panel=[SFChooseIdentityPanel sharedChooseIdentityPanel];

    [panel setAlternateButtonTitle:@"Cancel"];
    NSInteger ret=[panel runModalForIdentities:returnIdentityArray message:@"Select identity for Apple Push Notification Services."];

    if (ret==NSModalResponseOK) {
        SecIdentityRef selectedIdentity=panel.identity;
        CFRetain(selectedIdentity);
        SecIdentityRef identity=panel.identity;

        CFDataRef exportedData;
        CFUUIDRef uuidRef=CFUUIDCreate(NULL);
        CFStringRef passphrase=CFUUIDCreateString(NULL, uuidRef);
        SecItemImportExportKeyParameters params = {
            .version = SEC_KEY_IMPORT_EXPORT_PARAMS_VERSION,
            .passphrase = passphrase
        };


        SecKeyRef privateKey = NULL;
        OSStatus status = SecIdentityCopyPrivateKey(identity,
                                                    &privateKey);
        if (status != errSecSuccess) {

        }
        else                         {

        }



        OSStatus ret = SecItemExport(identity,
                                     kSecFormatPKCS12,
                                     0, /* Use kSecItemPemArmour to add PEM armor */
                                     &params,
                                     &exportedData);

        if(ret == errSecSuccess)
        {
            NSError *error;
            NSData *data=(__bridge NSData *)exportedData;


            NSString *filename=[[[NSUUID UUID] UUIDString] stringByAppendingPathExtension:@"p12"];
            NSString *tempDirName=[[NSUUID UUID] UUIDString];
            NSString *tempDir=[NSTemporaryDirectory() stringByAppendingPathComponent:tempDirName];
            if([[NSFileManager defaultManager] createDirectoryAtPath:tempDir withIntermediateDirectories:NO attributes:@{NSFilePosixPermissions:@0700} error:&error]==NO){
                NSLog(@"%@",error.localizedDescription);
                return;


            }

            NSString *tempFile=[tempDir stringByAppendingPathComponent:filename];


            if ([data writeToFile:tempFile atomically:NO]==NO) {

                return;
            }
            NSString *passphraseFilePath=[tempDir stringByAppendingPathComponent:@"passphrase"];

            NSString *privateKeyFilePath=[tempDir stringByAppendingPathComponent:@"privatekey.key"];


            NSString *privateKeyRSAFilePath=[tempDir stringByAppendingPathComponent:@"privatekey_rsa.key"];


            NSString *certificateFilePath=[tempDir stringByAppendingPathComponent:@"certificate.cer"];

            if ([(__bridge NSString *)passphrase writeToFile:passphraseFilePath atomically:NO encoding:NSUTF8StringEncoding error:&error]==NO){
                NSLog(@"error writing passphrase");
                return;

            }


            NSTask *task=[NSTask launchedTaskWithLaunchPath:@"/usr/bin/openssl" arguments:@[@"pkcs12",@"-out",certificateFilePath,@"-in",tempFile,@"-passin",[NSString stringWithFormat:@"file:%@",passphraseFilePath],@"-nodes",@"-nokeys"]];
            [task waitUntilExit];
            if (task.terminationStatus!=0){

                NSLog(@"error export certificate");
                return;
            }
            task=[NSTask launchedTaskWithLaunchPath:@"/usr/bin/openssl" arguments:@[@"pkcs12",@"-out",privateKeyFilePath,@"-in",tempFile,@"-passin",[NSString stringWithFormat:@"file:%@",passphraseFilePath],@"-nodes",@"-nocerts"]];
            [task waitUntilExit];
            if (task.terminationStatus!=0){

                NSLog(@"error export private key");
                return;
            }

            task=[NSTask launchedTaskWithLaunchPath:@"/usr/bin/openssl" arguments:@[@"rsa",@"-in",privateKeyFilePath,@"-out",privateKeyRSAFilePath]];
            [task waitUntilExit];
            if (task.terminationStatus!=0){

                NSLog(@"error converting private key");
                return;
            }
            NSString *cert=[NSString stringWithContentsOfFile:certificateFilePath encoding:NSUTF8StringEncoding error:&error];

            if (cert==nil){
                NSLog(@"error reading cert:%@",error.localizedDescription);
                return;
            }
            NSString *key=[NSString stringWithContentsOfFile:privateKeyRSAFilePath encoding:NSUTF8StringEncoding error:&error];

            if (key==nil){
                NSLog(@"error reading key:%@",error.localizedDescription);
                return;
            }

            if([[NSFileManager defaultManager]  removeItemAtPath:tempDir error:&error]==NO){
                NSLog(@"error deleting temp dir: %@",error.localizedDescription);
                return;
            }
            [[TCSMDMWebController sharedController] sendPayload:@{@"cert":[[cert dataUsingEncoding:NSUTF8StringEncoding] base64EncodedString],@"key":[[key dataUsingEncoding:NSUTF8StringEncoding] base64EncodedString]}  toEndpoint:@"/v1/config/certificate" httpAction:@"PUT" onCompletion:^(NSInteger responseCode, NSData *responseData) {

                [[TCSMDMService sharedManager] restartMDMWithCallback:^(NSError * _Nullable err) {

                }];
            } onError:^(NSError *error) {
                [[NSAlert alertWithError:error] runModal];
            }];

        }
    }

}
-(IBAction)showImportAPNSCertFromFile:(id)sender{
    [self performSegueWithIdentifier:@"importAPNSSegue" sender:self];
}


-(IBAction)showExportAPNSCertToFile:(id)sender{
    [self performSegueWithIdentifier:@"exportAPNSSegue" sender:self];

}
- (void)prepareForSegue:(NSStoryboardSegue *)segue sender:(NSButton *)sender{

    if ([segue.identifier isEqualToString:@"exportAPNSSegue"]) return;
    if ([segue.identifier isEqualToString:@"importAPNSSegue"]) return;
    if ([segue.identifier isEqualToString:@"segueShowConfig"]) return;
    if ([segue.identifier isEqualToString:@"showDeviceDetailsSegue"]){
        DeviceDetailsViewController * destination=(DeviceDetailsViewController *)[segue.destinationController contentViewController];
        destination.device=[[self.deviceArrayController selectedObjects] firstObject];
        destination.mdmDelegate=self;
    }
    else if ([segue.identifier isEqualToString:@"eraseDevicesPinSegue"]){
        TCSEraseDevicesPinViewController * destination=(TCSEraseDevicesPinViewController *)segue.destinationController;
        destination.delegate=self;
        destination.devices=[self.deviceArrayController selectedObjects];

    }
    else if ([segue.identifier isEqualToString:@"installAppsSegue"]){
        TCSMDMAppsViewController * destination=(TCSMDMAppsViewController *)segue.destinationController;
        destination.delegate=self;
        destination.devices=[self.deviceArrayController selectedObjects];

    }
    else if ([segue.identifier isEqualToString:@"deleteProfilesSegue"]){

        NSArray *devices=[self.deviceArrayController selectedObjects];
        TCSDeleteProfilesViewController * destination=(TCSDeleteProfilesViewController *)segue.destinationController;

        [destination deleteProfiles:devices];
        //
        //            if ([self.tabView.selectedTabViewItem.identifier isEqualToString:@"profiles"]){
        //
        //                ProfileMO * profile=self.profilesArrayController.selectedObjects.firstObject;
        //
        //                DeviceMO *device=profile.device;
        //                NSString *udid=device.udid;
        //                NSDictionary *profileDict=(NSDictionary *)profile.profileDict;
        //                if([profileDict objectForKey:@"PayloadIdentifier"]){
        //                    if (udid && udid.length>0){
        //                        [[TCSMDMWebController sharedController] deleteProfileFromDeviceWithUDID:udid profileIdentiifer:[profileDict objectForKey:@"PayloadIdentifier"]];
        //                    }
        //                }
        //
        //            }
    }

}


- (void)installApps:(nonnull NSArray *)appArray toDevices:(nonnull NSArray<DeviceMO *> *)devices {

    [[TCSMDMWebController sharedController] installApps:appArray onDevices:devices onCompletion:^(BOOL success) {
        if (success==NO){

            NSAlert *alert=[[NSAlert alloc] init];
            alert.messageText=@"Error Installing App";
            alert.informativeText=@"There was an error installing on or more application. Please check the log.";

            [alert addButtonWithTitle:@"OK"];
            [alert runModal];

        }

    }];
}



@end
