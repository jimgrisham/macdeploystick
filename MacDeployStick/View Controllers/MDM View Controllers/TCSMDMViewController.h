//
//  TCSMDMViewController.h
//  MDS
//
//  Created by Timothy Perfitt on 7/28/19.
//  Copyright © 2019 Twocanoes Software. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import "TCTaskWrapperWithBlocks.h"
#import "TCSCreateSelfSignedCertificateController.h"
#import "TCSEraseDevicesPinViewController.h"
#import "TCSServiceViewController.h"
NS_ASSUME_NONNULL_BEGIN

@protocol TCSMDMActionsProtocol <NSObject>

-(void)performMDMCommand:(nullable NSDictionary *)commandInfo pin:(nullable NSString *)pin filePayload:(nullable NSString *)inFilePayload forDevices:(NSArray *)devices;


@end



@interface TCSMDMViewController : TCSServiceViewController <TCSMDMActionsProtocol,TCSEraseDeviceConfirmationDelegateProtocol> 
-(IBAction)openEnrollmentPage:(id)sender;
-(IBAction)startStopMDM:(id)sender;
- (IBAction)importAPNSCertificate:(id)sender ;
-(void)stopMDM;
-(void)startMDMWithCallback:(void (^)(NSError * _Nullable err))callback;
-(IBAction)showImportAPNSCertFromFile:(id)sender;
-(IBAction)delete:(id)sender;
-(IBAction)showExportAPNSCertToFile:(id)sender;
-(void)performMDMCommand:(nullable NSDictionary *)commandInfo pin:(nullable NSString *)pin filePayload:(nullable NSString *)inFilePayload forDevices:(NSArray *)devices;

@end

NS_ASSUME_NONNULL_END
