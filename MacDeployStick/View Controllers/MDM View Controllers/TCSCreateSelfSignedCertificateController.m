//
//  TCSCreateSelfSignedCertificateController.m
//  MDS
//
//  Created by Timothy Perfitt on 7/29/19.
//  Copyright © 2019 Twocanoes Software. All rights reserved.
//

#import "TCSCreateSelfSignedCertificateController.h"
#import "TCTaskWrapperWithBlocks.h"
#import "TCSUtility.h"
#import "MDSPrivHelperToolController.h"
@interface TCSCreateSelfSignedCertificateController ()

@property (strong) NSString *certificateDNSName;
@property (strong) NSString *certificateValidDays;
@property (strong) TCTaskWrapperWithBlocks *csrTaskWrapper;
@property (strong) TCTaskWrapperWithBlocks *signTaskWrapper;
@end

@implementation TCSCreateSelfSignedCertificateController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.certificateValidDays=@"820";
    NSUserDefaults *ud=[NSUserDefaults standardUserDefaults];
    self.certificateDNSName=[ud objectForKey:SERVERHOSTNAME];

}
- (IBAction)createCertificate:(id)sender {

    NSString *tlsCertificateFolder=[[[NSUserDefaults standardUserDefaults] objectForKey:TLSCERTIFICATEFOLDER] stringByExpandingTildeInPath];

    NSFileManager *fm=[NSFileManager defaultManager];

    NSString *certPath=[tlsCertificateFolder stringByAppendingPathComponent:@"mds_identity.cer"];
    NSString *keyPath=[tlsCertificateFolder stringByAppendingPathComponent:@"mds_identity.key"];

    if ((certPath && [fm fileExistsAtPath:certPath]==YES)||
        (keyPath && [fm fileExistsAtPath:keyPath]==YES)){


        NSAlert *alert=[[NSAlert alloc] init];
          alert.messageText=@"Certificate Exists";
          alert.informativeText=[NSString stringWithFormat:@"There already exists an SSL certificate or private key. If you replace the SSL certificate, clients that have trusted the original certificate may stop working. Are you sure you want to replace the existing SSL certificate at %@?",certPath];

          [alert addButtonWithTitle:@"Replace"];
          [alert addButtonWithTitle:@"Cancel"];
          NSInteger res=[alert runModal];
          if (res==NSAlertSecondButtonReturn) return;


    }

    if ([fm fileExistsAtPath:tlsCertificateFolder]==NO){

        NSError *err;
        if([fm createDirectoryAtPath:tlsCertificateFolder withIntermediateDirectories:YES attributes:@{NSFilePosixPermissions:@0700} error:&err]==NO){

            [[NSAlert alertWithError:err] runModal];
            return;
        }
    }
    NSURL *saveURL=[NSURL fileURLWithPath:[tlsCertificateFolder stringByAppendingPathComponent:@"mds_identity"]];
    if (!self.delegate) self.delegate=self;
    self.csrTaskWrapper=[[TCTaskWrapperWithBlocks alloc] initWithStartBlock:^{

    } endBlock:^{

        if (self.csrTaskWrapper.terminationStatus==0){
            [self signRequestAtFileURL:saveURL];
        }
        else {
            if ([self.delegate respondsToSelector:@selector(identityCreatedAtURL:privateKeyURL:)]){

                [self.delegate identityCreatedAtURL:nil privateKeyURL:nil];
            }
        }
    } outputBlock:^(NSString *output) {
        NSLog(@"%@",output);
    } errorOutputBlock:^(NSString *errorOutput) {
        NSLog(@"%@",errorOutput);
    } arguments:@[@"/usr/bin/openssl",@"req",@"-nodes",@"-sha256", @"-newkey",@"rsa:2048",@"-keyout",[saveURL.path stringByAppendingString:@".key"],@"-out", [saveURL.path stringByAppendingString:@".csr"], @"-subj",[NSString stringWithFormat:@"/C=US/CN=%@",self.certificateDNSName]]];

    [self.csrTaskWrapper startProcess];


}
-(void)signRequestAtFileURL:(NSURL *)url{

    NSString *configFilePath=[[NSBundle mainBundle] pathForResource:@"ssl_config" ofType:@"txt"];

    NSMutableString *configFileContents=[NSMutableString stringWithContentsOfFile:configFilePath encoding:NSUTF8StringEncoding error:nil];

    [configFileContents replaceOccurrencesOfString:@"%HOSTNAME%" withString:self.certificateDNSName options:0 range:NSMakeRange(0, configFileContents.length)];

    NSString *updatedConfigFilePath=[NSTemporaryDirectory() stringByAppendingPathComponent:[[NSUUID UUID] UUIDString]];
    [configFileContents writeToFile:updatedConfigFilePath atomically:NO encoding:NSUTF8StringEncoding error:nil];

    


    self.signTaskWrapper=[[TCTaskWrapperWithBlocks alloc] initWithStartBlock:^{

    } endBlock:^{

        if (self.signTaskWrapper.terminationStatus==0){
            if ([self.delegate respondsToSelector:@selector(identityCreatedAtURL:privateKeyURL:)]){

                [self.delegate identityCreatedAtURL:[url URLByAppendingPathExtension:@"cer"] privateKeyURL:[url URLByAppendingPathExtension:@"key"]];
            }

        }
        else {
            if ([self.delegate respondsToSelector:@selector(identityCreatedAtURL:privateKeyURL:)]){

                [self.delegate identityCreatedAtURL:nil privateKeyURL:nil];
            }
        }
    } outputBlock:^(NSString *output) {
        NSLog(@"%@",output);
    } errorOutputBlock:^(NSString *errorOutput) {
        NSLog(@"%@",errorOutput);
    } arguments:@[@"/usr/bin/openssl",@"x509",@"-req",@"-sha256",@"-extfile",updatedConfigFilePath,@"-extensions",@"server_ssl",@"-days",self.certificateValidDays, @"-in",[url.path stringByAppendingString:@".csr"],@"-signkey",[url.path stringByAppendingString:@".key"],@"-out",[url.path stringByAppendingString:@".cer"]]];

    [self.signTaskWrapper startProcess];

}
-(void)identityCreatedAtURL:(nullable NSURL *)certificateFileURL privateKeyURL:(nullable NSURL *)privateKeyPathURL{

    [USERDEFAULTS setObject:certificateFileURL.path forKey:CERTIFICATEPATH];
    [USERDEFAULTS setObject:privateKeyPathURL.path forKey:INDENTITYKEYPATH];

    [self dismissViewController:self];

    [[MDSPrivHelperToolController sharedHelper] trustCertificateAtPath:certificateFileURL.path withCallback:^(NSError * _Nonnull err) {

        dispatch_async(dispatch_get_main_queue(), ^{

            if (err) {
                [[NSAlert alertWithError:err] runModal];
                return;
            }
            NSAlert *alert=[[NSAlert alloc] init];

            alert.messageText=@"Identity Created";
            alert.informativeText=[NSString stringWithFormat:@"The certificate and private key have been created and saved to %@.\n\nThe certificate has also been added to the system keychain and trusted.",[certificateFileURL.path stringByDeletingLastPathComponent]];

            [alert addButtonWithTitle:@"OK"];
            [alert runModal];

            [[NSNotificationCenter defaultCenter] postNotificationName:SSLINFOCHANGED object:self];
        });
   }];
}
@end
