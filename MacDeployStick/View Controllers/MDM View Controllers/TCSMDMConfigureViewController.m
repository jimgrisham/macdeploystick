//
//  TCSMDMConfigureViewController.m
//  MDS
//
//  Created by Timothy Perfitt on 7/29/19.
//  Copyright © 2019 Twocanoes Software. All rights reserved.
//

#import "TCSMDMConfigureViewController.h"
#import "TCSMDMWebController.h"
#import "TCSCreateSelfSignedCertificateController.h"
#import "NSData+Base64.h"

#import "TCSMDMService.h"
@interface TCSMDMConfigureViewController () 
@property (strong) IBOutlet NSUserDefaultsController *userDefaultsController;

@end

@implementation TCSMDMConfigureViewController


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do view setup here.
}
- (void)prepareForSegue:(NSStoryboardSegue *)segue sender:(id)sender{

    if ([segue.identifier isEqualToString:@"createCertSegue"]){

        TCSCreateSelfSignedCertificateController*destController=segue.destinationController;
        destController.delegate=self;
    }

}

- (void)viewWillDisappear{

    [[NSNotificationCenter defaultCenter] postNotificationName:@"configurationChanged" object:self];


}
- (IBAction)savePublicKeyButtonPressed:(id)sender {

    [[TCSMDMWebController sharedController] sendPayload:nil toEndpoint:@"/v1/dep-tokens" httpAction:@"GET" onCompletion:^(NSInteger responseCode, NSData *responseData) {

        NSDictionary *depInfo=[NSJSONSerialization JSONObjectWithData:responseData options:0 error:nil];


        if ([depInfo objectForKey:@"public_key"]==nil){

            NSAlert *alert=[[NSAlert alloc] init];
            alert.messageText=@"Error Getting DEP token";
            alert.informativeText=@"There was an error getting the DEP token. Please verify the MDM settings and try again.";
            [alert runModal];
            return;
        }
        
        NSString *publicKey=[NSString stringWithFormat:@"-----BEGIN CERTIFICATE-----\n%@\n-----END CERTIFICATE-----",[depInfo objectForKey:@"public_key"]];

        NSSavePanel *savePanel=[NSSavePanel savePanel];
        savePanel.allowsOtherFileTypes=NO;
        savePanel.allowedFileTypes=@[@"pem"];
        savePanel.nameFieldStringValue=@"DEP Public Key";
        savePanel.message=@"Specify the name and location to save the public key to upload to the DEP portal";
        
        NSModalResponse res=[savePanel runModal];

        if (res!=NSModalResponseOK) {
            return;
        }

        NSURL *saveURL=savePanel.URL;
        NSData *publicKeyData=[publicKey dataUsingEncoding:NSUTF8StringEncoding];
        if ([publicKeyData writeToURL:saveURL atomically:NO]==NO){

            NSAlert *alert=[[NSAlert alloc] init];
            alert.messageText=@"Save file error";
            alert.informativeText=[NSString stringWithFormat:@"The file %@ could not be saved. Please check the location and try again",saveURL.path];
            [alert addButtonWithTitle:@"OK"];
            [alert runModal];


        }
        else {
            NSAlert *alert=[[NSAlert alloc] init];
            alert.messageText=@"Export Successful";
            alert.informativeText=@"The public key has been saved successfully. Please upload to Apple Business Manager or Apple School Manager and download the DEP Token";

            [alert addButtonWithTitle:@"OK"];
            [alert runModal];
        }


    } onError:^(NSError *error) {

        [[NSAlert alertWithError:error] runModal];
    }];

}
- (IBAction)importDEPTokens:(id)sender {
    //p7m_content

    NSOpenPanel *openPanel=[NSOpenPanel openPanel];
    openPanel.allowsOtherFileTypes=NO;
    openPanel.allowedFileTypes=@[@"p7m"];

    openPanel.message=@"Select the DEP token file (p7m):";
    NSModalResponse res=[openPanel runModal];

    if (res!=NSModalResponseOK) {
        return;
    }
    NSURL *openURL=openPanel.URL;

    NSError *err;
    NSData *tokens=[NSData dataWithContentsOfURL:openURL options:0 error:&err];

    if (!tokens){

        [[NSAlert alertWithError:err] runModal];
        return;
    }
    NSString *base64Tokens=[tokens base64EncodedString];

    [[TCSMDMWebController sharedController] sendPayload:@{@"p7m_content":base64Tokens} toEndpoint:@"/v1/dep-tokens" httpAction:@"PUT" onCompletion:^(NSInteger responseCode, NSData *responseData) {

    } onError:^(NSError *error) {

    }];


}
-(BOOL)isRunning{
    return [[TCSMDMService sharedManager] isRunning];
}
@end
