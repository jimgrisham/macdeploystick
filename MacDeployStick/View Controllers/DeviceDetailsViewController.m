//
//  DeviceDetailsViewController.m
//  MDS_appstore
//
//  Created by Timothy Perfitt on 9/2/19.
//  Copyright © 2019 Twocanoes Software. All rights reserved.
//

#import "DeviceDetailsViewController.h"
#import "MDMDataController.h"
#import "CertificateMO+CoreDataClass.h"
#import "SecurityInfoMO+CoreDataClass.h"
#import "ManagementStatusMO+CoreDataClass.h"
#import "FirewallSettingsMO+CoreDataClass.h"
#import "FirewallApplicationsMO+CoreDataClass.h"
#import "TCSConfigHelper.h"
#import "TCSMDMWebController.h"
#import "TCSMDMService.h"
#import "DeviceMO+CoreDataProperties.h"
#import "ProfileMO+CoreDataProperties.h"
#import "DeviceMO+CoreDataClass.h"
#import "TCSEraseDevicesPinViewController.h"
#import "TCSMDMAppsViewController.h"
@interface DeviceDetailsViewController () <TCSEraseDeviceConfirmationDelegateProtocol,TCSInstallAppsProtocol>
@property (strong) IBOutlet NSObjectController *deviceObjectController;
@property (strong) IBOutlet NSObjectController *securityInfoObjectController;
@property (weak) IBOutlet NSTableView *profilesTableView;
@property (weak) IBOutlet NSTabView *tabView;
@property (weak) MDMDataController *dataController;
@property (strong) NSArray *profiles;
@property (strong) SecurityInfoMO *securityInfo;
@property (strong) FirewallSettingsMO *firewallSetting;

@property (strong) ManagementStatusMO *managementStatus;
@property (strong) NSArray <FirewallApplicationsMO *> *firewallApplications;

@property (strong) FirmwarePasswordStatusMO *firmwarePasswordStatus;
@property (strong) IBOutlet NSArrayController *profilesArrayController;

@end

@implementation DeviceDetailsViewController
-(IBAction)removeProfile:(id)sender{

    if ([self.tabView.selectedTabViewItem.identifier isEqualToString:@"profiles"]){

        ProfileMO * profile=self.profilesArrayController.selectedObjects.firstObject;

        DeviceMO *device=profile.device;
        NSString *udid=device.udid;
        NSDictionary *profileDict=(NSDictionary *)profile.profileDict;
        if([profileDict objectForKey:@"PayloadIdentifier"]){
            if (udid && udid.length>0){
                [[TCSMDMWebController sharedController] deleteProfileFromDeviceWithUDID:udid profileIdentiifer:[profileDict objectForKey:@"PayloadIdentifier"]];
            }
        }

    }
}
- (void)viewDidLoad {

    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(update:) name:TCSNOTIFICATIONMDMCLIENTUPDATED object:nil];

    [self refreshSettings:self];

}
-(IBAction)block:(id)sender{
    NSAlert *alert=[[NSAlert alloc] init];
      alert.messageText=@"Block Device?";
      alert.informativeText=@"Are you sure you want to block this device? The next time the device checks in to the MDM server, it will be unenrolled.";

      [alert addButtonWithTitle:@"Block"];
      [alert addButtonWithTitle:@"Cancel"];
      NSInteger res=[alert runModal];
      if (res==NSAlertSecondButtonReturn) return;

    [[TCSMDMWebController sharedController] blockDevices:@[self.device] onCompletion:^(BOOL success) {
        if (success==NO){
            NSAlert *alert=[[NSAlert alloc] init];
            alert.messageText=@"Error blocking device";
            alert.informativeText=@"There was an error blocking the device. Please check the log";

            [alert addButtonWithTitle:@"OK"];
            [alert runModal];
        }


    }];

}
-(IBAction)unblock:(id)sender{
    NSAlert *alert=[[NSAlert alloc] init];
      alert.messageText=@"Unblock Device?";
      alert.informativeText=@"Are you sure you want to unblock this device? Any existing blocks will be removed.";

      [alert addButtonWithTitle:@"Unblock"];
      [alert addButtonWithTitle:@"Cancel"];
      NSInteger res=[alert runModal];
      if (res==NSAlertSecondButtonReturn) return;

    [[TCSMDMWebController sharedController] unblockDevices:@[self.device] onCompletion:^(BOOL success) {
        if (success==NO){
            NSAlert *alert=[[NSAlert alloc] init];
            alert.messageText=@"Error unblocking device";
            alert.informativeText=@"There was an error unblocking the device. Please check the log";

            [alert addButtonWithTitle:@"OK"];
            [alert runModal];
        }

      }];

}
-(IBAction)erase:(id)sender{

    [self performSegueWithIdentifier:@"eraseDeviceSegue" sender:self];
}
-(void)eraseDevices:(NSArray *)inDevices pin:(NSString *)inPin{
    TCSMDMWebController *webController=[TCSMDMWebController sharedController];
    [webController eraseDevices:inDevices withPIN:inPin onCompletion:^(BOOL success) {

    }];


}

-(void)mdmCommand:(NSMenuItem *)menu {
    [self.mdmDelegate performMDMCommand:menu.representedObject pin:nil filePayload:nil forDevices:@[self.device]];
    

}
-(IBAction)refreshDeviceDetails:(id)sender{
    if ([[TCSMDMService sharedManager] isRunning]==NO) {

        NSAlert *alert=[[NSAlert alloc] init];
        alert.messageText=@"Service Status";
        alert.informativeText=@"The MDM Service is not running. Please start the service and try again.";
        [alert addButtonWithTitle:@"OK"];
        [alert runModal];

        return;
    }


    [[TCSMDMWebController sharedController] refreshDataFromDeviceWithUDID:self.device.udid];


}
-(void)refreshSettings:(id)sender{
    dispatch_async(dispatch_get_main_queue(), ^{

        [self.deviceObjectController fetch:self];
        self.profiles= [[[self device] installedProfiles] allObjects];
        self.securityInfo= [[self device] securityInfo];
        self.managementStatus= [[self.securityInfo managementStatus] anyObject];;
        self.firewallSetting= [[self.securityInfo firewallSettings] anyObject];
        self.firmwarePasswordStatus= [[self.securityInfo firmwarePasswordStatus] anyObject];
        self.firewallApplications=[[self.firewallSetting firewallApplications] allObjects];

    });
}
-(void)update:(NSNotification *)inNotification{

    if ([[inNotification userInfo] objectForKey:TCSNOTIFICATIONUDIDKEY]){
        NSString *udid=[[inNotification userInfo] objectForKey:TCSNOTIFICATIONUDIDKEY];

        if ([[self.device.udid lowercaseString] isEqualToString:[udid lowercaseString]]){
            [self refreshSettings:self];
        }

    }

}
- (void)prepareForSegue:(NSStoryboardSegue *)segue sender:(id)sender{
    if ([segue.identifier isEqualToString:@"installAppsSegue2"]){
        TCSMDMAppsViewController * destination=(TCSMDMAppsViewController *)segue.destinationController;
        destination.delegate=self;
        destination.devices=@[self.device];


    }
    else if([segue.identifier isEqualToString:@"eraseDeviceSegue"]){
       TCSEraseDevicesPinViewController * destination=(TCSEraseDevicesPinViewController *)segue.destinationController;
            destination.delegate=self;
            destination.devices=@[self.device];
    }

}
-(NSArray *)certificates{


    return [[[self device] certificates] allObjects];

}
-(IBAction)installApps:(id)sender{

    [self performSegueWithIdentifier:@"installAppsSegue2" sender:self];

}

- (void)installApps:(nonnull NSArray *)appArray toDevices:(nonnull NSArray<DeviceMO *> *)devices {
    [[TCSMDMWebController sharedController] installApps:appArray onDevices:devices onCompletion:^(BOOL success) {

        if(success==NO){
            NSAlert *alert=[[NSAlert alloc] init];
            alert.messageText=@"Error Installing App";
            alert.informativeText=@"There was an error installing on or more application. Please check the log.";

            [alert addButtonWithTitle:@"OK"];
            [alert runModal];
        }


    }];

}


@end
