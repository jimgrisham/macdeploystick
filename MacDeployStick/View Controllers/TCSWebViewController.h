//
//  TCSMunkiViewController.h
//  MDS
//
//  Created by Timothy Perfitt on 3/9/20.
//  Copyright © 2020 Twocanoes Software. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import "TCSServiceViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface TCSWebViewController : TCSServiceViewController <TCSServiceViewProtocol>

@end

NS_ASSUME_NONNULL_END
