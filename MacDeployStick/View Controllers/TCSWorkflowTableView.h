//
//  TCSWorkflowTableView.h
//  MDS
//
//  Created by Timothy Perfitt on 11/3/19.
//  Copyright © 2019 Twocanoes Software. All rights reserved.
//

#import <Cocoa/Cocoa.h>

NS_ASSUME_NONNULL_BEGIN

@interface TCSWorkflowTableView : NSTableView

@end

NS_ASSUME_NONNULL_END
