//
//  TCSDownloadMacOSViewController.h
//  MDS
//
//  Created by Timothy Perfitt on 10/6/19.
//  Copyright © 2019 Twocanoes Software. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import "TCSMainWindowRightViewController.h"
NS_ASSUME_NONNULL_BEGIN

@interface TCSDownloadMacOSViewController : TCSMainWindowRightViewController

@end

NS_ASSUME_NONNULL_END
