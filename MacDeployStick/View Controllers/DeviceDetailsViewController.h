//
//  DeviceDetailsViewController.h
//  MDS_appstore
//
//  Created by Timothy Perfitt on 9/2/19.
//  Copyright © 2019 Twocanoes Software. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import "DeviceMO+CoreDataClass.h"
#import "TCSMDMViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface DeviceDetailsViewController : NSViewController
-(IBAction)refreshDeviceDetails:(id)sender;
@property (assign) id <TCSMDMActionsProtocol> mdmDelegate;

@property (strong) DeviceMO *device;
@end

NS_ASSUME_NONNULL_END
