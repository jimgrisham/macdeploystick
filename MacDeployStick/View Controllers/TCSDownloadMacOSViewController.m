//
//  TCSDownloadMacOSViewController.m
//  MDS
//
//  Created by Timothy Perfitt on 10/6/19.
//  Copyright © 2019 Twocanoes Software. All rights reserved.
//

#import "TCSDownloadMacOSViewController.h"
#import "TCTaskWrapperWithBlocks.h"
#import "MDSPrivHelperToolController.h"
#import "TCSLogFileWindowController.h"
#import <IOKit/pwr_mgt/IOPMLib.h>
@interface TCSDownloadMacOSViewController (){
    IOPMAssertionID assertionID;
}


@property (strong) NSArray *catalogNames;
@property (strong) NSMutableArray *catalog;
@property (assign) NSInteger selectedCatalogIndex;
@property (strong) TCTaskWrapperWithBlocks *wrapper;
@property (strong) IBOutlet NSArrayController *catalogArrayController;
@property (assign) BOOL isRunning;
@property (assign) BOOL isCancelling;
@property (strong) TCSLogFileWindowController *logWindowController;
@property (strong) NSMutableArray *selectedProductsQueue;
@property (strong) NSString *selectedCatalogName;
@property (strong) NSString *workingPath;
@end

@implementation TCSDownloadMacOSViewController
-(void)turnOffSleep:(NSString *)reason{
    NSLog(@"Setting to not sleep....");

    IOPMAssertionCreateWithName(kIOPMAssertionTypePreventUserIdleSystemSleep,
                                kIOPMAssertionLevelOn, (__bridge CFStringRef)reason, &assertionID);


}
- (void)awakeFromNib{
    NSSortDescriptor *versionSortDescriptor = [NSSortDescriptor sortDescriptorWithKey:@"version" ascending:YES];
    NSArray *arrayOfSortDescriptors = [NSArray arrayWithObject:versionSortDescriptor];
    [self.catalogArrayController setSortDescriptors:arrayOfSortDescriptors];

}
-(void)allowSleep{
    NSLog(@"Setting the system to allow sleep...");
    if (assertionID) IOPMAssertionRelease(assertionID);

}
- (IBAction)catalogChanged:(id)sender {
    [self loadCatalog:self];
    
}

- (IBAction)showLogButtonPressed:(id)sender {

    [[NSWorkspace sharedWorkspace] openFile:@"/Library/Logs/mdshelper.log" withApplication:@"Console"];

//    if (!self.logWindowController){
//        self.logWindowController=[[TCSLogFileWindowController alloc] initWithWindowNibName:@"TCSLogFileWindowController"];
//        self.logWindowController.logFile=@"/Library/Logs/mdshelper.log";
//
//    }
//    [self.logWindowController.window makeKeyAndOrderFront:self];

}

- (IBAction)cancelButtonPressed:(id)sender {

    [[MDSPrivHelperToolController sharedHelper] stopRunningProcessesWithCallback:^(BOOL success) {

        dispatch_async(dispatch_get_main_queue(), ^{
            self.isRunning=NO;
            self.isCancelling=YES;

        });
    }];
}

- (void)viewDidLoad {

    [super viewDidLoad];
    self.isRunning=NO;
    self.isCancelling=NO;
    NSMutableDictionary *catalogDict=[NSMutableDictionary dictionaryWithContentsOfFile:@"/System/Library/PrivateFrameworks/Seeding.framework/Versions/Current/Resources/SeedCatalogs.plist"];


    self.catalogNames=@[@"Production"];
    self.catalogNames=[self.catalogNames arrayByAddingObjectsFromArray:catalogDict.allKeys];

    self.catalog=[NSMutableArray array];
    [self loadCatalog:self];

}

-(void)loadCatalog:(id)sender{

    NSMutableString *catalogPlistString=[NSMutableString string];
    NSString *selectedCatalogName=[self.catalogNames objectAtIndex:self.selectedCatalogIndex];
    NSString *installInstallMacOSScriptPath=[[NSBundle mainBundle] pathForResource:@"installinstallmacos" ofType:@"py"];

    NSArray *arguments=@[installInstallMacOSScriptPath,@"--seedprogram",selectedCatalogName,@"--workdir",@"/tmp",@"--list"];

    if ([selectedCatalogName isEqualToString:@"Production"]){
        arguments=@[installInstallMacOSScriptPath,@"--workdir",@"/tmp",@"--list"];
    }

    self.wrapper=[[TCTaskWrapperWithBlocks alloc] initWithStartBlock:^{

        self.isRunning=YES;
    } endBlock:^{
        self.isRunning=NO;

        NSError *err;
        NSPropertyListFormat format;
        NSDictionary *catalogDict=[NSPropertyListSerialization propertyListWithData:[catalogPlistString dataUsingEncoding:NSUTF8StringEncoding] options:0 format:&format error:&err];



        if (!catalogDict) {
            NSLog(@"%@",err.localizedDescription);
            return;
        }
        [self updateCatalog:catalogDict];

    } outputBlock:^(NSString *output) {
        [catalogPlistString appendString:output];
    } errorOutputBlock:^(NSString *errorOutput) {
        NSLog(@"%@",errorOutput);
    } arguments:arguments] ;

    [self.wrapper startProcess];

}

-(void)updateCatalog:(NSDictionary *)inDict{

    dispatch_async(dispatch_get_main_queue(), ^{

        [self.catalogArrayController removeObjects:[self.catalogArrayController arrangedObjects]];
        [inDict enumerateKeysAndObjectsUsingBlock:^(NSString * key, NSDictionary *attributes, BOOL * _Nonnull stop) {
            NSMutableDictionary *catalogDictionary=[NSMutableDictionary dictionaryWithDictionary:attributes];
            catalogDictionary[@"productId"]=key;
            [self.catalogArrayController addObject:catalogDictionary];
        }];
        [self.catalogArrayController rearrangeObjects];
        [self.catalogArrayController setSelectionIndex:[self.catalogArrayController.arrangedObjects count]-1];
        });
}

- (IBAction)downloadButtonPressed:(id)sender {

    NSString *scriptPath=[[NSBundle mainBundle] pathForResource:@"installinstallmacos" ofType:@"py"];


    NSFileManager *fm=[NSFileManager defaultManager];

    NSDictionary *dict=[fm attributesOfItemAtPath:scriptPath error:nil];

    if (dict && [dict objectForKey: NSFilePosixPermissions] && [[dict objectForKey: NSFilePosixPermissions] intValue]==0755 && [dict objectForKey:NSFileOwnerAccountID] && [[dict objectForKey:NSFileOwnerAccountID] intValue]==0){

        NSOpenPanel *openPanel=[NSOpenPanel openPanel];

        openPanel.canChooseDirectories=YES;
        openPanel.canChooseFiles=NO;
        openPanel.message=@"Select an output folder:";
        NSModalResponse res=[openPanel runModal];

        if (res!=NSModalResponseOK) {
            return;
        }

        self.isRunning=YES;
        self.isCancelling=NO;

        self.workingPath=openPanel.URL.path;
        self.selectedCatalogName=[self.catalogNames objectAtIndex:self.selectedCatalogIndex];
        self.selectedProductsQueue=[NSMutableArray arrayWithArray:[self.catalogArrayController selectedObjects]];
        
        [self kickoffNextProcess];

    }
    else {
        NSAlert *alert=[[NSAlert alloc] init];
        alert.messageText=@"Invalid Permissions";
        alert.informativeText=[NSString stringWithFormat:@"The script to create the disk image of macOS must be owned by root and only be writable by the owner (755).\n\nPlease verify it hasn't been tampered with and update to the correct permissions and then try again.\n\nScript path: %@",scriptPath];

        [alert addButtonWithTitle:@"OK"];
        [alert runModal];

    }



}

-(void)kickoffNextProcess{
    if (self.selectedProductsQueue.count>0){
        NSDictionary *currentProduct=[self.selectedProductsQueue firstObject];
        NSString *productID=[currentProduct objectForKey:@"productId"];
        [self.selectedProductsQueue removeObject:currentProduct];

        [self turnOffSleep:@"Download and build macOS installer"];
        [[MDSPrivHelperToolController sharedHelper] installInstallMacOSWithProductID:productID catalog:self.selectedCatalogName workingPath:self.workingPath withCallback:^(BOOL success) {

            if (success==YES) {
                if (self.isCancelling==NO) {
                    [self kickoffNextProcess];
                }
                else {
                    self.isRunning=NO;

                }
            }
            else {
                [self showFinishMessageWasSuccessfull:NO];
                self.isRunning=NO;

            }
        }];
    }
    else {
        [self showFinishMessageWasSuccessfull:YES];

    }
}
-(void)showFinishMessageWasSuccessfull:(BOOL)success{

    [self allowSleep];
    dispatch_async(dispatch_get_main_queue(), ^{
        self.isRunning=NO;
        NSAlert *alert=[[NSAlert alloc] init];
        if (success==NO) {
            alert.messageText=@"Error";
            alert.informativeText=@"The OS Image was not created successfully. Please grant /Library/PrivilegedHelperTools/com.twocanoes.mdshelpertool Full Disk access in System Preferences->Privacy->Full Disk Access and reboot. This is required so MDS can access removable volumes.";

        }
        else {
            alert.messageText=@"Completed";
            alert.informativeText=@"The OS Image was completed successfully.";

            [self dismissController:self];
        }

        [alert addButtonWithTitle:@"OK"];
        [alert runModal];
    });

}
@end
