//
//  TCSServerMainViewController.m
//  MDS
//
//  Created by Timothy Perfitt on 3/7/20.
//  Copyright © 2020 Twocanoes Software. All rights reserved.
//

#import "TCSServerMainViewController.h"
#import "TCSServiceNode.h"
#import "TCSStatusTableCellView.h"
#import "TCSConstants.h"
#import "TCSServiceUpdateManager.h"
#import "DeploySettings.h"
#import "TCSWebServiceController.h"
#import "TCSMDMService.h"
#import "TCSSaveMasterViewController.h"
#import "TCSUtility.h"
@interface TCSServerMainViewController () <NSOutlineViewDelegate,NSOutlineViewDataSource> 
@property (strong) IBOutlet NSTreeController *servicesTreeController;
@property (weak) IBOutlet NSOutlineView *servicesOutlineView;
@property (strong) TCSServiceNode *rootServiceNode;;
@property (strong) TCSServiceNode *currentSelectedItem;

@end

@implementation TCSServerMainViewController


- (NSInteger)outlineView:(NSOutlineView *)outlineView numberOfChildrenOfItem:(TCSServiceNode *)item{

    if (item==nil) return [self.rootServiceNode.children count];
    return [item.children count];
}
- (CGFloat)outlineView:(NSOutlineView *)outlineView heightOfRowByItem:(TCSServiceNode *)item{
    if (item.children.count==0) return 25;
    return 25;

}

- (id)outlineView:(NSOutlineView *)outlineView child:(NSInteger)index ofItem:(TCSServiceNode *)item{

    if (item==nil){

        return [self.rootServiceNode.children objectAtIndex:index];
    }
    return [item.children objectAtIndex:index];
}
- (BOOL)outlineView:(NSOutlineView *)outlineView isItemExpandable:(TCSServiceNode *)item{

    if (item.children.count==0) return NO;
    return YES;
}

-(IBAction)syncNow:(id)sender{

    NSStoryboard *mainStoryboard = [NSStoryboard storyboardWithName:@"Main" bundle: nil];

    TCSSaveMasterViewController *vc= [mainStoryboard instantiateControllerWithIdentifier:@"savemasterviewcontroller"];

    [self presentViewControllerAsSheet:vc];


    dispatch_async(dispatch_get_main_queue(), ^{

        vc.status=@"Mounting Disk Image...";
    });

    [vc performSelector:@selector(syncNow:) withObject:self afterDelay:0.5];

}

-(void)updateAvailable:(NSNotification *)not{
    NSString *updatedService=not.userInfo[@"service"];
    NSArray *services=[[self.rootServiceNode.children objectAtIndex:2] children];
    TCSServiceNode *munkiNode=[services objectAtIndex:2];
    TCSServiceNode *munkireportNode=[services objectAtIndex:3];

    if ([updatedService isEqualToString:@"munki"]){
        munkiNode.statusImage=nil;
    }
    else  if ([updatedService isEqualToString:@"munkireport"]){
        munkireportNode.statusImage=nil;
    }
    NSDictionary *updates=[[TCSServiceUpdateManager updateManager] updatesAvailable];
    if ([updates objectForKey:@"munki"]){
        NSDictionary *updateInfo=[updates objectForKey:@"munki"];
        NSString *latestVersion=[updateInfo objectForKey:@"latest_version"];
        if(latestVersion) {
            NSString *installedVersion=[updateInfo objectForKey:@"installed_version"];
            if ([installedVersion hasPrefix:latestVersion]==NO) {

                munkiNode.statusImage=[NSImage imageNamed:NSImageNameStatusPartiallyAvailable];
            }
            else {
                munkiNode.statusImage=nil;
            }
        }

    }
    if ([updates objectForKey:@"munkireport"]){
        NSDictionary *updateInfo=[updates objectForKey:@"munkireport"];
        NSString *latestVersion=[updateInfo objectForKey:@"latest_version"];
        NSString * installedVersion=[updateInfo objectForKey:@"installed_version"];

        if (latestVersion){
            if ([installedVersion hasPrefix:latestVersion]==NO) {

                munkireportNode.statusImage=[NSImage imageNamed:NSImageNameStatusPartiallyAvailable];
            }
            else {
                munkireportNode.statusImage=nil;
            }
        }

    }
    [self reloadSidebar:self];
}

-(void)readloadSidebarNotificationReceived:(NSNotification *)not{

    [self reloadSidebar:self];
}
-(void)reloadSidebar:(id)sender{
    dispatch_async(dispatch_get_main_queue(), ^{
        
        NSIndexSet *is=[self.servicesOutlineView selectedRowIndexes];
        [self.servicesOutlineView reloadData];
        [self.servicesOutlineView selectRowIndexes:is byExtendingSelection:YES];
    });

}
- (void)viewDidLoad {
    [super viewDidLoad];

    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateAvailable:) name:TCSNOTIFICATIONUPDATEAVAILABLE object:nil];
    [[TCSServiceUpdateManager updateManager] checkForMunkiUpdate:self];
    [[TCSServiceUpdateManager updateManager] checkForMunkiReportUpdate:self];
    NSUserDefaults *ud=[NSUserDefaults standardUserDefaults];

    [TCSUtility updateTLSCertIfNeed:self];



    [self.view.window setMinSize:NSMakeSize(1000,600)];

    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(readloadSidebarNotificationReceived:) name:TCSNOTIFICATIONSIDEBARRELOAD object:nil];
    NSMutableArray *nodeSections=[NSMutableArray array];

    NSString *sectionsPath=[[NSBundle mainBundle] pathForResource:@"Sections" ofType:@"plist"];

    NSArray *sectionsInfo=[NSArray arrayWithContentsOfFile:sectionsPath];
    [sectionsInfo enumerateObjectsUsingBlock:^(NSDictionary *sectionInfo, NSUInteger idx, BOOL * _Nonnull stop) {

        TCSServiceNode *sectionNode=[[TCSServiceNode alloc] init];
        sectionNode.name=[sectionInfo objectForKey:@"name"];
        sectionNode.icon=[sectionInfo objectForKey:@"icon"];


        NSArray *children=[sectionInfo objectForKey:@"children"];
        NSMutableArray *nodeChildren=[NSMutableArray array];
        [children enumerateObjectsUsingBlock:^(NSDictionary *currChild, NSUInteger idx, BOOL * _Nonnull stop) {

            TCSServiceNode *node=[[TCSServiceNode alloc] init];
            node.name=[currChild objectForKey:@"name"];
            node.icon=[currChild objectForKey:@"icon"];
            node.storyboardID=[currChild objectForKey:@"storyboardID"];
            node.storyboardName=[currChild objectForKey:@"storyboardName"];

            if ([currChild objectForKey:@"hasStatusIcon"]){
                node.hasStatusIcon=[[currChild objectForKey:@"hasStatusIcon"] boolValue];
            }

            if ([currChild objectForKey:@"hasBadge"]){
                node.hasBadge=[[currChild objectForKey:@"hasBadge"] boolValue];
            }
            [nodeChildren addObject:node];
        }];
        sectionNode.children=[NSArray arrayWithArray:nodeChildren];
        [nodeSections addObject:sectionNode];
    }];

    self.rootServiceNode=[[TCSServiceNode alloc] init];
    ////   _rootServicesNode.title=@"root";
    self.rootServiceNode.children=[NSArray arrayWithArray:nodeSections];


    [self.servicesOutlineView reloadData];
    [self.servicesOutlineView expandItem:nil expandChildren:YES];

    if (![ud objectForKey:CERTIFICATEPATH] && ![ud objectForKey:INDENTITYKEYPATH] && ![ud objectForKey:@"TCSSuppressCreateSSLCertificate"] ){
        [self performSelector:@selector(showCertificatePrompt:) withObject:self afterDelay:1];

    }



}

-(void)showCertificatePrompt:(id)sender{
    NSUserDefaults *ud=[NSUserDefaults standardUserDefaults];
    NSAlert *alert=[[NSAlert alloc] init];
    alert.messageText=@"Create SSL Certificate";
    alert.informativeText=@"MDS does not have an SSL certificate defined. Do you want to create a new self-signed certificate now?";
    alert.showsSuppressionButton=YES;
    [alert addButtonWithTitle:@"Create Certificate"];
    [alert addButtonWithTitle:@"Not Now"];
    NSInteger res=[alert runModal];

    if (alert.suppressionButton.state == NSOnState) {
        [ud setBool: YES forKey:@"TCSSuppressCreateSSLCertificate"];
    }

    if (res==NSAlertSecondButtonReturn) return;
        NSIndexSet *i=[NSIndexSet indexSetWithIndex:2];
        [self.servicesOutlineView selectRowIndexes:i byExtendingSelection:NO];

}
- (void)viewDidAppear{

}
- (BOOL)outlineView:(NSOutlineView *)outlineView shouldShowOutlineCellForItem:(id)item {
    return NO;
}
-(NSView *)outlineView:(NSOutlineView *)outlineView viewForTableColumn:(NSTableColumn *)tableColumn item:(TCSServiceNode *)item{
    TCSStatusTableCellView *result ;
    if (item.isLeaf==NO){
        result= [outlineView makeViewWithIdentifier:@"serviceViewHeader" owner:self];
    }
    else {
        result=[outlineView makeViewWithIdentifier:@"serviceView" owner:self];

        if (item.hasStatusIcon==YES) {
            result.serviceStatusImageView.hidden=NO;
            result.serviceStatusImageView.image=[NSImage imageNamed:NSImageNameStatusNone];

            if ([item.name isEqualToString:@"MicroMDM"] && [[TCSMDMService sharedManager] isRunning]==YES){
                result.serviceStatusImageView.image=[NSImage imageNamed:NSImageNameStatusAvailable];
            }



            BOOL webServiceRunning=[[TCSWebServiceController sharedController] isRunning];

            if (webServiceRunning==YES){
                if ([item.name isEqualToString:@"MunkiReport"] && [[TCSWebServiceController sharedController]  munkiReportWebserverSettings] && [[[TCSWebServiceController sharedController]  munkiReportWebserverSettings] isActive]==YES){
                    result.serviceStatusImageView.image=[NSImage imageNamed:NSImageNameStatusAvailable];
                }
                if ([item.name isEqualToString:@"Munki"] && [[TCSWebServiceController sharedController]  munkiWebserverSettings] && [[[TCSWebServiceController sharedController]  munkiWebserverSettings] isActive]==YES){
                    result.serviceStatusImageView.image=[NSImage imageNamed:NSImageNameStatusAvailable];
                }
                if ([item.name isEqualToString:@"Web Service"]){
                    result.serviceStatusImageView.image=[NSImage imageNamed:NSImageNameStatusAvailable];
                }

            }

        }
        else {
            result.serviceStatusImageView.hidden=YES;

        }



        NSImage *image=[NSImage imageNamed:item.icon];;
        [image setTemplate:YES];
        result.imageView.image=image;
        result.statusImageView.image=item.statusImage;//[NSImage imageNamed:NSImageNameStatusPartiallyAvailable];



        if (item.hasBadge==YES) {

            NSInteger workflowCount=[[[DeploySettings sharedSettings] workflows] count];
            if (workflowCount>0){
                result.badgeBackgroundImageView.image=[NSImage imageNamed:@"badge"];
                result.badgeTextField.hidden=NO;
                result.badgeTextField.stringValue=[NSString stringWithFormat:@"%li",workflowCount];
            }
            else {

                result.badgeBackgroundImageView.image=nil;
                result.badgeTextField.hidden=YES;

            }
        }
        else {
            result.badgeBackgroundImageView.image=nil;
            result.badgeTextField.hidden=YES;
        }
    }
    result.textField.stringValue=item.name;

    return result;
}
- (void)outlineViewSelectionDidChange:(NSNotification *)notification{

    TCSServiceNode *selectedItem = [self.servicesOutlineView itemAtRow:[self.servicesOutlineView selectedRow]];

    if (selectedItem.storyboardName && selectedItem.storyboardID && selectedItem != self.currentSelectedItem) {
        [self.delegate selectedViewControllerWithStoryboardName:selectedItem.storyboardName storyboardID:selectedItem.storyboardID];
        self.currentSelectedItem=selectedItem;
    }
}
- (BOOL)outlineView:(NSOutlineView *)outlineView shouldSelectItem:(TCSServiceNode *)node{

    return [node isLeaf];

}

-(void)dealloc{


    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

@end
