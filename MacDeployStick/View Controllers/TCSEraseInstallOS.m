//
//  TCSEraseInstallOS.m
//  MacDeployStick
//
//  Created by Timothy Perfitt on 1/2/19.
//  Copyright © 2019 Twocanoes Software. All rights reserved.
//

#import "TCSEraseInstallOS.h"
#import "TCSUtility.h"
#import "MDSPrivHelperToolController.h"
#import <SecurityInterface/SFAuthorizationView.h>

#define LOGFILE @"/Library/Logs/mdshelper.log"

#import "TCSCreateBootableMediaStatusViewController.h"
@interface TCSEraseInstallOS () <TCSCreateBootableMediaStatusFeedbackProtocol>
@property (strong) NSString *macOSInstallerPath;
@property (strong) NSImage *macOSInstallerImage;
@property (strong) NSString *targetVolumePath;
@property (strong) NSImage *targetVolumeImage;

@property (strong) NSString *command;
@property (nonatomic,strong) NSArray *listOfMedia;
@property (weak) IBOutlet SFAuthorizationView *authenticationView;
@property (assign) BOOL isAuthorized;

@end

@implementation TCSEraseInstallOS 


-(void)viewWillAppear{
    self.listOfMedia =[TCSUtility listOfMedia];

}
- (IBAction)targetVolumePopupChanged:(id)sender {
    [self updateCommandString];
}
- (IBAction)createBootableMacVolumeButtonPressed:(id)sender {
    if (self.isAuthorized==NO) return;

//    NSFileManager *fm=[NSFileManager defaultManager];
//
//    NSError *err;
//    NSArray *contents;
//    contents=[fm contentsOfDirectoryAtPath:self.targetVolumePath error:&err];
//    if(!contents){
//
//
//        NSAlert *alert=[[NSAlert alloc] init];
//        alert.messageText=@"Access Error";
//        alert.informativeText=@"MDS does not have access to the selected volume. Please open System Preferences->Security & Privacy and allow MDS full disk access";
//
//        [alert addButtonWithTitle:@"OK"];
//        [alert runModal];
//
//        return;
//    }



    NSAlert *alert=[[NSAlert alloc] init];
    alert.messageText=@"Install macOS on External Volume";
    alert.informativeText=[NSString stringWithFormat:@"Are you sure you want to erase and create a bootable volume on %@? THIS WILL COMPLETELY ERASE ALL DATA ON THE DISK",self.targetVolumePath];

    [alert addButtonWithTitle:@"Erase and Create Bootable Volume"];
    [alert addButtonWithTitle:@"Cancel"];
    NSInteger res=[alert runModal];
    if (res==NSAlertSecondButtonReturn) return;

    [self performSegueWithIdentifier:@"TCSCreateBootableVolumeStoryboardID" sender:self];
    [[MDSPrivHelperToolController sharedHelper] createMacOSInstallVolume:self.targetVolumePath withInstaller:self.macOSInstallerPath callback:^(BOOL isDone, NSString * _Nonnull statusMsg, NSError * err) {
            if (isDone==YES){
                dispatch_async(dispatch_get_main_queue(), ^{

                    [self dismissViewController:self.presentedViewControllers.firstObject];
                    if (err) {

                        if (err.code!=-100){//not cancelled
                            [[NSAlert alertWithError:err] runModal];
                        }
                    }
                    else {

                        NSAlert *alert=[[NSAlert alloc] init];
                        alert.messageText=@"Success";
                        alert.informativeText=@"macOS has been installed successfully on the selected volume.";

                        [alert addButtonWithTitle:@"OK"];
                        [alert runModal];

                    }
                });
            }
            else {

                NSLog(@"%@",statusMsg);

            }

    }];
}

-(IBAction)selectMacOSInstaller:(id)sender{
    NSOpenPanel *openPanel=[NSOpenPanel openPanel];
    openPanel.canChooseFiles=YES;
    openPanel.canChooseDirectories=NO;
    openPanel.allowedFileTypes=@[@"app"];
    openPanel.allowsMultipleSelection=NO;
    openPanel.message=@"Please select a macOS installer app";
    NSModalResponse res=[openPanel runModal];

    if (res==NSModalResponseOK) {
        NSURL *selectedFileURL=[openPanel URL];
        NSURL *createInstallMediaURL=[selectedFileURL URLByAppendingPathComponent:@"Contents/Resources/createinstallmedia"];
        if ([[NSFileManager defaultManager] fileExistsAtPath:createInstallMediaURL.path]==YES){

            self.macOSInstallerPath=[openPanel URL].path;
            [self updateCommandString];
        }
        else {
            NSAlert *alert=[[NSAlert alloc] init];
            alert.messageText=@"Invalid Installer";

            alert.informativeText=@"The installer did not contain createinstallmedia. Please select a valid macOS installer";


            [alert runModal];
            return;

        }

    }

}
-(void)updateCommandString{


    if (self.targetVolumePath && self.macOSInstallerPath){
        NSString *createInstallMediaPath=[self.macOSInstallerPath stringByAppendingPathComponent:@"Contents/Resources/createinstallmedia"];
        self.command=[NSString stringWithFormat:@"\"%@\" --volume \"%@\" --nointeraction",createInstallMediaPath,self.targetVolumePath];


    }
    else self.command=nil;
}
- (void)cancelOperation:(nonnull id)sender {

    [[MDSPrivHelperToolController sharedHelper] stopRunningProcessesWithCallback:^(BOOL success) {
    }];


}

- (void)showLog:(nonnull id)sender {
    [[NSWorkspace sharedWorkspace] openFile:LOGFILE withApplication:@"Console"];


}

- (void)prepareForSegue:(NSStoryboardSegue *)segue sender:(id)sender{


    TCSCreateBootableMediaStatusViewController *statusViewController=segue.destinationController;
    statusViewController.delegate=self;
}
-(void)dealloc{
    [self.authenticationView setAutoupdate:NO];
    self.authenticationView.delegate=nil;
}
- (void)viewDidLoad {
    [super viewDidLoad];

    [self.authenticationView setString:TCSAUTHRIGHT];

    [self.authenticationView setAutoupdate:YES interval:5*60];
    self.authenticationView.delegate=self;

    [self.authenticationView updateStatus:self];
}
- (void)authorizationViewDidAuthorize:(SFAuthorizationView *)view{
    self.isAuthorized=YES;
    [MDSPrivHelperToolController sharedHelper].authorization=view.authorization;

}
- (void)authorizationViewDidDeauthorize:(SFAuthorizationView *)view{
    self.isAuthorized=NO;
}

@end
