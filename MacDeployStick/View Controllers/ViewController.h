//
//  ViewController.h
//  MacDeployStick
//
//  Created by Timothy Perfitt on 12/22/18.
//  Copyright © 2018 Twocanoes Software. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import "DeploySettings.h"
#import "TCSMainWindowRightViewController.h"
@interface ViewController : TCSMainWindowRightViewController  <NSTextViewDelegate>
//@property (nonatomic, strong) DeploySettings *deploySettings;
-(DeploySettings *)deploySettings;
- (IBAction)connectToWifiButtonPressed:(id)sender;

-(IBAction)checkForResources:(id)sender;
-(IBAction)saveMaster:(id)sender;
//- (IBAction)createAutomatonButtonPressed:(id)sender;
//- (IBAction)configureAutomatonButtonPressed:(id)sender;
//- (IBAction)createChromebookAutomatonPressed:(id)sender;
//- (IBAction)configureChromebookAutomatonPressed:(id)sender;
-(void)selectWorkflow:(TCSWorkflow *)workflow;
@end

