//
//  TCSWorkflowTableView.m
//  MDS
//
//  Created by Timothy Perfitt on 11/3/19.
//  Copyright © 2019 Twocanoes Software. All rights reserved.
//

#import "TCSWorkflowTableView.h"
#import "TCSConfigHelper.h"
@implementation TCSWorkflowTableView

- (void)drawRect:(NSRect)dirtyRect {
    [super drawRect:dirtyRect];
    
    // Drawing code here.
}
- (void)keyDown:(NSEvent *)event{

    if ([event.characters isEqualToString:@" "]){

        [[NSNotificationCenter defaultCenter] postNotificationName:TCSSelectKeyPressed object:self];
    }

    else {
        [super keyDown:event];
    }


}

@end
