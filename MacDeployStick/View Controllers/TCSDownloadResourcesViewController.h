//
//  TCSDownloadResourcesViewController.h
//  MacDeployStick
//
//  Created by Timothy Perfitt on 1/5/19.
//  Copyright © 2019 Twocanoes Software. All rights reserved.
//

#import <Cocoa/Cocoa.h>

NS_ASSUME_NONNULL_BEGIN
@protocol TCSDownloadResourcesProtocol <NSObject>

-(void)resourceDownloadCompleteWithError:(BOOL)hadError;


@end

@interface TCSDownloadResourcesViewController : NSViewController
@property (assign) id <TCSDownloadResourcesProtocol> delegate;

@end

NS_ASSUME_NONNULL_END
