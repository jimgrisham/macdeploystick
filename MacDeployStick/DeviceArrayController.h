//
//  DeviceArrayController.h
//  MDS
//
//  Created by Timothy Perfitt on 8/31/19.
//  Copyright © 2019 Twocanoes Software. All rights reserved.
//

#import <Cocoa/Cocoa.h>

NS_ASSUME_NONNULL_BEGIN

@interface DeviceArrayController : NSArrayController

@end

NS_ASSUME_NONNULL_END
