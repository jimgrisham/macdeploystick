//
//  MDMUtilities.h
//  MDS
//
//  Created by Timothy Perfitt on 9/9/19.
//  Copyright © 2019 Twocanoes Software. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface MDMUtilities : NSObject
+ (instancetype)sharedUtilities ;
-(void)removeMDMRepositoryApps:(NSArray *)namesOfApps completeBlock:(void (^)(NSError *err))completeBlock;

-(void)setupApps:(NSArray *)inArray statusBlock:(void (^)(NSString *status, float percentComplete))statusBlock completeBlock:(void (^)(NSArray *manifestArray))completeBlock;
@property (assign) BOOL shouldCancel;
@end

NS_ASSUME_NONNULL_END
