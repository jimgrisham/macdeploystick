//
//  TCSMDMService.m
//  MDS
//
//  Created by Timothy Perfitt on 9/3/19.
//  Copyright © 2019 Twocanoes Software. All rights reserved.
//

#import "TCSMDMService.h"
#import "TCTaskWrapperWithBlocks.h"
#import "TCSUtility.h"
#import"TCSConstants.h"
#import "MDSPrivHelperToolController.h"
#import "TCSMDMWebController.h"
@interface TCSMDMService()
@property (strong) TCTaskWrapperWithBlocks *task;
@property (strong) id notObserver;
@property (assign) BOOL isRunning;
@end

@implementation TCSMDMService

+ (TCSMDMService *)sharedManager {
    static TCSMDMService *sharedMyManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedMyManager = [[[self class] alloc] init];
        sharedMyManager.notObserver=[[NSNotificationCenter defaultCenter] addObserverForName:SSLINFOCHANGED object:nil queue:nil usingBlock:^(NSNotification * _Nonnull note) {

        }];

    });
    [sharedMyManager updateRunning];
    return sharedMyManager;
}
-(void)dealloc{

    [[NSNotificationCenter defaultCenter] removeObserver:self.notObserver];
}
-(void)updateRunning{

    self.isRunning=[TCSUtility isProcessRunning:TCSPROCESSNAMEMICROMDM];

}
-(void)restartMDMWithCallback:(void (^)(NSError * _Nullable err))callback{
    [[MDSPrivHelperToolController sharedHelper] restartMicroMDMWithCallback:callback];


}
-(void)startMDMWithCallback:(void (^)(NSError * _Nullable err))callback{


    if ([USERDEFAULTS objectForKey:INDENTITYKEYPATH]==nil || [USERDEFAULTS objectForKey:CERTIFICATEPATH]==nil){


//                NSAlert *alert=[[NSAlert alloc] init];
//                alert.messageText=@"SSL Error";
//                alert.informativeText=@"";
//
//                [alert addButtonWithTitle:@"OK"];
//                [alert runModal];
//                return;

        NSError *err=[NSError errorWithDomain:@"TCS" code:-1 userInfo:@{NSLocalizedDescriptionKey:@"An SSL certificate is required for the MDM service. Please configure an SSL certificate in Preferences->Security"}];

        callback(err);
        return;

            }

    [[MDSPrivHelperToolController sharedHelper] startMicroMDMWithSettings:[self mdmSettings] callback:callback];

    dispatch_async(dispatch_get_main_queue(), ^{

        [self performSelector:@selector(updateSidebar:) withObject:self afterDelay:.25];
});

}
-(void)updateSidebar:(id)sender{
    [[NSNotificationCenter defaultCenter] postNotificationName:TCSNOTIFICATIONSIDEBARRELOAD object:self];

}
-(void)stopMDMWithCallback:(void (^)(NSError * _Nullable err))callback{
    [[MDSPrivHelperToolController sharedHelper] stopMicroMDMWithCallback:callback];
    dispatch_async(dispatch_get_main_queue(), ^{

    [[NSNotificationCenter defaultCenter] postNotificationName:TCSNOTIFICATIONSIDEBARRELOAD object:self];
    });

}
-(NSString *)mdmSettings{

    NSMutableString *settingsString=[NSMutableString string];

    NSString *repoResolvedPath=[[[NSUserDefaults standardUserDefaults] objectForKey:MDMSERVERREPOPATH] stringByExpandingTildeInPath];

    NSString *serverURL=[NSString stringWithFormat:@"https://%@:%@",[USERDEFAULTS objectForKey:SERVERHOSTNAME],[USERDEFAULTS objectForKey:MDMSERVERPORT]];
    NSString *webhookURL=[NSString stringWithFormat:@"http://%@:%@",@"127.0.0.1",@"9999"];


    NSString *mdmFileRepo=[[[NSUserDefaults standardUserDefaults] objectForKey:MDMSERVERFILEREPOPATH] stringByExpandingTildeInPath];

    if (![[NSFileManager defaultManager] fileExistsAtPath:mdmFileRepo]){
        NSError *err;
        if([[NSFileManager defaultManager] createDirectoryAtPath:mdmFileRepo withIntermediateDirectories:YES attributes:nil error:&err]==NO){
            NSLog(@"%@",err.localizedDescription);

        }
    }

    [settingsString appendFormat:@"MICROMDM_CONFIG_PATH=\"%@\"\n",repoResolvedPath];
    [settingsString appendFormat:@"MICROMDM_HTTP_ADDR=\"%@\"\n",[@":" stringByAppendingString:[USERDEFAULTS objectForKey:MDMSERVERPORT]]];
    [settingsString appendFormat:@"MICROMDM_API_KEY=\"%@\"\n",[USERDEFAULTS objectForKey:MDMSERVERRAPIKEY]];
    [settingsString appendFormat:@"MICROMDM_SERVER_URL=\"%@\"\n",serverURL];
    if ([USERDEFAULTS objectForKey:CERTIFICATEPATH]) [settingsString appendFormat:@"MICROMDM_TLS_CERT=\"%@\"\n",[USERDEFAULTS objectForKey:CERTIFICATEPATH]];
    if ([USERDEFAULTS objectForKey:INDENTITYKEYPATH]) [settingsString appendFormat:@"MICROMDM_TLS_KEY=\"%@\"\n",[USERDEFAULTS objectForKey:INDENTITYKEYPATH]];
    [settingsString appendFormat:@"MICROMDM_WEBHOOK_URL=\"%@\"\n",webhookURL];
    [settingsString appendFormat:@"MICROMDM_FILE_REPO=\"%@\"\n",mdmFileRepo];


    NSUserDefaults *ud=[NSUserDefaults standardUserDefaults];
    if ([ud boolForKey:TCSMDMHTTPDEBUG]==YES){
        [settingsString appendString:@"MICROMDM_HTTP_DEBUG=true\n"];

    }
    return [NSString stringWithString:settingsString];

}
-(BOOL)checkExistanceOfMDMFiles{

    if (![USERDEFAULTS objectForKey:CERTIFICATEPATH] ||
        ![USERDEFAULTS objectForKey:INDENTITYKEYPATH]||
        ![USERDEFAULTS objectForKey:MDMSERVERRAPIKEY]||
        ![USERDEFAULTS objectForKey:SERVERHOSTNAME]||
        ![USERDEFAULTS objectForKey:MDMSERVERPORT]||
        ![USERDEFAULTS objectForKey:MDMSERVERREPOPATH]){

        return NO;
    }

    return YES;
}
@end
