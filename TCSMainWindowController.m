//
//  TCSMainWindowController.m
//  MDS
//
//  Created by Timothy Perfitt on 3/30/20.
//  Copyright © 2020 Twocanoes Software. All rights reserved.
//

#import "TCSMainWindowController.h"

@interface TCSMainWindowController () <NSWindowDelegate>

@end

@implementation TCSMainWindowController

- (void)windowDidLoad {
    [super windowDidLoad];
    
    // Implement this method to handle any initialization after your window controller's window has been loaded from its nib file.
}



@end
