//
//  MDSPrivHelperToolController.m
//  MDS
//
//  Created by Timothy Perfitt on 10/6/19.
//  Copyright © 2019 Twocanoes Software. All rights reserved.
//

#import "MDSPrivHelperToolController.h"
#import <Security/Security.h>
#import <ServiceManagement/ServiceManagement.h>
#import "mdshelperProtocol.h"
@interface MDSPrivHelperToolController(){
       AuthorizationRef        _authRef;
}
@property (strong) NSXPCConnection *privHelperToolConnection;
@end
@implementation MDSPrivHelperToolController
+ (instancetype)sharedHelper {
    static MDSPrivHelperToolController *sharedMyManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedMyManager = [[[self class] alloc] initPrivate];
        if (![[self class] isToolInstalled]) {
            [sharedMyManager installTool];
        }

    });
    
    return sharedMyManager;
}
- (instancetype)initPrivate {
    return [super init];
}

- (instancetype)init {
    [[NSException exceptionWithName:@"Unimplemented" reason:@"Shared singleton must be used" userInfo:nil] raise];
    return nil;
}
+ (BOOL)isToolInstalled {
    NSFileManager *filemanager = [NSFileManager defaultManager];
    BOOL toolInstalled = [filemanager fileExistsAtPath:@"/Library/PrivilegedHelperTools/com.twocanoes.mdshelpertool"];
    BOOL plistInstalled = [filemanager fileExistsAtPath:@"/Library/LaunchDaemons/com.twocanoes.mdshelpertool.plist"];
    return toolInstalled && plistInstalled;
}
+ (BOOL)installTool{

    MDSPrivHelperToolController *controller=[[MDSPrivHelperToolController alloc] initPrivate];

    NSError *err;
    if([controller installToolError:&err]==NO){

        NSLog(@"%@",err.localizedDescription);
        return NO;
    }
    return YES;

}
- (void)connectToPrivHelperTool
{


    if (self.privHelperToolConnection == nil) {
        assert([NSThread isMainThread]);
        self.privHelperToolConnection = [[NSXPCConnection alloc] initWithMachServiceName:@"com.twocanoes.mdshelpertool" options:NSXPCConnectionPrivileged];
        self.privHelperToolConnection.remoteObjectInterface = [NSXPCInterface interfaceWithProtocol:@protocol(mdshelperProtocol)];
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Warc-retain-cycles"
        // We can ignore the retain cycle warning because a) the retain taken by the
        // invalidation handler block is released by us setting it to nil when the block
        // actually runs, and b) the retain taken by the block passed to -addOperationWithBlock:
        // will be released when that operation completes and the operation itself is deallocated
        // (notably self does not have a reference to the NSBlockOperation).
        self.privHelperToolConnection.invalidationHandler = ^{
            // If the connection gets invalidated then, on the main thread, nil out our
            // reference to it.  This ensures that we attempt to rebuild it the next time around.
            self.privHelperToolConnection.invalidationHandler = nil;
            [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                self.privHelperToolConnection = nil;
            }];
        };
#pragma clang diagnostic pop
        [self.privHelperToolConnection resume];

    }
}
- (id <mdshelperProtocol>)privHelperTool {
    [self connectToPrivHelperTool];
    id<mdshelperProtocol> proxyObject = [self.privHelperToolConnection remoteObjectProxyWithErrorHandler:^(NSError *error) {

        NSLog(@"%@",error.localizedDescription);
    }];

    return proxyObject;
}
- (void)installTool {

    NSError *error = nil;
    BOOL success = [self installToolError:&error];
    if (!success) {
        NSDictionary *info = @{NSUnderlyingErrorKey: error};
        [[NSNotificationCenter defaultCenter] postNotificationName:@"HelperNotification" object:self userInfo:info];
        return;
    }
    dispatch_async(dispatch_get_main_queue(), ^{
        [self connectToPrivHelperTool];
    });

}
- (BOOL)installToolError:(NSError **)error {
    NSError *blessError;
    BOOL success = [self blessHelperWithLabel:@"com.twocanoes.mdshelpertool" error:&blessError];
    if (!success && error != NULL) {
        *error = blessError;
    }
    return success;
}
- (void)installToolIfNecessary {
    NSInteger expectedVersion = ((NSNumber *)[[NSBundle mainBundle] objectForInfoDictionaryKey:@"MDSHelperVersion"]).integerValue;
    [[self privHelperTool] getVersionCallback:^(NSInteger version) {
        if (version != expectedVersion) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [self installTool];
                [self connectToPrivHelperTool];
            });
        }
    }];
}
- (void)getVersionCallback:(void (^)(NSInteger))callback{
    [self.privHelperTool getVersionCallback:^(NSInteger version) {
        callback(version);
    }];
}

-(NSData *)token{

    OSStatus myStatus;
    AuthorizationExternalForm myExternalAuthorizationRef;
    myStatus = AuthorizationMakeExternalForm (self.authorization.authorizationRef,
                                              &myExternalAuthorizationRef);
    NSData *authData=[NSData dataWithBytes:myExternalAuthorizationRef.bytes length:kAuthorizationExternalFormLength];

    return authData;
}
- (void)installInstallMacOSWithProductID:(NSString *)productID catalog:(NSString *)catalog workingPath:(NSString *)workingPath withCallback:(void (^)(BOOL success))callback{
    NSData *token=[self token];

    [self.privHelperTool installInstallMacOSWithProductID:productID catalog:catalog workingPath:workingPath auth:(NSData *)token withCallback:^(BOOL success){
        callback(success);
    }];
}
-(void)stopRunningProcessesWithCallback:(void (^)(BOOL success))callback{

    NSData *token=[self token];
    [self.privHelperTool stopRunningProcessesWithAuth:token callback:^(BOOL success) {
        callback(success);
    }];
}

-(void)updateWebserverWithConfigurations:(NSDictionary *)configurations withCallback:(void (^)(BOOL success))callback{
    NSData *token=[self token];
    [self.privHelperTool updateWebserverWithAuth:token configurations:configurations withCallback:^(BOOL success) {
        callback(success);
    }];

}
-(void)stopWebserverWithCallback:(void (^)(BOOL success))callback{
NSData *token=[self token];

    [self.privHelperTool stopWebserverWithAuth:(NSData *)token callback:^(BOOL success) {
        callback(success);
    }];

}
-(void)startWebserverWithCallback:(void (^)(BOOL success))callback{
    NSData *token=[self token];

    [self.privHelperTool startWebserverWithAuth:(NSData *)token callback:^(BOOL success) {
        callback(success);
    }];

}
-(void)restartWebserverWithCallback:(void (^)(BOOL success))callback{
    NSData *token=[self token];

    [self.privHelperTool restartWebserverWithAuth:(NSData *)token callback:^(BOOL success) {
        callback(success);
    }];

}

-(void)startMicroMDMWithSettings:(NSString *)settings callback:(void (^)(NSError * _Nullable))callback{
    NSData *token=[self token];

    [self.privHelperTool startMicroMDMWithAuth:(NSData *)token settings:settings callback:callback];
}
- (void)stopMicroMDMWithCallback:(void (^)(NSError * _Nullable))callback{
    NSData *token=[self token];

    [self.privHelperTool stopMicroMDMWithAuth:(NSData *)token callback:callback];
}
- (void)restartMicroMDMWithCallback:(void (^)(NSError * _Nullable))callback{
    NSData *token=[self token];

    [self.privHelperTool restartMicroMDMWithAuth:(NSData *)token callback:callback];
}

-(void)createMunkiRepoAtPath:(NSString *)munkiRepoPath callback:(void (^)(NSError * _Nullable))callback{
    NSData *token=[self token];

    [self.privHelperTool createMunkiRepoWithAuth:(NSData *)token path:munkiRepoPath callback:callback];

}
-(void)trustCertificateAtPath:(NSString *)certPath withCallback:(void (^)(NSError *err))callback{
    [self.privHelperTool trustCertificateAtPath:certPath withCallback:callback];
}
-(void)createMacOSInstallVolume:(NSString *)volumePath withInstaller:(NSString *)installerPath callback:(void (^)(BOOL isDone, NSString *statusMsg,NSError * _Nullable))callback{
    NSData *token=[self token];

    [self.privHelperTool createMacOSInstallWithAuth:(NSData *)token volume:volumePath withInstaller:installerPath callback:callback];
}
-(void)setupMunkiReportWithSource:(NSString *)source destination:(NSString *)destination withCallback:(void (^)(NSError *err))callback{
    NSData *token=[self token];

    [self.privHelperTool setupMunkiReportWithAuth:(NSData *)token source:source destination:destination withCallback:^(NSError *err) {
        callback(err);
    }];

}

-(void)addMunkiReportUserFile:(NSString *)path contents:(NSString *)contents withCallback:(void (^)(NSError *err))callback{
    NSData *token=[self token];

    [self.privHelperTool addMunkiReportWithAuth:(NSData *)token userFile:path contents:contents withCallback:callback];



}
//    [[MDSPrivHelperToolController sharedHelper] addMunkiReportUserFile:path contents:userFolder];




- (BOOL)blessHelperWithLabel:(NSString *)label error:(NSError **)errorPtr;
{
    OSStatus authStatus = AuthorizationCreate(NULL, kAuthorizationEmptyEnvironment, kAuthorizationFlagDefaults, &self->_authRef);
    if (authStatus != errAuthorizationSuccess) {
        /* AuthorizationCreate really shouldn't fail. */
        assert(NO);
        self->_authRef = NULL;
        return NO;
    }

    BOOL result = NO;
    NSError * error = nil;

    AuthorizationItem authItem        = { kSMRightBlessPrivilegedHelper, 0, NULL, 0 };
    AuthorizationRights authRights    = { 1, &authItem };
    AuthorizationFlags flags        =    kAuthorizationFlagDefaults                |
                                    kAuthorizationFlagInteractionAllowed    |
                                    kAuthorizationFlagPreAuthorize            |
                                    kAuthorizationFlagExtendRights;


    /* Obtain the right to install our privileged helper tool (kSMRightBlessPrivilegedHelper). */
    OSStatus status = AuthorizationCopyRights(self->_authRef, &authRights, kAuthorizationEmptyEnvironment, flags, NULL);
    if (status != errAuthorizationSuccess) {
        error = [NSError errorWithDomain:NSOSStatusErrorDomain code:status userInfo:nil];
    } else {
        CFErrorRef  cfError;

        /* This does all the work of verifying the helper tool against the application
         * and vice-versa. Once verification has passed, the embedded launchd.plist
         * is extracted and placed in /Library/LaunchDaemons and then loaded. The
         * executable is placed in /Library/PrivilegedHelperTools.
         */
        result = (BOOL) SMJobBless(kSMDomainSystemLaunchd, (__bridge CFStringRef)label, self->_authRef, &cfError);
        if (!result) {
            error = (__bridge NSError *)(cfError);
        }
    }
    if ( ! result && (errorPtr != NULL) ) {
        assert(error != nil);
        *errorPtr = error;
    }

    
    return result;
}

-(BOOL)areRightsValid:(id)sender{
   int myFlags = kAuthorizationFlagDefaults |
    kAuthorizationFlagExtendRights ;


    //kAuthorizationRightExecute
    AuthorizationItem right = {TCSAUTHRIGHT, 0, NULL, 0};
    AuthorizationRights rights = {1, &right};

    OSStatus myStatus = AuthorizationCopyRights (self.authorization.authorizationRef, &rights,kAuthorizationEmptyEnvironment, myFlags, NULL);

    if (myStatus==0) {
        return YES;
    }
    return NO;
}

-(void)removeUsers:(NSArray *)users fromFolder:(NSString *)path callback:(void (^)(NSError *err))callback{

    NSData *token=[self token];
    [self.privHelperTool removeUsers:users fromFolder:path withAuth:(NSData *)token withCallback:callback];

}
@end
