//
//  TCSRunCommandPrefViewController.h
//  MDS
//
//  Created by Timothy Perfitt on 2/26/20.
//  Copyright © 2020 Twocanoes Software. All rights reserved.
//

#import <Cocoa/Cocoa.h>

NS_ASSUME_NONNULL_BEGIN

@interface TCSRunCommandPrefViewController : NSViewController

@end

NS_ASSUME_NONNULL_END
