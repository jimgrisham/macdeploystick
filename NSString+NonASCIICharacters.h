//
//  NSString+NonASCIICharacters.h
//  MDS
//
//  Created by Timothy Perfitt on 4/27/20.
//  Copyright © 2020 Twocanoes Software. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface NSString (NonASCIICharacters)


-(BOOL)hasNonASCIICharacters;
@end

NS_ASSUME_NONNULL_END
