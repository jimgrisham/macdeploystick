/*
 Copyright (c) 2018, Amplified IT
 See the full description at http://labs.amplifiedit.com/centipede

 Support forums are available at https://plus.google.com/communities/100599537603662785064

 Published under an MIT License https://opensource.org/licenses/MIT

 */
#include <Keyboard.h>
#include <avr/wdt.h>
#include <avr/wdt.h>
#include <EEPROM.h>


#define CURRENTVERSION 8
#define SHOWFREEMEM 1
#ifdef __arm__
// should use uinstd.h to define sbrk but Due causes a conflict
extern "C" char* sbrk(int incr);
#else  // __ARM__
extern char *__brkval;
#endif  // __arm__

#ifdef SHOWFREEMEM
int freeMemory() {
  char top;
#ifdef __arm__
  return &top - reinterpret_cast<char*>(sbrk(0));
#elif defined(CORE_TEENSY) || (ARDUINO > 103 && ARDUINO != 151)
  return &top - __brkval;
#else  // __arm__
  return __brkval ? &top - __brkval : &top - __malloc_heap_start;
#endif  // __arm__
}
#endif

enum mode_types {CHROMESETUP, CLI} current_mode;
#define BLOCK_SIZE 256
#define WIFI_SECURITY_NONE 0
#define WIFI_SECURITY_WEP 1
#define WIFI_SECURITY_WPA 2

#define EAP_TLS 0
#define EAP_TTLS 1
#define EAP_LEAP 2
#define EAP_PEAP 3

#define PHASE2_AUTO 0
#define PHASE2_EAP_MD5 1
#define PHASE2_MSCHAP 2
#define PHASE2_MSCHAPV2 3
#define PHASE2_PAP 4
#define PHASE2_CHAP 5
#define PHASE2_GTC 6

#define SERVER_CA_CHECK_DEFAULT 0
#define SERVER_CA_CHECK_NO 1


#define DEVICETYPE_LESS_THAN_SIXTYNINE 0
#define DEVICETYPE_SIXTYNINE 1
#define DEVICETYPE_GREATERTHAN69 2

struct settings_t
{
    int version;
    int device_type;
    char wifi_name[100];
    char wifi_password[100];
    int wifi_security;
    char username[100];
    char password[100];
    bool autorun;
    bool advanced_network_setup;
    int eap_method;
    int phase2_authentication;
    bool server_ca_check;
    char subject_match[100];
    char identity[100];
    char anonymous_identity[100];
} __attribute__((packed)) settings;

void reboot() {
    wdt_disable();
    wdt_enable(WDTO_15MS);
    while (1) {}
}
void setdefaults() {
    settings.version = CURRENTVERSION;
    settings.device_type=DEVICETYPE_GREATERTHAN69;
    strcpy(settings.wifi_name, "");
    strcpy(settings.wifi_password, "");
    settings.wifi_security=WIFI_SECURITY_NONE;
    strcpy(settings.username, "");
    strcpy(settings.password, "");
    settings.autorun = true;
    settings.advanced_network_setup=false;
    settings.eap_method=EAP_PEAP;
    settings.phase2_authentication=PHASE2_AUTO;
    settings.server_ca_check=SERVER_CA_CHECK_DEFAULT;
    strcpy(settings.subject_match, "");
    strcpy(settings.identity, "");
    strcpy(settings.anonymous_identity, "");
    

    EEPROM_writeAnything(0, settings);
    EEPROM_readAnything(0, settings);

}

/* Modify the following definitions to fit your wireless and enrollment credentials. */
/*
 #define device_version 66 // Change to the ChromeOS version you expect to use with Centipede; Changes have been reported in the following ranges 58-68, 69, 70
 */

// Use these options to deter mine if you want to disable analytics, skip asset ID, or if you need to slow down the Centipede

#define sendUsageToGoogle 1 //[0,1] Set to 0 if you want to un-check the box to send usage analytics to Google
#define skipAssetIdScreen 1 //[0,1] Set to 0 if you want Centipede to stop at the asset ID and location screen
#define languageIsSelectedOnBoot 0 //[0,1] Set to 1 if the language box is selected when you first power on the device

/* These are advanced options. The defaults should be fine, but feel free to tweak values below. */

#define setWiFi true //[true,false] Set to false for devices that already have WiFi setup and have accepted Terms of Service (ToS)

// Use this area for advanced network setup options
//#define advancedNetworkSetup false //[true,false] Set to true for EAP configuration, and fill out the definitions below
//#define eapMethod "LEAP" // Valid options are "LEAP" "PEAP" "EAP-TLS" or "EAP-TTLS" - Note that they require the quotes to work properly
//#define phaseTwoAuthentication 2 //[0,1,2,3,4,5,6] Set to 0 for automatic, 1 for EAP-MD5, 2 for MSCHAP(v2 pre v69; v1 V69+, 3 for MSCHAPv2, 4 for PAP, 5 for CHAP, 6 for GTC; v69+)
//#define serverCaCertificateCheck 0 //[0,1] 0 is default, 1 is "Do not check"
//#define subjectMatch "" // Fill in subject match here if needed for advanced wireless
//#define identity "identity" // Fill in identity here if needed for advanced wireless
//#define anonymousIdentity "" // Fill in anonymous identity here for advanced wireless
#define saveIdentityAndPassword 0 //[0,1] Set to 1 to save identity and password. NOT RECOMMENDED
#define sso 0 //[0,1] Set to 1 if using Single Sign On - NOTE: May need additional configuration in Advanced Network Setup around line 182.

// Use this section for additional non-traditional methods
#define longer_enrollment_time 5 // Set to additional seconds to wait for Device Configuration and Enrollment
#define update_via_guest 0 //[0,1] Set to 1 to go into Guest Mode and navigate to chrome://chrome and Check for Updates
#define powerwash 0 //[0,1] Powerwash the device BEFORE enrollment is completed - NOTE: Will not work on ENROLLED devices. Used for Setting up Centipede.
#define sign_in 0 //[0,1] Set to 1 to sign-in to the device after enrollment - NOTE: Will not sign-in if update_via_guest or powerwash is set to true;
#define remove_enrollment_wifi 0 //[0,1] Set to 1 to remove the enrollment wifi network. *sign_in also must be true* - NOTE: Only set to true when Chrome Device Network has been pulled down
#define enroll_device_cert 0 //[0,1] Set to 1 if enrolling device wide certificate *sign_in also must be true* - NOTE: Works best if user _*only*_ has Certificate Enrollment extension force installed

#define slowMode 0 // [0,1] Set to 1 if Centipede appears to be moving too quickly at any screen. This will slow down the entire process
#define update_wait_time 90 // Set to seconds to wait for Update with update_via_guest before exiting guest mode.  Update will continue while device is online.

/* Do not modify anything below this line unless you're confident that you understand how to program Arduino or C */

// Version Defination
/*
 #define VERSION_69 (device_version >= 69)
 #define VERSION_70 (device_version >= 70)
 */

// Special characters definition
#define KEY_LEFT_CTRL   0x80
#define KEY_LEFT_SHIFT  0x81
#define KEY_LEFT_ALT    0x82
#define KEY_RIGHT_CTRL  0x84
#define KEY_RIGHT_SHIFT 0x85
#define KEY_RIGHT_ALT   0x86
#define KEY_UP_ARROW    0xDA
#define KEY_DOWN_ARROW  0xD9
#define KEY_LEFT_ARROW  0xD8
#define KEY_RIGHT_ARROW 0xD7
#define KEY_BACKSPACE   0xB2
#define KEY_TAB         0xB3
#define KEY_ENTER       0xB0
#define KEY_ESC         0xB1
#define KEY_CAPS_LOCK   0xC1

int buttonPin = 2;  // Set a button to any pin
int RXLED = 17;
static uint8_t __clock_prescaler = (CLKPR & (_BV(CLKPS0) | _BV(CLKPS1) | _BV(CLKPS2) | _BV(CLKPS3)));

void setup()
{
    setPrescaler(); // Set prescaler to highest clock speed
    Keyboard.begin(); // Start they keyboard emulator
    pinMode(buttonPin, INPUT);  // Set up the debugging pin. If you want to debug the code, use a length of wire to connect pins 2 and GND on the board
    digitalWrite(buttonPin, HIGH);

    pinMode(RXLED, OUTPUT); // Configure the on-board LED
    digitalWrite(RXLED, LOW);
//    TXLED1;
    if (digitalRead(buttonPin) == 0) {
        showSuccess();
    }
    wait(5); // Wait for all services to finish loading
}

void loop() { // Main Function - workflow is called within loop();
digitalWrite(13, HIGH);
  Serial.begin(9600);
  delay(500);
digitalWrite(13, LOW);
digitalWrite(13, HIGH);
  EEPROM_readAnything(0, settings);
  if (settings.version != CURRENTVERSION) {
    setdefaults();
  }
  Serial.println(F("Copyright 2018 Twocanoes Software, Inc."));
  Serial.println(F("Press <return> to enter configuration mode."));
  int i;
  current_mode = CHROMESETUP;
  if (settings.autorun == true) {
    for (i = 0; i < 20; i++) {
      if (Serial.available()) {
        while (Serial.available()) {
          Serial.read();
        }
        current_mode = CLI;
         break;
      }
      else {
        delay(500);
      }
    }
    
  }
  else {
    current_mode = CLI;;
  }
  digitalWrite(13, LOW);
  if (current_mode != CLI) {
    Serial.end();
  }
  while (1) {
    switch (current_mode) {

      case CLI:
        enter_cli();
        break;
      case CHROMESETUP:
        chrome_setup();
        break;
    }
  }
   
}

void chrome_setup(){
   if (digitalRead(buttonPin) == 1 ) { // Check for debugging. If not debugging, run the program
        showVersion();
        if (!update_via_guest){ // Guestmode not available for devices tagged for enrollment
            enterEnrollment();
        }
        if (setWiFi){
            wifiConfig(); // Enter the wifi configuration method (written down below)
            ToS(); // Accept Terms of Service
        }
        wait(15 + longer_enrollment_time); // Wait device to download configuration
        while (digitalRead(buttonPin) != 1) {
            bootLoop();
        }
//        TXLED0;
        if (update_via_guest){
            updateViaGuest(); // Enrollment keypress at the end (around line 447)to continue the enrollment process
        }
        enterCredentials(); // Max progress with powerwash set to true - Will Powerwash after typing the password but before submitting
        wait(50 + longer_enrollment_time); // wait for Enrollment to complete

        if (sign_in && skipAssetIdScreen){ // Do not sign-in if "skipAssetIdScreen" is false
            Keyboard.write(KEY_ENTER);
            wait(10);
            enterCredentials();
            wait(90); // Wait for profile to load
            if (enroll_device_cert){
                certificateEnrollment(); // Enroll Device wide Certificate
            }
            if (remove_enrollment_wifi){
                removeEnrollmentWifi(); // Remove non-managed Enrollment WiFi
            }
        }
        if (skipAssetIdScreen) {
            shutDown();
        }
        showSuccess();
    }
    bootLoop();
}

void bootLoop() {
    //rr      digitalWrite(RXLED, LOW);   // set the LED on
//    TXLED0; //TX LED is not tied to a normally controlled pin
    delay(200);              // wait for a second
//    TXLED1;
    delay(200);
//    TXLED0; //TX LED is not tied to a normally controlled pin
    delay(200);              // wait for a second
//    TXLED1;
    delay(800);
}

void showSuccess() {
    digitalWrite(RXLED, HIGH);  // set the LED off
    while (true) {
        bootLoop();
    }
}

void repeatKey(byte key, int num) {
    for (int i = 0; i < num; i++) {
        Keyboard.write(key);
        wait(1);
    }
}

void blink() {
    digitalWrite(RXLED, LOW);
    //  TXLED1;
    delay(250);
    digitalWrite(RXLED, HIGH);
    //  TXLED0;
    delay(250);
}

void wait(int cycles) {
    for (int i = 0; i < cycles; i++) {
        blink();
        if (slowMode) {
            delay(250);
        }
    }
}

void enterCredentials() {
    wait(5);
    Keyboard.print(settings.username);
    wait(3);
    Keyboard.write(KEY_ENTER);
    wait(8);
    if (sso){
        Keyboard.write(KEY_TAB);

        Keyboard.print(settings.username);
        Keyboard.write(KEY_TAB);
        wait(1);
    }
    Keyboard.print(settings.password);
    if (powerwash){
        wait(5);
        Powerwash();
    }
    wait(3);
    Keyboard.write(KEY_ENTER);
    wait(3);
}

void enterEnrollment() {
    Keyboard.press(KEY_LEFT_CTRL);
    Keyboard.press(KEY_LEFT_ALT);
    Keyboard.write('e');
    Keyboard.release(KEY_LEFT_ALT);
    Keyboard.release(KEY_LEFT_CTRL);
    wait(1);
}
void wifiConfig() {
    // Access the Network option from the system tray (Status Area).
    Keyboard.press(KEY_LEFT_SHIFT);
    Keyboard.press(KEY_LEFT_ALT);
    Keyboard.write('s');
    Keyboard.release(KEY_LEFT_ALT);
    Keyboard.release(KEY_LEFT_SHIFT);
    wait(2);
    //to select the Network
    if (settings.device_type==DEVICETYPE_GREATERTHAN69) {
      repeatKey(KEY_TAB, 4 ); 
    } // 3 for pre v70, 4 for ver 70 (black menu)
    else {
         repeatKey(KEY_TAB, 3);
    }
    wait(1);
    Keyboard.write(KEY_ENTER);
    wait(1);
    //to select the 'add Wifi' icon
    repeatKey(KEY_TAB, 3);
    Keyboard.write(KEY_ENTER);
    wait(1);
    // SSID
    Keyboard.print(settings.wifi_name);
    wait(1);
    // TAB
    Keyboard.write(KEY_TAB);
    wait(1);
    if (settings.wifi_security == 0) {
        repeatKey(KEY_TAB, 2);
    } 
    else {
        if (settings.advanced_network_setup==true) {
            repeatKey(KEY_DOWN_ARROW, 3); //[1]WEP, [2]PSK (WPA or RSN), [3]EAP;

            setupAdvancedNetworkConfig();
        }
        else{
            repeatKey(KEY_DOWN_ARROW, settings.wifi_security); //[1]WEP, [2]PSK (WPA or RSN), [3]EAP;

            // TAB
            Keyboard.write(KEY_TAB); //[1,2]password, [3]EAP method;
            wait(1);
            // type wifi password
            Keyboard.print(settings.wifi_password);
            repeatKey(KEY_TAB, 3);
        }
    }
    wait(1);
    // Enter
    Keyboard.write(KEY_ENTER); // Connect
    // Delay 15 seconds to connect
    wait(15);
    repeatKey(KEY_TAB, 3 - languageIsSelectedOnBoot);
    wait(2);
    Keyboard.write(KEY_ENTER); // Click "Let's Go"
    wait(1);
    switch (settings.device_type) {
        case DEVICETYPE_LESS_THAN_SIXTYNINE:
            repeatKey(KEY_TAB, 2);
            break;
        case DEVICETYPE_SIXTYNINE:
            repeatKey(KEY_TAB, 3);
            break;
        case DEVICETYPE_GREATERTHAN69:
            repeatKey(KEY_TAB, 3);
            break;
        default:
            break;
    }
    // 3 for pre v70, 4 for ver 70 (black menu)
    // After connecting, enter the enrollment key command to skip checking for update at this point in the process
    if (!update_via_guest){
        enterEnrollment();
    }
    wait(1);
    Keyboard.write(KEY_ENTER); // Click "Next"
    
}
void ToS() {
    // Terms of Service screen
    wait(1);
    repeatKey(KEY_TAB, 2);
    if (!sendUsageToGoogle) {
        Keyboard.write(KEY_ENTER);
        wait(1);
    }
    repeatKey(KEY_TAB, 3);
    wait(1);
    Keyboard.write(KEY_ENTER);
}




void shutDown() { // Shutdown if not signed in, Sign out if signed in
    // Access the Network option from the system tray (Status Area).
    Keyboard.press(KEY_LEFT_SHIFT);
    Keyboard.press(KEY_LEFT_ALT);
    Keyboard.write('s');
    Keyboard.release(KEY_LEFT_ALT);
    Keyboard.release(KEY_LEFT_SHIFT);
    wait(2);

     switch (settings.device_type) {
            case DEVICETYPE_LESS_THAN_SIXTYNINE:
                 Keyboard.press(KEY_LEFT_SHIFT);
                 repeatKey(KEY_TAB, 2 + sign_in);
                 Keyboard.release(KEY_LEFT_SHIFT);
                break;
            case DEVICETYPE_SIXTYNINE:
                Keyboard.press(KEY_LEFT_SHIFT);
                repeatKey(KEY_TAB, 2 + sign_in);
                Keyboard.release(KEY_LEFT_SHIFT);
                break;
            case DEVICETYPE_GREATERTHAN69:
                repeatKey(KEY_TAB,1 + sign_in);
                break;

        }
        
   
    repeatKey(KEY_ENTER, 1);
}

void setupAdvancedNetworkConfig() {
    //Starting at Security box
    if (settings.device_type == DEVICETYPE_SIXTYNINE){
        repeatKey(KEY_DOWN_ARROW, 3); // Select Security "EAP" (v69);
        Keyboard.write(KEY_TAB);
    }else{
        //ARROW_DOWN x3 WEP, PSK, EAP
//        repeatKey(KEY_TAB, 2);
//        Keyboard.write(KEY_ENTER);
//        wait(1);
        //SSID (again);
//        Keyboard.print(settings.wifi_name);
        Keyboard.write(KEY_TAB);
        //@EAP Method
    }

    if (settings.eap_method==EAP_LEAP) {
        // Default is LEAP v69+
        switch (settings.device_type) {
            case DEVICETYPE_LESS_THAN_SIXTYNINE:
                repeatKey(KEY_DOWN_ARROW, 1);
                break;
            case DEVICETYPE_SIXTYNINE:
                repeatKey(KEY_DOWN_ARROW, 0);
                break;
            case DEVICETYPE_GREATERTHAN69:
                repeatKey(KEY_DOWN_ARROW, 1);
                break;

        }
        Keyboard.write(KEY_TAB);
        // Identity
        Keyboard.print(settings.identity);
        Keyboard.write(KEY_TAB);
        wait(1);
        Keyboard.print(settings.wifi_password);
        repeatKey(KEY_TAB, 2);
        wait(1);
        Keyboard.write(KEY_ENTER); // Save Identity and Password (true);
        repeatKey(KEY_TAB, 2);
        Keyboard.write(KEY_ENTER); // Connect;
    } else     if (settings.eap_method==EAP_PEAP) {

        // Select PEAP method
        switch (settings.device_type) {
            case DEVICETYPE_LESS_THAN_SIXTYNINE:
                repeatKey(KEY_DOWN_ARROW, 2);
                break;
            case DEVICETYPE_SIXTYNINE:
                repeatKey(KEY_DOWN_ARROW, 1);
                break;
            case DEVICETYPE_GREATERTHAN69:
                repeatKey(KEY_DOWN_ARROW, 1);
                break;

        }
        //        repeatKey(KEY_DOWN_ARROW, 2 - VERSION_69);
        Keyboard.write(KEY_TAB);
        wait(1);
        // EAP Phase 2 authentication
        // If phase two authentication is defined, select it
        if (settings.phase2_authentication!=PHASE2_AUTO) {
            repeatKey(KEY_DOWN_ARROW, settings.phase2_authentication); // [0]Automatic, [1]EAP-MD5, [2]MSCHAP(v2 pre-v69;v1 v69+, [3]MSCHAPv2, [4]PAP, [5]CHAP, [6]GTC : v69)
        }
        Keyboard.write(KEY_TAB);
        // Server CA Certificate
        if (settings.server_ca_check==SERVER_CA_CHECK_NO) {
            Keyboard.write(KEY_DOWN_ARROW);//change to "DO NOT CHECK"
        }
        Keyboard.write(KEY_TAB);

        // Identity
        Keyboard.print(settings.identity);
        Keyboard.write(KEY_TAB);
        wait(1);
        Keyboard.print(settings.wifi_password);
        repeatKey(KEY_TAB, 2);

        // Anonymous Identity
        Keyboard.print(settings.anonymous_identity);
        Keyboard.write(KEY_TAB);
        Keyboard.write(KEY_ENTER); //Save ID and PW
        switch (settings.device_type) {
            case DEVICETYPE_LESS_THAN_SIXTYNINE:
                repeatKey(KEY_TAB, 1 );
                break;
            case DEVICETYPE_SIXTYNINE:
                repeatKey(KEY_TAB, 2);
                break;
            case DEVICETYPE_GREATERTHAN69:
                repeatKey(KEY_TAB, 2);
                break;

        }

    } else if (settings.eap_method==EAP_TLS) {

        // Select EAP-TLS method
        repeatKey(KEY_DOWN_ARROW, 2);
        Keyboard.write(KEY_TAB);
        //EAP Phase 2 authentication
        // If phase two authentication is defined, select it
        if (settings.phase2_authentication!=PHASE2_AUTO) {
            repeatKey(KEY_DOWN_ARROW, settings.phase2_authentication); // [0]Automatic, [1]EAP-MD5, [2]MSCHAP(v2 pre-v69;v1 v69+, [3]MSCHAPv2, [4]PAP, [5]CHAP, [6]GTC : v69)
        }
        Keyboard.write(KEY_TAB);
        // Server CA Certificate
        if (settings.server_ca_check==SERVER_CA_CHECK_NO) {
            Keyboard.write(KEY_DOWN_ARROW); // Change to "DO NOT CHECK"
        }
        Keyboard.write(KEY_TAB);

        // Subject match
        Keyboard.print(settings.subject_match);
        Keyboard.write(KEY_TAB);

        // Identity
        Keyboard.print(settings.identity);
        repeatKey(KEY_TAB, 3);

    } else    if (settings.eap_method==EAP_TTLS) {

        repeatKey(KEY_DOWN_ARROW, 4);
        Keyboard.write(KEY_TAB);

        // If phase two authentication is defined, select it
        if (settings.phase2_authentication!=PHASE2_AUTO) {
            repeatKey(KEY_DOWN_ARROW, settings.phase2_authentication);
        }
        Keyboard.write(KEY_TAB);

        // Server CA Certificate
        if (settings.server_ca_check==SERVER_CA_CHECK_NO) {
            Keyboard.write(KEY_DOWN_ARROW);
        }

        // Identity
        Keyboard.print(settings.identity);
        Keyboard.write(KEY_TAB);
        Keyboard.print(settings.wifi_password);
        repeatKey(KEY_TAB, 2);

        // Anonymous Identity
        Keyboard.print(settings.anonymous_identity);
        repeatKey(KEY_TAB, 2);
    } else  if (settings.eap_method==EAP_TLS) {
        // Select EAP-TLS method
        repeatKey(KEY_DOWN_ARROW, 3);
        Keyboard.write(KEY_TAB);

        // Server CA Certificate
        if (settings.server_ca_check==SERVER_CA_CHECK_NO) {
            Keyboard.write(KEY_DOWN_ARROW);
        }
        Keyboard.write(KEY_TAB);

        // Subject match
        Keyboard.print(settings.subject_match);
        Keyboard.write(KEY_TAB);

        // Identity
        Keyboard.print(settings.identity);
        repeatKey(KEY_TAB, 3);

        // Anonymous Identity
        Keyboard.print(settings.anonymous_identity);
        repeatKey(KEY_TAB, 2);
    }
}

void updateViaGuest(){ // Guest mode not available after enrollment keys pressed
    wait(3);
    repeatKey(KEY_TAB, 6); // from "Enter Email Address"
    wait(2);
    Keyboard.write(KEY_ENTER);
    wait(15);
    newTab();
    Keyboard.print("chrome://chrome");
    Keyboard.write(KEY_ENTER);
    wait(3);
    repeatKey(KEY_TAB, 1); // Move to "Check for Updates"
    wait(1);
    Keyboard.write(KEY_ENTER);
    wait(update_wait_time);
    //exit Guest Mode
    Keyboard.press(KEY_RIGHT_SHIFT);
    Keyboard.press(KEY_RIGHT_ALT);
    Keyboard.print("s");
    wait(1);
    Keyboard.release(KEY_RIGHT_SHIFT);
    Keyboard.release(KEY_RIGHT_ALT);
    wait(1);
    Keyboard.write(KEY_TAB);
    wait(1);
    Keyboard.write(KEY_ENTER);
    wait(15);
    //  enterEnrollment(); // Comment out to prevent enrolling after guestmode;
    wait(2);
}

void reloadPolicies(){
    wait(3);
    newTab();
    Keyboard.print("chrome://policy");
    Keyboard.write(KEY_ENTER);
    wait(2);
    repeatKey(KEY_TAB, 1);
    wait(1);
    Keyboard.write(KEY_ENTER);
    wait(1);
    closeTab();
}

void removeEnrollmentWifi(){
    wait(7);
    reloadPolicies();
    newTab();
    Keyboard.print("chrome://settings/knownNetworks?type=WiFi");
    Keyboard.write(KEY_ENTER);
    wait(5);
    repeatKey(KEY_TAB, 3); // Select the Top Network's "More options"
    wait(1);
    Keyboard.write(KEY_ENTER);
    wait(2);
    repeatKey(KEY_DOWN_ARROW, 3); // Select "Forget";
    wait(2);
    Keyboard.write(KEY_ENTER);
    wait(5);
    closeTab();
}

void newTab(){
    Keyboard.press(KEY_RIGHT_CTRL);
    Keyboard.print("n");
    wait(1);
    Keyboard.release(KEY_RIGHT_CTRL);
    wait(2);
}
void closeTab(){
    Keyboard.press(KEY_RIGHT_CTRL);
    Keyboard.print("w");
    wait(1);
    Keyboard.release(KEY_RIGHT_CTRL);
    wait(1);
}
void showVersion(){
    Keyboard.press(KEY_RIGHT_ALT);
    Keyboard.print("v");
    wait(1);
    Keyboard.release(KEY_RIGHT_ALT);
}
void Powerwash(){
    Keyboard.press(KEY_RIGHT_SHIFT);
    Keyboard.press(KEY_RIGHT_CTRL);
    Keyboard.press(KEY_RIGHT_ALT);
    Keyboard.print("r");
    wait(1);
    Keyboard.release(KEY_RIGHT_SHIFT);
    Keyboard.release(KEY_RIGHT_CTRL);
    Keyboard.release(KEY_RIGHT_ALT);
    wait(1);
    Keyboard.write(KEY_ENTER);
    wait(2);
    Keyboard.write(KEY_TAB);
    Keyboard.write(KEY_ENTER);
}
void certificateEnrollment() {
    wait(5);
    repeatKey(KEY_TAB, 2);
    Keyboard.print(settings.username); //Enter Username for Certificate Enrollment
    Keyboard.write(KEY_TAB);
    wait(1);
    Keyboard.print(settings.password); //Enter Password for Certificate Enrollment
    Keyboard.write(KEY_TAB);
    wait(1);
    Keyboard.write(KEY_ENTER); //Enable Device Wide certificate for Certificate Enrollment
    wait(1);
    repeatKey(KEY_TAB, 4);
    wait(1);
    Keyboard.write(KEY_ENTER);
    wait(40);
}
void setPrescaler() {
    // Disable interrupts.
    uint8_t oldSREG = SREG;
    cli();

    // Enable change.
    CLKPR = _BV(CLKPCE); // write the CLKPCE bit to one and all the other to zero

    // Change clock division.
    CLKPR = 0x0; // write the CLKPS0..3 bits while writing the CLKPE bit to zero

    // Copy for fast access.
    __clock_prescaler = 0x0;

    // Recopy interrupt register.
    SREG = oldSREG;
}
void flash_led(int count) {

    for (int i = 0; i < count; i++) {
        digitalWrite(13, HIGH);
        delay(100);
        digitalWrite(13, LOW);
        delay(100);
    }
}

void enter_cli() {

    Serial.begin(9600);

    Serial.println(F("Configuration Mode. Enter help for assistance. Copyright 2018-2019 Twocanoes Software, Inc."));
    Serial.setTimeout(1000000);
    while (current_mode == CLI) {
        
        flash_led(1);
Serial.println("");
        Serial.print(">");
        String s = Serial.readStringUntil('\n');
        s.trim();

        Serial.print("read line is:");
        Serial.println(s);
        Serial.println("");
        
#ifdef SHOWFREEMEM
        Serial.print("Free memory: ");
        Serial.println(freeMemory());
#endif


        if (s.startsWith("show") == true) {
          Serial.println("");
            Serial.print("Version: ");
            Serial.println(settings.version, DEC);
            Serial.print(F("Device Version:"));
            Serial.println(settings.device_type==DEVICETYPE_LESS_THAN_SIXTYNINE?"<69":settings.device_type==DEVICETYPE_SIXTYNINE?"69":settings.device_type==DEVICETYPE_GREATERTHAN69?">69":"");
            Serial.print(F("WiFi Name:"));
            Serial.println(strncmp(settings.wifi_name,"",1)==0?"<Not Set>":settings.wifi_name);
            Serial.print(F("WiFi Password:"));
            Serial.println(strncmp(settings.wifi_password,"",1)==0?"<Not Set>":settings.wifi_password);
            Serial.print(F("WiFi Security:"));
            Serial.println(settings.wifi_security==WIFI_SECURITY_NONE?"NONE":settings.wifi_security==WIFI_SECURITY_WEP?"WEP":settings.wifi_security==WIFI_SECURITY_WPA?"WPA":"");
            Serial.print(F("Username:"));
            Serial.println(strncmp(settings.username,"",1)==0?"<Not Set>":settings.username);
            Serial.print(F("Password:"));
            Serial.println(strncmp(settings.password,"",1)==0?"<Not Set>":settings.password);
            Serial.print(F("Autorun:"));
            if (settings.autorun == 1) Serial.println("on");
            else Serial.println("off");
        }
              
              
       else if (s.startsWith(F("version")) == true) {
            Serial.print("Version: ");
            Serial.println(settings.version, DEC);
        }
     
        else if (s.startsWith(F("help")) == true) {
            showusage();

        }

        else if (s.startsWith(F("reset")) == true) {
            setdefaults();
        }
        else if (s.startsWith(F("bootloader")) == true) {
            enter_bootloader();
        }

        else if (s.startsWith(F("reboot")) == true) {

            reboot();
        }
        else if (s.length() == 0) {


        }
        else if (s.startsWith(F("set_settings")) == true){
              static int block_number;
          
              int string_length=s.length()-16;
              s.c_str();  //convert to c string
  
              char *pos=&s[13]; //jump past header
              block_number=3;
              sscanf(pos,"%d",&block_number);
              pos=&s[16];
            
              byte b;
              int start_byte=block_number*BLOCK_SIZE/2;
              int end_byte=start_byte+string_length/2-1;
              if ((end_byte>sizeof(settings)-1)|| (string_length>BLOCK_SIZE) ){
                 Serial.println(F("Invalid block or length."));
  
                 continue;
             
              }
           
            
              for(long i = 0; i<string_length/2; i++){
                  byte *ptr=(byte *)(void *)&settings;
                  sscanf(&pos[i*2],"%02x",&b);
                
                  memcpy(&ptr[start_byte+i],&b,1); 
              }

              
              if (block_number>=sizeof(settings)/(BLOCK_SIZE/2)){
                
                EEPROM_writeAnything(0, settings);
              }
        }

        else if (s.startsWith("get_settings") == true) {

            const byte* p = (const byte*)(const void*)&settings;
            unsigned int i;
            char curr_bytes[3];
            Serial.print(F("DATA:"));
            for (i = 0; i < sizeof(settings); i++){
                sprintf(curr_bytes,"%02x",p[i]);
                Serial.print(curr_bytes);
            }
            Serial.println();

         
        }
        else {
            Serial.println(F("Invalid command. Enter help for usage."));
            Serial.println(s);

        }
    }
    Serial.end();
}
void showusage() {

  Serial.println(F("\nCopyright 2018 Twocanoes Software, Inc."));
  Serial.println(F("help: this message"));
  Serial.println(F("show: show current settings"));
  Serial.println(F("reset: reset settings to defaults"));
  Serial.println(F("reboot: reboot the device"));
 

}
void enter_bootloader(){
  uint16_t bootKey = 0x7777;
  uint16_t *const bootKeyPtr = (uint16_t *)0x0800;

// Stash the magic key
*bootKeyPtr = bootKey;

// Set a watchdog timer
wdt_enable(WDTO_120MS);

while(1) {} // This infinite loop ensures nothing else
            // happens before the watchdog reboots us

}
//TODO:
#define in_developer_mode 0 // Set to 1 if device is in developer mode
