// https://developer.apple.com/legacy/library/qa/qa2001/qa1123.html

#ifndef GetBSDProcessList_h
#define GetBSDProcessList_h

#include <stdio.h>
#include <sys/sysctl.h>

typedef struct kinfo_proc kinfo_proc;

int GetBSDProcessList(kinfo_proc **procList, size_t *procCount);

#endif /* GetBSDProcessList_h */
