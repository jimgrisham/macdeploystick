//
//  TCSMainWindowRightViewController.h
//  MDS
//
//  Created by Timothy Perfitt on 4/1/20.
//  Copyright © 2020 Twocanoes Software. All rights reserved.
//

#import <Cocoa/Cocoa.h>

NS_ASSUME_NONNULL_BEGIN

@interface TCSMainWindowRightViewController : NSViewController
@property (strong) IBOutlet NSImageView *titleImage;
@property (weak) IBOutlet NSLayoutConstraint *titleImageWidthConstaint;

@property (weak) IBOutlet NSLayoutConstraint *titleImageHeightConstaint;

@property (weak) IBOutlet NSLayoutConstraint *titleImageLeadingConstaint;

@property (weak) IBOutlet NSLayoutConstraint *titleImageTopConstaint;


@end

NS_ASSUME_NONNULL_END
