//
//  TCSSyncViewController.h
//  MDS
//
//  Created by Timothy Perfitt on 3/26/20.
//  Copyright © 2020 Twocanoes Software. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import "TCSMainWindowRightViewController.h"
NS_ASSUME_NONNULL_BEGIN

@interface TCSSyncViewController : TCSMainWindowRightViewController

@end

NS_ASSUME_NONNULL_END
