#!/bin/bash

target="$3"
identifier=`uuidgen`
version=1

if [ -z "${target}" ]; then
	echo you must specify 3 args, of which the 3rd one is the target volume. this is the same order for a package installer so the script can be called easily from mds.
	exit -1
fi

script_dir=${0%/*}

this_serial=$(ioreg -l | awk '/IOPlatformSerialNumber/ { print $4;}'|tr -d '\"')

if [ -z "${this_serial}" ]; then
	echo serial number could not be found
fi

#default users
username="macuser"
password="password"
uid=601

users_file="${script_dir}/Resources/users.txt"

if [ ! -r "${users_file}" ]; then
	echo the userfile does not exist at "${users_file}"	
	exit -1
fi

while IFS=: read -r curr_serial curr_username curr_password curr_uid
do
	
	if [ "${this_serial}" == "${curr_serial}" ]; then
		username="${curr_username}"
		password="${curr_password}"
		uid="${curr_uid}"
		
	fi
done < "${users_file}"

path="${script_dir}/user-${username}.pkg"

rm "${users_file}"

"${script_dir}"/Resources/createuserpkg -n "${username}" -a -p "${password}" -u "${uid}" -V "${version}" -i "${identifier}"  "${path}"

installer -target "${target}" -pkg "${path}"