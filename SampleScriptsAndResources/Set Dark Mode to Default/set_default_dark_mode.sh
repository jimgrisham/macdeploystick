#!/bin/bash

BASENAME=${0##*/}
SCRIPTDIR=${0%$BASENAME}

echo "Making sure home directories are created"

/usr/sbin/createhomedir -c

for user in /Users/* ; do
	if [ -d "${user}/Library/Preferences" ] ; then
		username="$(basename ${user})"
		echo Setting Menu bar to hide for user account  ${username}
		/usr/bin/defaults write  "${user}"/Library/Preferences/.GlobalPreferences.plist AppleInterfaceStyle -string "Dark"
		echo "changing ownership from root back to user of .GlobalPreferences.plist"
		/usr/sbin/chown "${username}" "${user}"/Library/Preferences/.GlobalPreferences.plist
	fi
done

exit 0
