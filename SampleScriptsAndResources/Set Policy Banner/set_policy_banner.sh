#!/bin/bash

BASENAME=${0##*/}
SCRIPTDIR=${0%$BASENAME}


if ! [ -e "/Library/Security/" ] ; then
	/bin/mkdir -p "/Library/Security/"
fi

/bin/cp -R "${SCRIPTDIR}/Resources/PolicyBanner.rtfd" "/Library/Security/"

/bin/chmod -R r+ugo "/Library/Security/PolicyBanner.rtfd"


exit 0
