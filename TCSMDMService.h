//
//  TCSMDMService.h
//  MDS
//
//  Created by Timothy Perfitt on 9/3/19.
//  Copyright © 2019 Twocanoes Software. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface TCSMDMService : NSObject
+ (TCSMDMService *)sharedManager ;
-(BOOL)isRunning;
-(void)startMDMWithCallback:(void (^)(NSError * _Nullable err))callback;
-(void)stopMDMWithCallback:(void (^)(NSError * _Nullable err))callback;
-(void)restartMDMWithCallback:(void (^)(NSError * _Nullable err))callback;
-(NSString *)mdmSettings;
@end

NS_ASSUME_NONNULL_END
