//
//  MunkiView.h
//  MDS
//
//  Created by Timothy Perfitt on 9/15/19.
//  Copyright © 2019 Twocanoes Software. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import "TCSWorkflow.h"
#import "MunkiView.h"
#import "MunkiConfigProtocol.h"

NS_ASSUME_NONNULL_BEGIN


@interface MunkiView : NSView

@end

NS_ASSUME_NONNULL_END
