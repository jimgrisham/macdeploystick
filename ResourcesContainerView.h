//
//  MunkiContainerView.h
//  MDS
//
//  Created by Timothy Perfitt on 9/15/19.
//  Copyright © 2019 Twocanoes Software. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import "ResourcesView.h"
#import "ResourcesConfigProtocol.h"
NS_ASSUME_NONNULL_BEGIN

@interface ResourcesContainerView : NSView
@property IBOutlet ResourcesView *view;
@property IBOutlet id <ResourcesConfigProtocol> delegate;
@property (weak) IBOutlet NSButton *scriptsOrderButton;
@property (weak) IBOutlet NSButton *profilesOrderButton;
@property (weak) IBOutlet NSButton *packagesOrderButton;
@property (strong) IBOutlet NSView *packagesButtonView;
@property (strong) IBOutlet NSView *scriptsButtonView;
@property (strong) IBOutlet NSView *profilesButtonView;


@end

NS_ASSUME_NONNULL_END
